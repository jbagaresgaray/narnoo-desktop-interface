import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  NgZone,
  ViewEncapsulation
} from "@angular/core";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import * as $ from "jquery";
import * as _ from "lodash";
import * as async from "async";

import { CollectionsService } from "../../services/collections.service";

@Component({
  selector: "app-collection-channel-picker",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./collection-channel-picker.component.html",
  styleUrls: ["./collection-channel-picker.component.css"]
})
export class CollectionChannelPickerComponent implements OnInit {
  action: string;
  type: string;
  title: string;
  token: string;

  params: any = {};
  business: any = {};
  selected: any = {};

  perPage = 16;
  showLoading: boolean = false;
  isSaving: boolean = false;

  showContentCollections: boolean = false;
  showContentCollectionsErr: boolean = false;
  collectionsContentErr: any = {};
  pageCollection: number = 1;
  totalPageCollection: number = 0;

  imagesLoadingArr: any[] = [];
  selectedMedia: any[] = [];
  collectionArr: any[] = [];

  constructor(
    public bsModalRef: BsModalRef,
    private changeDetection: ChangeDetectorRef,
    public zone: NgZone,
    public toastr: ToastrService,
    public coolDialog: NgxCoolDialogsService,
    private modalService: BsModalService,
    public collections: CollectionsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
    for (let i = 0; i < 16; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  ngOnInit() {
    console.log("action: ", this.action);
    console.log("params: ", this.params);

    this.pageCollection = 1;
    this.showContentCollections = false;
    this.showContentCollectionsErr = false;

    this.loadCollections(this.pageCollection, this.perPage);
  }

  private loadCollections(page, total) {
    this.collections
      .collecton_list(this.token, {
        page: page,
        total: total
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            this.collectionArr = [];

            this.pageCollection = parseInt(data.data.page, 0);
            this.totalPageCollection = parseInt(data.data.totalPages, 0);

            for (let i = 0; i < _.size(data.data.data); i++) {
              data.data.data[i].selected = false;

              if (!_.isEmpty(data.data.data[i].images)) {
                data.data.data[i].image = data.data.data[i].images[0].image200;
              } else {
                data.data.data[i].image = "./assets/img/thumb.jpg";
              }

              this.collectionArr.push(data.data.data[i]);
            }
            this.showContentCollectionsErr = false;
          } else if (data && data.success && data.data[0] === false) {
            this.showContentCollectionsErr = true;
            this.collectionsContentErr = {
              message: "No results found"
            };
          }
          this.showLoading = false;
          this.showContentCollections = true;
        },
        error => {
          this.showContentCollections = true;
          this.showContentCollectionsErr = true;
          this.showLoading = false;
          if (error && !error.success) {
            this.collectionsContentErr = error;
          }
        }
      );
  }

  private addToCollection() {
    let selectedDatas: any[] = [];
    selectedDatas = _.filter(this.collectionArr, (row: any) => {
      return row.selected;
    });
    console.log("selectedDatas: ", selectedDatas);

    if (_.isEmpty(selectedDatas)) {
      this.toastr.warning("No collections are selected!", "WARNING");
      return;
    }

    async.eachSeries(
      selectedDatas,
      (item: any, callback) => {
        this.collections
          .collection_add_media(
            {
              id: item.id,
              media: this.type,
              mediaId: this.params.id
            },
            this.token
          )
          .then(
            (data: any) => {
              if (data && data.success) {
                this.modalService.setDismissReason("save");
                this.toastr.success(data.data, "INFO");
              }
              callback();
            },
            error => {
              console.log("error: ", error);
              if (error && !error.success) {
                this.toastr.warning(error.message, "WARNING");
              }
              callback();
            }
          );
      },
      () => {
        this.bsModalRef.hide();
      }
    );
  }

  selectMedia(item: any) {
    console.log("selectMedia: ", item);
    /* this.selected = item;
    _.each(this.collectionArr, (row: any) => {
      if (item.id === row.id) {
        row.selected = true;
      } else {
        row.selected = false;
      }
    }); */

    if (_.find(this.selectedMedia, { id: item.id })) {
      item.selected = false;
      _.remove(this.selectedMedia, (row: any) => {
        return row.id === item.id;
      });
    } else {
      item.selected = true;
      this.selectedMedia.push(item);
    }
  }

  viewMore(event: any) {
    console.log("Page changed to: " + event.page);
    console.log("Number items per page: " + event.itemsPerPage);

    this.pageCollection = event.page;
    this.showLoading = true;
    this.showContentCollections = false;
    this.showContentCollectionsErr = false;

    this.loadCollections(this.pageCollection, this.perPage);
  }

  saveChanges() {
    this.coolDialog.confirm("Add to collection?").subscribe(res => {
      if (res) {
        this.addToCollection();
      }
    });
  }
}
