import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CollectionChannelPickerComponent } from "./collection-channel-picker.component";

describe("CollectionChannelPickerComponent", () => {
  let component: CollectionChannelPickerComponent;
  let fixture: ComponentFixture<CollectionChannelPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CollectionChannelPickerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionChannelPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
