import { CollectionChannelPickerModule } from './collection-channel-picker.module';

describe('CollectionChannelPickerModule', () => {
  let collectionChannelPickerModule: CollectionChannelPickerModule;

  beforeEach(() => {
    collectionChannelPickerModule = new CollectionChannelPickerModule();
  });

  it('should create an instance', () => {
    expect(collectionChannelPickerModule).toBeTruthy();
  });
});
