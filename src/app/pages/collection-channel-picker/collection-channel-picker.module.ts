import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";
import { BsDropdownModule, PaginationModule } from "ngx-bootstrap";
import { FormsModule } from "@angular/forms";

import { CollectionChannelPickerComponent } from "./collection-channel-picker.component";

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    PipesModule,
    BsDropdownModule,
    PaginationModule,
    FormsModule
  ],
  declarations: [CollectionChannelPickerComponent],
  entryComponents: [CollectionChannelPickerComponent]
})
export class CollectionChannelPickerModule {}
