import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { BsDropdownModule, TabsModule, AccordionModule } from "ngx-bootstrap";
import { QuillModule } from "ngx-quill";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TagInputModule } from "ngx-chips";

import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

import { ProductsComponent } from "./products.component";
import { ProductEntryComponent } from "./product-entry/product-entry.component";
import { ProductDetailsComponent } from "./product-details/product-details.component";
import { ProductDetailsInformationComponent } from "./product-details/product-details-information/product-details-information.component";
import { ProductDetailsGalleryComponent } from "./product-details/product-details-gallery/product-gallery.component";
import { ProductDetailsDetailComponent } from "./product-details/product-details-detail/product-details-detail.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    PipesModule,
    ComponentsModule,
    FormsModule,
    BsDropdownModule,
    TabsModule,
    AccordionModule,
    QuillModule,
    ComponentsModule,
    TagInputModule,
    ReactiveFormsModule
  ],
  declarations: [
    ProductsComponent,
    ProductDetailsComponent,
    ProductEntryComponent,
    ProductDetailsGalleryComponent,
    ProductDetailsInformationComponent,
    ProductDetailsDetailComponent
  ],
  entryComponents: [
    ProductsComponent,
    ProductDetailsComponent,
    ProductEntryComponent
  ],
  exports: [ProductDetailsInformationComponent, ProductDetailsDetailComponent]
})
export class ProductsModule {}
