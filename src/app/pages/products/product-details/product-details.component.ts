import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef,
  NgZone
} from "@angular/core";
import { Location } from "@angular/common";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as _ from "lodash";
import { combineLatest, Subscription } from "rxjs";
import { filter } from "rxjs/operators";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";

import { ProductsService } from "../../../services/products.service";
import { UtilitiesService } from "../../../services/utilities.service";
import { ByobService } from "../../../services/byob.service";

import { DatapickerComponent } from "../../datapicker/datapicker.component";

declare let $: any;

@Component({
  selector: "app-product-details",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./product-details.component.html",
  styleUrls: ["./product-details.component.css"]
})
export class ProductDetailsComponent implements OnInit {
  token: string;
  action: string;

  connect_params: any = {};
  business: any = {};
  notif_business: any = {};
  productId: string;
  product: any = {};
  features: any = {};
  textEditorConfig: any = {};

  fakeArr: any[] = [];
  platformsArr: any[] = [];
  productMapping: any[] = [];
  newDescriptionArr: any[] = [];
  newSummaryArr: any[] = [];

  pageProduct: number = 0;
  perPage: number = 10;

  imageAction: string = "overview";

  showPreview: boolean = false;
  showContent: boolean = true;

  isBookingSaving: boolean = false;
  isProductNameSaving: boolean = false;
  isProductSummarySaving: boolean = false;
  isProductDescriptionSaving: boolean = false;
  isProductFeaturesSaving: boolean = false;
  isProductInformationSaving: boolean = false;
  isProductMappingSaving: boolean = false;

  isInfo: boolean = true;
  isDetails: boolean = false;
  isGallery: boolean = false;

  showBookingSettings: boolean = true;
  showGalleryContent: boolean = false;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    public zone: NgZone,
    private router: Router,
    private route: ActivatedRoute,
    public location: Location,
    public toastr: ToastrService,
    private changeDetection: ChangeDetectorRef,
    private modalService: BsModalService,
    public coolDialogs: NgxCoolDialogsService,
    private products: ProductsService,
    private utilities: UtilitiesService,
    private byob: ByobService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }

  ngOnInit() {
    this.productId = this.route.snapshot.paramMap.get("id");
    this.action = this.route.snapshot.queryParamMap.get("action");
    this.connect_params =
      JSON.parse(this.route.snapshot.queryParamMap.get("params")) || {};
    this.notif_business = this.route.snapshot.queryParamMap.get("business");

    this.textEditorConfig = {
      toolbar: [
        ["bold", "italic", "underline", "strike"],
        [{ align: [] }],
        [{ list: "ordered" }, { list: "bullet" }],
        [{ size: ["small", false, "large", "huge"] }],
        [{ font: [] }]
      ]
    };

    const options = {
      step: 15,
      timeFormat: "g:i A"
    };
    $(document).ready(function() {
      $("#operational-start-time").timepicker(options);
      $("#operational-end-time").timepicker(options);
    });

    this.product.keywordsArr = [];
    this.product.settingsLibrary = false;
    this.product.settingsBookable = false;

    if (this.action === "connect") {
      if (this.connect_params.type === "operator") {
        this.showBookingSettings = false;
      } else if (
        this.connect_params.type !== "operator" &&
        (this.business.role !== 3 && this.business.role !== 4)
      ) {
        this.showBookingSettings = true;
      }
    } else {
      if (this.business.role !== 3 && this.business.role !== 4) {
        this.showBookingSettings = true;
      }
    }
    this.initData();
  }

  private initData() {
    this.newDescriptionArr = [];
    this.newSummaryArr = [];

    const productResponse = (data: any) => {
      if (data && data.success) {
        this.product = data.data;

        if (this.product.description) {
          const descriptionArr = this.product.description.description;
          const summaryArr = this.product.description.summary;

          if (!_.isEmpty(descriptionArr)) {
            _.each(descriptionArr, (row: any) => {
              const arr: any = Object.keys(row).map(key => ({
                key,
                value: row[key],
                expanded: false,
                action: "desc",
                mode: "desc_" + key
              }));

              this.zone.run(() => {
                this.newDescriptionArr.push(arr[0]);
              });
            });
          } else {
            this.newDescriptionArr.push(
              {
                key: "english",
                value: {
                  text: ""
                },
                expanded: false,
                action: "desc",
                mode: "desc_english"
              },
              {
                key: "japanese",
                value: {
                  text: ""
                },
                expanded: false,
                action: "desc",
                mode: "desc_english"
              },
              {
                key: "chinese",
                value: {
                  text: ""
                },
                expanded: false,
                action: "desc",
                mode: "desc_english"
              },
              {
                key: "korean",
                value: {
                  text: ""
                },
                expanded: false,
                action: "desc",
                mode: "desc_english"
              },
              {
                key: "german",
                value: {
                  text: ""
                },
                expanded: false,
                action: "desc",
                mode: "desc_english"
              }
            );
          }

          if (!_.isEmpty(summaryArr)) {
            _.each(summaryArr, (row: any) => {
              const arr: any = Object.keys(row).map(key => ({
                key,
                value: row[key],
                expanded: false,
                action: "sum",
                mode: "sum_" + key
              }));
              this.zone.run(() => {
                this.newSummaryArr.push(arr[0]);
              });
            });
          } else {
            this.newSummaryArr.push(
              {
                key: "english",
                value: {
                  text: ""
                },
                expanded: false,
                action: "sum",
                mode: "sum_english"
              },
              {
                key: "japanese",
                value: {
                  text: ""
                },
                expanded: false,
                action: "sum",
                mode: "sum_english"
              },
              {
                key: "chinese",
                value: {
                  text: ""
                },
                expanded: false,
                action: "sum",
                mode: "sum_english"
              },
              {
                key: "korean",
                value: {
                  text: ""
                },
                expanded: false,
                action: "sum",
                mode: "sum_english"
              },
              {
                key: "german",
                value: {
                  text: ""
                },
                expanded: false,
                action: "sum",
                mode: "sum_english"
              }
            );
          }
        } else {
          this.newDescriptionArr.push(
            {
              key: "english",
              value: {
                text: ""
              },
              expanded: false,
              action: "desc",
              mode: "desc_english"
            },
            {
              key: "japanese",
              value: {
                text: ""
              },
              expanded: false,
              action: "desc",
              mode: "desc_english"
            },
            {
              key: "chinese",
              value: {
                text: ""
              },
              expanded: false,
              action: "desc",
              mode: "desc_english"
            },
            {
              key: "korean",
              value: {
                text: ""
              },
              expanded: false,
              action: "desc",
              mode: "desc_english"
            },
            {
              key: "german",
              value: {
                text: ""
              },
              expanded: false,
              action: "desc",
              mode: "desc_english"
            }
          );

          this.newSummaryArr.push(
            {
              key: "english",
              value: {
                text: ""
              },
              expanded: false,
              action: "sum",
              mode: "sum_english"
            },
            {
              key: "japanese",
              value: {
                text: ""
              },
              expanded: false,
              action: "sum",
              mode: "sum_english"
            },
            {
              key: "chinese",
              value: {
                text: ""
              },
              expanded: false,
              action: "sum",
              mode: "sum_english"
            },
            {
              key: "korean",
              value: {
                text: ""
              },
              expanded: false,
              action: "sum",
              mode: "sum_english"
            },
            {
              key: "german",
              value: {
                text: ""
              },
              expanded: false,
              action: "sum",
              mode: "sum_english"
            }
          );
        }

        if (this.product.additionalInformation) {
          this.product.startTime = this.product.additionalInformation.startTime;
          this.product.endTime = this.product.additionalInformation.endTime;
          this.product.duration = this.product.additionalInformation.operatingHours;
          this.product.transportText = this.product.additionalInformation.transfer;
          this.product.purchasesText = this.product.additionalInformation.purchases;
          this.product.packingText = this.product.additionalInformation.packing;
          this.product.healthText = this.product.additionalInformation.fitness;
          this.product.childrenText = this.product.additionalInformation.child;
          this.product.additionalText = this.product.additionalInformation.additional;
          this.product.termsText = this.product.additionalInformation.terms;
        }

        this.product.price = this.product.avgPrice;
        this.product.bookingLink = this.product.directBooking;
        this.product.settingsBookable = this.product.bookable;
        this.product.settingsLibrary = this.product.access;
        if (this.product.keywords) {
          this.product.keywordsArr = _.map(
            this.product.keywords.split(","),
            (row: any) => {
              return {
                display: row,
                value: row
              };
            }
          );
        } else {
          this.product.keywordsArr = [];
        }

        this.features = this.product.features;
        if (this.features) {
          // ==================================================================
          // ==================================================================
          // ==================================================================
          this.features.disabledAccess =
            this.features.disabledAccess === "1" ? true : false;
          this.features.giftShop =
            this.features.giftShop === "1" ? true : false;
          this.features.storageLockers =
            this.features.storageLockers === "1" ? true : false;
          this.features.visitorInformation =
            this.features.visitorInformation === "1" ? true : false;
          this.features.dinnerIncluded =
            this.features.dinnerIncluded === "1" ? true : false;
          this.features.internetAccess =
            this.features.internetAccess === "1" ? true : false;
          this.features.media = this.features.media === "1" ? true : false;
          this.features.functionHire =
            this.features.functionHire === "1" ? true : false;
          this.features.hotelPickupService =
            this.features.hotelPickupService === "1" ? true : false;
          this.features.mealProvided =
            this.features.mealProvided === "1" ? true : false;
          this.features.guidedTours =
            this.features.guidedTours === "1" ? true : false;
          this.features.weddingServices =
            this.features.weddingServices === "1" ? true : false;
          this.features.breakfestIncluded =
            this.features.breakfestIncluded === "1" ? true : false;
          this.features.cafe = this.features.cafe === "1" ? true : false;
          this.features.languageTranslation =
            this.features.languageTranslation === "1" ? true : false;
          this.features.conferenceServices =
            this.features.conferenceServices === "1" ? true : false;
          this.features.lunchIncluded =
            this.features.lunchIncluded === "1" ? true : false;
          // ==================================================================
          // ==================================================================
          // ==================================================================

          this.features["24HourDesk"] =
            this.features["24HourDesk"] === "1" ? true : false;
          this.features.bar = this.features.bar === "1" ? true : false;
          this.features.conciergeService =
            this.features.conciergeService === "1" ? true : false;
          this.features.conferenceServices =
            this.features.conferenceServices === "1" ? true : false;
          this.features.expressCheckIn =
            this.features.expressCheckIn === "1" ? true : false;
          this.features.expressCheckOut =
            this.features.expressCheckOut === "1" ? true : false;
          this.features.fitnessCentre =
            this.features.fitnessCentre === "1" ? true : false;
          this.features.kitchenFacilities =
            this.features.kitchenFacilities === "1" ? true : false;
          this.features.laundryFacilities =
            this.features.laundryFacilities === "1" ? true : false;
          this.features.meetingFacilities =
            this.features.meetingFacilities === "1" ? true : false;
          this.features.nonSmoking =
            this.features.nonSmoking === "1" ? true : false;
          this.features.parkingAccess =
            this.features.parkingAccess === "1" ? true : false;
          this.features.restaurant =
            this.features.restaurant === "1" ? true : false;
          this.features.roomService =
            this.features.roomService === "1" ? true : false;
          this.features.safeDeposit =
            this.features.safeDeposit === "1" ? true : false;
          this.features.swimmingPool =
            this.features.swimmingPool === "1" ? true : false;
          this.features.tourBookingService =
            this.features.tourBookingService === "1" ? true : false;
          this.features.wifiAccess =
            this.features.wifiAccess === "1" ? true : false;
        }
      }
      this.showContent = false;
      console.log("product: ", this.product);
      console.log("this.features: ", this.features);

      setTimeout(() => {
        $(".progressive-image").each(function() {
          const image = new Image();
          const previewImage = $(this).find(".loadingImage");
          const newImage = $(this).find(".overlay");

          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(./assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
          };
          image.src = previewImage.data("image");
        });
      }, 600);
    };

    const errorResponse = error => {
      this.showContent = false;
      console.log("product detail ERROR: ", error);
    };

    this.showContent = true;
    if (this.action === "connect") {
      this.products
        .operator_products_detail(
          this.token,
          this.connect_params.id,
          this.productId
        )
        .then(productResponse, errorResponse);
    } else {
      this.products
        .product_details(this.token, this.productId)
        .then(productResponse, errorResponse);
    }
  }

  back() {
    this.location.back();
  }

  refresh() {
    this.initData();
  }

  showTab(tab) {
    if (tab === "info") {
      this.isInfo = true;
      this.isDetails = false;
      this.isGallery = false;
    } else if (tab === "details") {
      this.isInfo = false;
      this.isDetails = true;
      this.isGallery = false;
    } else if (tab === "gallery") {
      this.isInfo = false;
      this.isDetails = false;
      this.isGallery = true;
    } else if (tab === "booking") {
      this.isInfo = false;
      this.isDetails = false;
      this.isGallery = false;
    } else if (tab === "mapping") {
      this.isInfo = false;
      this.isDetails = false;
      this.isGallery = false;
    }
  }

  viewImageDetail(item: any) {
    if (item.error) {
      this.toastr.warning(item.msg, "WARNING");
      return;
    }

    /*if (this.action == 'connect') {
			let navigationExtras: NavigationExtras = {
				queryParams: {
					'action': 'connect',
					'params': JSON.stringify(this.connect_params),
				},
				preserveFragment: true
			};
			this.router.navigate(['/products', item.id], navigationExtras);
		} else {
			this.router.navigate(['/products', item.id]);
		}*/
  }

  viewVideoDetail(item: any) {
    if (item.error) {
      this.toastr.warning(item.msg, "WARNING");
      return;
    }

    /*if (this.action == 'connect') {
		  this.navCtrl.push(VideoDetailPage, {
			video: item,
			action: 'connect',
			params: this.connect_params
		  });
		} else {
		  this.navCtrl.push(VideoDetailPage, {
			video: item,
		  });
		}*/
  }

  viewPrintDetail(item: any) {
    if (item.error) {
      this.toastr.warning(item.msg, "WARNING");
      return;
    }

    /*if (this.action == 'connect') {
		  this.navCtrl.push(PrintDetailPage, {
			print: item,
			action: 'connect',
			params: this.connect_params
		  });
		} else {
		  this.navCtrl.push(PrintDetailPage, {
			print: item,
		  });
		}*/
  }

  viewLogoDetail(item: any) {
    if (item.error) {
      this.toastr.warning(item.msg, "WARNING");
      return;
    }

    /*if (this.action == 'connect') {
		  this.navCtrl.push(LogoDetailPage, {
			logo: item,
			action: 'connect',
			params: this.connect_params
		  });
		} else {
		  this.navCtrl.push(LogoDetailPage, {
			logo: item,
		  });
		}*/
  }

  saveProductNameChanges() {
    this.coolDialogs
      .confirm("Are you sure to save Product name/title?")
      .subscribe(res => {
        if (res) {
          this.isProductNameSaving = false;
          this.toastr.info("Saving product summary...", "INFO");
          this.products
            .product_edit_title(this.token, {
              productId: this.product.productId,
              title: this.product.title
            })
            .then(
              (data: any) => {
                if (data && data.success) {
                  console.log("success: ", data);
                  this.toastr.success(data.data, "SUCCESS");
                  setTimeout(() => {
                    this.refresh();
                  }, 300);
                } else if (data && !data.success) {
                  this.toastr.warning(data.message, "WARNING");
                }
                this.isProductNameSaving = false;
              },
              error => {
                console.log("error: ", error);
                this.isProductNameSaving = false;
                if (error && !error.success) {
                  this.toastr.error(error.error, "ERROR");
                }
              }
            );
        }
      });
  }

  saveProductChanges(action, mode?: any, product?: any) {
    console.log("saveProductChanges: ", action);
    console.log("saveProductChanges: ", mode);
    console.log("saveProductChanges: ", product);

    let confirmMsg: string;

    const callbackSave = () => {
      if (action === "information") {
        this.isProductInformationSaving = false;
      } else if (action === "booking") {
        this.isBookingSaving = false;
      } else if (action === "mapping") {
        this.isProductMappingSaving = false;
      } else if (action === "desc") {
        this.isProductDescriptionSaving = false;
      } else if (action === "sum") {
        this.isProductSummarySaving = false;
      }
    };

    const callbackConfirm = () => {
      if (action === "information") {
        confirmMsg = "Are you sure to save product information?";
      } else if (action === "booking") {
        confirmMsg = "Are you sure to save product booking link?";
      } else if (action === "desc") {
        const _mode = _.split(mode, "_");
        confirmMsg =
          "Are you sure to save " +
          _.startCase(_mode[1]) +
          " product description?";
        if (product) {
          this.product["description" + _.startCase(_mode[1])] =
            product.value.text;
        }
      } else if (action === "sum") {
        const _mode = _.split(mode, "_");
        confirmMsg =
          "Are you sure to save " + _.startCase(_mode[1]) + " product summary?";
        if (product) {
          this.product["summary" + _.startCase(_mode[1])] = product.value.text;
        }
      } else if (action === "mapping") {
        confirmMsg = "Map product to reservation platform?";
      }
    };

    const callbackToast = () => {
      if (action === "information") {
        this.isProductInformationSaving = true;
        this.toastr.info("Saving Product Information...", "INFO");
      } else if (action === "booking") {
        this.isBookingSaving = true;
        this.toastr.info("Saving Booking link...", "INFO");
      } else if (action === "desc") {
        this.isProductDescriptionSaving = true;
        const _mode = _.split(mode, "_");
        this.toastr.info(
          "Saving " + _.startCase(_mode[1]) + " product description...",
          "INFO"
        );
      } else if (action === "sum") {
        this.isProductDescriptionSaving = true;
        const _mode = _.split(mode, "_");
        this.toastr.info(
          "Saving " + _.startCase(_mode[1]) + " product summary...",
          "INFO"
        );
      } else if (action === "mapping") {
        this.isProductMappingSaving = true;
        this.toastr.info("Mapping product to reservation platform...", "INFO");
      }
    };

    const saveProductChanges = () => {
      callbackConfirm();

      this.coolDialogs.confirm(confirmMsg).subscribe(res => {
        if (res) {
          callbackToast();

          if (!_.isEmpty(this.product.keywordsArr)) {
            this.product.keywords = _.map(
              this.product.keywordsArr,
              (row: any) => {
                return row.value;
              }
            ).join(",");
          }
          this.product.bookingLink = this.product.directBooking;
          this.product.startTime = $("#operational-start-time").val();
          this.product.endTime = $("#operational-end-time").val();

          this.products.product_edit(this.token, this.product).then(
            (data: any) => {
              if (data && data.success) {
                console.log("success: ", data);
                this.toastr.success(data.data, "SUCCESS");
                setTimeout(() => {
                  this.refresh();
                }, 300);
              } else if (data && !data.success) {
                this.toastr.warning(data.message, "WARNING");
              }

              callbackSave();
            },
            error => {
              console.log("error: ", error);
              if (error && !error.success) {
                this.toastr.error(error.error, "ERROR");
              }

              callbackSave();
            }
          );
        }
      });
    };

    saveProductChanges();
  }

  saveProductFeatures() {
    let featuresObj: any = {};
    console.log("this.product.category: ", this.product.category);

    if (this.product.category === "service") {
      featuresObj = {
        productId: this.product.productId,
        disabilityAccess: this.features.disabledAccess,
        internet: this.features.internetAccess,
        foodIncluded: this.features.mealProvided,
        cafe: this.features.cafe,
        giftShop: this.features.giftShop,
        mediaService: this.features.media,
        guidedTours: this.features.guidedTours,
        translation: this.features.languageTranslation,
        lockers: this.features.storageLockers,
        functions: this.features.functionHire,
        weddings: this.features.weddingServices,
        conference: this.features.conferenceServices,
        vistiorInformation: this.features.visitorInformation,
        pickUp: this.features.hotelPickupService,
        breakfast: this.features.breakfestIncluded,
        lunch: this.features.lunchIncluded,
        dinner: this.features.dinnerIncluded
      };
    } else if (this.product.category === "accommodation") {
      featuresObj = {
        expressCheckIn: this.features.expressCheckIn,
        expressCheckOut: this.features.expressCheckOut,
        desk24hr: this.features["24HourDesk"],
        concierge: this.features.conciergeService,
        tourDesk: this.features.tourBookingService,
        fitness: this.features.fitnessCentre,
        parking: this.features.parkingAccess,
        meetingFacilities: this.features.meetingFacilities,
        laundry: this.features.laundryFacilities,
        restaurant: this.features.restaurant,
        roomService: this.features.roomService,
        kitchen: this.features.kitchenFacilities,
        internet: this.features.internetAccess,
        wifi: this.features.wifiAccess,
        nonsmoking: this.features.nonSmoking,
        disabilityAccess: this.features.disabledAccess,
        safe: this.features.safeDeposit,
        weddings: this.features.weddingServices,
        conference: this.features.conferenceServices,
        bar: this.features.bar,
        swimming: this.features.swimmingPool
      };
    } else {
      featuresObj = {
        productId: this.product.productId,
        expressCheckIn: this.features.expressCheckIn,
        expressCheckOut: this.features.expressCheckOut,
        desk24hr: this.features["24HourDesk"],
        concierge: this.features.conciergeService,
        tourDesk: this.features.tourBookingService,
        fitness: this.features.fitnessCentre,
        parking: this.features.parkingAccess,
        meetingFacilities: this.features.meetingFacilities,
        laundry: this.features.laundryFacilities,
        restaurant: this.features.restaurant,
        roomService: this.features.roomService,
        kitchen: this.features.kitchenFacilities,
        internet: this.features.internetAccess,
        wifi: this.features.wifiAccess,
        nonsmoking: this.features.nonSmoking,
        disabilityAccess: this.features.disabledAccess,
        safe: this.features.safeDeposit,
        weddings: this.features.weddingServices,
        conference: this.features.conferenceServices,
        bar: this.features.bar,
        swimming: this.features.swimmingPool,
        foodIncluded: this.features.mealProvided,
        cafe: this.features.cafe,
        giftShop: this.features.giftShop,
        mediaService: this.features.media,
        guidedTours: this.features.guidedTours,
        translation: this.features.languageTranslation,
        lockers: this.features.storageLockers,
        functions: this.features.functionHire,
        vistiorInformation: this.features.visitorInformation,
        pickUp: this.features.hotelPickupService,
        breakfast: this.features.breakfestIncluded,
        lunch: this.features.lunchIncluded,
        dinner: this.features.dinnerIncluded
      };
    }

    console.log("featuresObj: ", featuresObj);

    this.coolDialogs
      .confirm("Are you sure to save product features?")
      .subscribe(res => {
        if (res) {
          this.isProductFeaturesSaving = true;
          this.products.product_edit_features(this.token, featuresObj).then(
            (data: any) => {
              if (data && data.success) {
                console.log("success: ", data);
                this.toastr.success(data.data, "SUCCESS");
                setTimeout(() => {
                  this.refresh();
                }, 300);
              } else if (data && !data.success) {
                this.toastr.warning(data.message, "WARNING");
              }
              this.isProductFeaturesSaving = false;
            },
            error => {
              console.log("error: ", error);
              if (error && !error.success) {
                this.toastr.error(error.error, "ERROR");
              }
              this.isProductFeaturesSaving = false;
            }
          );
        }
      });
  }

  setProductFeature(type) {
    let title: string;
    let action: string;

    if (type === "image") {
      title = "Set Feature Image";
      action = "feature_image";
    } else if (type === "video") {
      title = "Set Feature Video";
      action = "feature_video";
    } else if (type === "print") {
      title = "Set Feature Print Item";
      action = "feature_print";
    } else if (type === "logo") {
      title = "Set Feature Logo";
      action = "feature_logo";
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        if (reason === "save") {
          this.initData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: action,
      title: title,
      params: this.product
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(DatapickerComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  addProductImages() {
    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        if (reason === "save") {
          this.initData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "gallery",
      title: "Add Product Image",
      params: this.product
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(DatapickerComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  deleteProduct() {
    const deleteProduct = () => {
      this.products
        .product_delete(this.token, {
          productId: this.product.productId
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              this.toastr.success(data.data, "SUCCESS");
              this.router.navigate(["/products"]);
            }
          },
          error => {
            console.log("error: ", error);
          }
        );
    };

    this.coolDialogs
      .confirm("Are you sure to delete this product?")
      .subscribe(res => {
        if (res) {
          deleteProduct();
        }
      });
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
