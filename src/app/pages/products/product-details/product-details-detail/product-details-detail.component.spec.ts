import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDetailsDetailComponent } from './product-details-detail.component';

describe('ProductDetailsDetailComponent', () => {
  let component: ProductDetailsDetailComponent;
  let fixture: ComponentFixture<ProductDetailsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductDetailsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDetailsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
