import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as _ from "lodash";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { combineLatest, Subscription } from "rxjs";

import { ProductsService } from "../../services/products.service";
import { UtilitiesService } from "src/app/services/utilities.service";

import { ProductEntryComponent } from "./product-entry/product-entry.component";

@Component({
  selector: "app-products",
  templateUrl: "./products.component.html",
  styleUrls: ["./products.component.css"]
})
export class ProductsComponent implements OnInit {
  token: string;
  action: string;
  connect_params: any = {};

  business: any = {};
  productArr: any[] = [];
  productArrCopy: any[] = [];
  fakeArr: any[] = [];

  showContentProducts = false;
  showContentProductsErr = false;
  productsContentErr: any = {};

  pageProduct = 1;
  totalPageProduct = 0;
  perPage = 20;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private changeDetection: ChangeDetectorRef,
    private route: ActivatedRoute,
    public toastr: ToastrService,
    private modalService: BsModalService,
    private coolDialogs: NgxCoolDialogsService,
    public products: ProductsService,
    public utilities: UtilitiesService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
    // this.action = navParams.get('action');
    // this.connect_params = navParams.get('params');

    for (let i = 0; i < 30; ++i) {
      this.fakeArr.push(i);
    }
  }

  initData() {
    const successResponse = (data: any) => {
      if (data && data.success) {
        const products = data.data.products;
        this.pageProduct = parseInt(data.data.currentPage, 0);
        this.totalPageProduct = parseInt(data.data.totalPages, 0);

        for (let i = 0; i < _.size(products); i++) {
          products[i].selected = false;
          this.productArr.push(products[i]);
        }
        this.productArrCopy = _.cloneDeep(this.productArr);
      }
      this.showContentProducts = true;
    };

    const errorResponse = (error: any) => {
      this.showContentProducts = true;
      this.showContentProductsErr = true;
      if (error && !error.success) {
        this.productsContentErr = error;
      }
    };

    this.showContentProducts = false;
    if (this.action === "connect") {
      this.products
        .operator_products(this.token, this.connect_params.id, {
          page: this.pageProduct,
          total: this.perPage
        })
        .then(successResponse, errorResponse);
    } else {
      this.products
        .product_list(this.token, {
          page: this.pageProduct,
          total: this.perPage
        })
        .then(successResponse, errorResponse);
    }
  }

  ngOnInit() {
    this.pageProduct = 1;
    this.initData();
  }

  refresh() {
    this.pageProduct = 1;
    this.productArr = [];
    this.productArrCopy = [];

    this.initData();
  }

  gotoDetails(item) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        action: this.action,
        params: JSON.stringify(this.connect_params)
      },
      preserveFragment: true
    };

    this.router.navigate(["/products", item.productId], navigationExtras);
  }

  updateProduct(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    /* this.app.getRootNav().push(ProductsEntryPage, {
			product: item,
      action: 'update'
    }); */

    /*let modal = this.modalCtrl.create(ProductsEntryPage, {
			product: item,
			action: 'update'
		});
		modal.onDidDismiss((resp: any) => {
			if (resp == 'save') {
				this.doRefresh(null);
			}
		});
		modal.present();*/
  }

  deleteProduct(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const deleteProduct = () => {
      this.products
        .product_delete(this.token, {
          productId: item.productId
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              this.productArr = [];
              this.productArrCopy = [];
              this.refresh();
            }
          },
          error => {
            console.log("error: ", error);
          }
        );
    };

    this.coolDialogs.confirm("Delete selected product?").subscribe(res => {
      if (res) {
        console.log("You clicked OK. You dumb.");
        deleteProduct();
      } else {
        console.log("You clicked Cancel. You smart.");
      }
    });
  }

  createProduct() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.refresh();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "media",
      title: "Upload Image"
    };

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      ProductEntryComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
