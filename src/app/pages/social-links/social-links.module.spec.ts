import { SocialLinksModule } from './social-links.module';

describe('SocialLinksModule', () => {
  let socialLinksModule: SocialLinksModule;

  beforeEach(() => {
    socialLinksModule = new SocialLinksModule();
  });

  it('should create an instance', () => {
    expect(socialLinksModule).toBeTruthy();
  });
});
