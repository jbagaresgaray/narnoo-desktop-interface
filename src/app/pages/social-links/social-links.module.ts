import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { QuillModule } from "ngx-quill";
import { ModalModule, TabsModule, BsDropdownModule } from "ngx-bootstrap";
import { ClipboardModule } from "ngx-clipboard";

import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

import { SocialLinksComponent } from "./social-links.component";
import { EntryPageComponent } from "./entry-page/entry-page.component";

@NgModule({
  declarations: [SocialLinksComponent, EntryPageComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ModalModule,
    ClipboardModule,
    ComponentsModule,
    PipesModule,
    TabsModule,
    BsDropdownModule,
    QuillModule,
    ReactiveFormsModule
  ],
  entryComponents: [EntryPageComponent]
})
export class SocialLinksModule {}
