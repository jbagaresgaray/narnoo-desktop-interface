import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { BsDropdownModule, TabsModule, AccordionModule } from "ngx-bootstrap";

import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

import { ConnectedBusinessComponent } from "./connected-business.component";
import { ConnectedMediaComponent } from "./connected-media/connected-media.component";

import { ConnectedMediaImagesComponent } from "./connected-media/connected-media-images/connected-media-images.component";
import { ConnectedMediaLogosComponent } from "./connected-media/connected-media-logos/connected-media-logos.component";
import { ConnectedMediaPrintsComponent } from "./connected-media/connected-media-prints/connected-media-prints.component";
import { ConnectedMediaVideosComponent } from "./connected-media/connected-media-videos/connected-media-videos.component";
import { ConnectedMediaAlbumsComponent } from "./connected-media/connected-media-albums/connected-media-albums.component";
import { ConnectedMediaCollectionsComponent } from "./connected-media/connected-media-collections/connected-media-collections.component";
import { ConnectedMediaChannelsComponent } from "./connected-media/connected-media-channels/connected-media-channels.component";
import { ConnectedMediaProductsComponent } from "./connected-media/connected-media-products/connected-media-products.component";
import { ConnectedMediaCompanyBioComponent } from "./connected-media/connected-media-company-bio/connected-media-company-bio.component";
import { ConnectSettingsComponent } from "./connect-settings/connect-settings.component";
import { ConnectedPlatformComponent } from "./connected-platform/connected-platform.component";

@NgModule({
  imports: [
    CommonModule,
    PipesModule,
    ComponentsModule,
    BsDropdownModule,
    TabsModule,
    AccordionModule,
    RouterModule,
    FormsModule
  ],
  declarations: [
    ConnectedBusinessComponent,
    ConnectedMediaComponent,
    ConnectedMediaImagesComponent,
    ConnectedMediaLogosComponent,
    ConnectedMediaPrintsComponent,
    ConnectedMediaVideosComponent,
    ConnectedMediaAlbumsComponent,
    ConnectedMediaCollectionsComponent,
    ConnectedMediaChannelsComponent,
    ConnectedMediaProductsComponent,
    ConnectedMediaCompanyBioComponent,
    ConnectSettingsComponent,
    ConnectedPlatformComponent
  ],
  entryComponents: [ConnectSettingsComponent, ConnectedPlatformComponent]
})
export class ConnectedBusinessModule {}
