import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectedPlatformComponent } from './connected-platform.component';

describe('ConnectedPlatformComponent', () => {
  let component: ConnectedPlatformComponent;
  let fixture: ComponentFixture<ConnectedPlatformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectedPlatformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectedPlatformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
