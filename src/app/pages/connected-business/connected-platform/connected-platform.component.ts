import { Component, OnInit, NgZone, ViewEncapsulation } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import * as _ from "lodash";
import * as async from "async";

import { ByobService } from "src/app/services/byob.service";

@Component({
  selector: "app-connected-platform",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./connected-platform.component.html",
  styleUrls: ["./connected-platform.component.css"]
})
export class ConnectedPlatformComponent implements OnInit {
  connect_user: any = {};
  userName: string;
  token: string;
  selected: any = {};

  platformsArr: any[] = [];
  fakeArr: any[] = [];
  assignedArr: any = {};

  contentErr: string;

  isSaving: boolean = false;

  constructor(
    public zone: NgZone,
    public bsModalRef: BsModalRef,
    public toastr: ToastrService,
    public coolDialog: NgxCoolDialogsService,
    private modalService: BsModalService,
    public byobServices: ByobService
  ) {
    for (let index = 0; index < 5; index++) {
      this.fakeArr.push(index);
    }
  }

  ngOnInit() {
    this.token = localStorage.getItem("bus.token");
    console.log("connect_user: ", this.connect_user);
    if (this.connect_user) {
      if (this.connect_user.details) {
        this.userName =
          this.connect_user.details.business || this.connect_user.details.name;
        this.selected = this.connect_user.details;
      } else {
        this.userName = this.connect_user.name;
        this.selected = this.connect_user;
      }
    }

    async.waterfall(
      [
        callback => {
          this.byobServices.platform_options(this.selected.id, this.token).then(
            (data: any) => {
              if (data && data.success) {
                this.platformsArr = data.data.data;
                _.each(this.platformsArr, (row: any) => {
                  row.selected = false;
                });
              }
              console.log("platform_options: ", this.platformsArr);
              callback();
            },
            (error: any) => {
              console.log("platform_options error: ", error);
              callback();
            }
          );
        },
        callback => {
          this.byobServices
            .assigned_platform(this.selected.id, this.token)
            .then(
              (data: any) => {
                if (data && data.success) {
                  this.assignedArr = data.data;
                }
                console.log("assigned_platform: ", this.assignedArr);
                callback();
              },
              (error: any) => {
                console.log("assigned_platform error: ", error);
                if (error && !error.success) {
                  this.contentErr = error.message;
                }
                callback();
              }
            );
        }
      ],
      () => {
        if (!_.isEmpty(this.platformsArr)) {
          _.each(this.platformsArr, (row: any) => {
            if (row.nickname === this.assignedArr.nickname) {
              row.selected = true;
            } else {
              row.selected = false;
            }
          });
        } else {
          console.log("this.assignedArr: ", this.assignedArr);
          if (!_.isEmpty(this.assignedArr)) {
            this.assignedArr.selected = true;
            this.platformsArr.push(this.assignedArr);
          }
        }
        console.log("this.platformsArr: ", this.platformsArr);
      }
    );
  }

  selectItem(item: any) {
    _.each(this.platformsArr, (row: any) => {
      if (row.nickname === item.nickname) {
        row.selected = true;
      } else {
        row.selected = false;
      }
    });
    console.log("this.platformsArr: ", this.platformsArr);
  }

  saveChanges() {
    const _selected = _.find(this.platformsArr, (row: any) => {
      return row.selected === true;
    });

    if (_.isEmpty(_selected)) {
      this.toastr.warning("Please select a platform!", "WARNING");
      return;
    }

    this.coolDialog.confirm("Save changes?").subscribe(res => {
      if (res) {
        this.toastr.info("Saving...");
        this.isSaving = true;
        this.byobServices
          .platform_edit(
            { operator: this.selected.id, platform: _selected.nickname },
            this.token
          )
          .then(
            (data: any) => {
              if (data && data.success) {
                this.bsModalRef.hide();
              }
            },
            (error: any) => {
              console.log("platform_edit Error: ", error);
            }
          );
      }
    });
  }
}
