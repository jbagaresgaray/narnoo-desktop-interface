import { Component, OnInit, NgZone, ChangeDetectorRef } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as _ from "lodash";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { combineLatest, Subscription } from "rxjs";

import { ConnectService } from "../../services/connect.service";
import { UtilitiesService } from "../../services/utilities.service";

import { ConnectSettingsComponent } from "./connect-settings/connect-settings.component";
import { ConnectedPlatformComponent } from "./connected-platform/connected-platform.component";

@Component({
  selector: "app-connected-business",
  templateUrl: "./connected-business.component.html",
  styleUrls: ["./connected-business.component.css"]
})
export class ConnectedBusinessComponent implements OnInit {
  token: string;
  business: any = {};
  txtSearch: string = "";

  isFollowing: boolean = true;
  isFollower: boolean = false;
  isFind: boolean = false;

  showContent: boolean = false;
  showFollowingContent: boolean = false;
  showFollowerContent: boolean = false;

  followingArr: any[] = [];
  followingArrCopy: any[] = [];

  followerArr: any[] = [];
  followerArrCopy: any[] = [];

  usersArr: any[] = [];
  usersArrCopy: any[] = [];

  followingErr: any;
  followerErr: any;

  totalNotification: number = 0;

  fakeArr: any[] = [];

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    public zone: NgZone,
    public toastr: ToastrService,
    public cooldialogs: NgxCoolDialogsService,
    private modalService: BsModalService,
    private changeDetection: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    public connect: ConnectService,
    public utilities: UtilitiesService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    for (let i = 0; i < 21; ++i) {
      this.fakeArr.push(i);
    }

    this.followingErr = null;
    this.followerErr = null;
  }

  ngOnInit() {
    if (this.business.type === 1) {
      this.initData();
      this.getFollowers();
      this.getFollowing();
    } else {
      this.initData();
      this.getFollowing();
    }
  }

  private initData() {
    this.showContent = false;
    this.connect
      .connect_find(this.token)
      .then(
        (data: any) => {
          if (data && data.success) {
            this.usersArr = [];
            this.usersArrCopy = [];

            const filtered: any[] = _.filter(data.data, (row: any) => {
              return !row.details.connected;
            });

            this.usersArr = filtered;
            this.usersArrCopy = _.clone(filtered);
          }

          this.zone.run(() => {
            this.showContent = true;
          });
        },
        error => {
          console.log("connect_find error: ", error);
          this.zone.run(() => {
            this.showContent = true;
          });
        }
      )
      .then(() => {
        this.zone.run(() => {
          this.showContent = true;
        });
      });
  }

  private getFollowers() {
    this.showFollowerContent = false;
    this.connect
      .connect_followers(this.token)
      .then(
        (data: any) => {
          if (data && data.success) {
            this.followerArr = [];
            this.followerArrCopy = [];

            this.followerArr = data.data.data;
            this.followerArrCopy = _.clone(data.data.data);
          }
          this.zone.run(() => {
            this.showFollowerContent = true;
          });
        },
        error => {
          console.log("error: ", error);
          this.zone.run(() => {
            this.showFollowerContent = true;
          });
          if (error && !error.success) {
            this.followerErr = error.message;
          }
        }
      )
      .then(() => {
        this.zone.run(() => {
          this.showFollowerContent = true;
        });
      });
  }

  private getFollowing() {
    this.showFollowingContent = false;
    this.connect
      .connect_following(this.token)
      .then(
        (data: any) => {
          if (data && data.success) {
            this.followingArr = [];
            this.followingArrCopy = [];

            this.followingArr = data.data.data;
            this.followingArrCopy = _.clone(data.data.data);
          }
          this.zone.run(() => {
            this.showFollowingContent = true;
          });
        },
        (error: any) => {
          console.log("error: ", error);
          this.zone.run(() => {
            this.showFollowingContent = true;
          });
          if (error && !error.success) {
            this.followingErr = error.message;
          }
        }
      )
      .then(() => {
        this.zone.run(() => {
          this.showFollowingContent = true;
        });
      });
  }

  private followBusiness(user: any) {
    let selected: any = {};
    if (user.details) {
      selected = user.details;
    } else {
      selected = user;
    }

    this.toastr.info("Following...", "INFO");
    this.connect
      .connect_add(this.token, {
        type: selected.type,
        id: selected.id
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            this.toastr.success(data.data, "SUCCESS");
            setTimeout(() => {
              this.refresh();
            }, 600);
          } else if (data && !data.success) {
            this.toastr.warning(data.message, "WARNING");
          }
        },
        error => {
          console.log("error: ", error);
        }
      );
  }

  private unfollowBusiness(user: any) {
    let selected: any = {};
    if (user.details) {
      selected = user.details;
    } else {
      selected = user;
    }

    this.toastr.info("Unfollowing...", "INFO");
    this.connect
      .connect_remove(this.token, {
        type: selected.type,
        id: selected.id
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            this.toastr.success(data.data, "SUCCESS");
            setTimeout(() => {
              this.refresh();
            }, 600);
          } else if (data && !data.success) {
            this.toastr.warning(data.message, "WARNING");
          }
        },
        error => {
          console.log("error: ", error);
        }
      );
  }

  refresh() {
    if (this.isFind) {
      this.initData();
    } else if (this.isFollower) {
      this.getFollowers();
    } else if (this.isFollowing) {
      this.getFollowing();
    }
  }

  viewDetails(user: any) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        action: "connect",
        type: "image",
        params: JSON.stringify(user.details),
        user: JSON.stringify(user)
      },
      preserveFragment: true
    };

    if (this.isFollowing) {
      if (
        _.toLower(this.business.businessType) === "operator" &&
        user.details.type === "operator"
      ) {
        this.router.navigate(
          ["/connected-business/" + user.details.id + "/business"],
          navigationExtras
        );
      } else if (
        _.toLower(this.business.businessType) === "operator" &&
        user.details.type === "distributor"
      ) {
      } else if (
        _.toLower(this.business.businessType) === "distributor" &&
        user.details.type === "operator"
      ) {
        // Product List of Operator selected
        // Media Items ( images print logos video albums)
        this.router.navigate(
          ["/connected-business/" + user.details.id + "/business"],
          navigationExtras
        );
      } else if (
        _.toLower(this.business.businessType) === "distributor" &&
        user.details.type === "distributor"
      ) {
      }
    } else if (this.isFollower) {
      console.log("user: ", user);
      if (
        _.toLower(this.business.businessType) === "operator" &&
        user.details.type === "operator"
      ) {
        // Operator Settings Form
        this.router.navigate(
          ["/connected-business/" + user.details.id + "/business"],
          navigationExtras
        );
      } else if (
        _.toLower(this.business.businessType) === "operator" &&
        user.details.type === "distributor"
      ) {
        // Media Items ( images print logos video albums)
        this.router.navigate(
          ["/connected-business/" + user.details.id + "/business"],
          navigationExtras
        );
      } else if (
        _.toLower(this.business.businessType) === "distributor" &&
        user.details.type === "operator"
      ) {
      } else if (
        _.toLower(this.business.businessType) === "distributor" &&
        user.details.type === "distributor"
      ) {
      }
    }
  }

  connectUser(user: any, action: string) {
    let business: any;
    if (user.details) {
      business = user.details.business || user.details.name;
    } else {
      business = user.name;
    }

    if (action === "follow") {
      this.cooldialogs
        .confirm("Do you want to connect with " + business + "?")
        .subscribe(res => {
          if (res) {
            this.followBusiness(user);
          }
        });
    } else if (action === "unfollow") {
      this.cooldialogs
        .confirm("Do you want to unfollow " + business + "?")
        .subscribe(res => {
          if (res) {
            this.unfollowBusiness(user);
          }
        });
    }
  }

  viewPlatformSettings(user: any) {
    let business: any;
    let selected: any = {};
    if (user.details) {
      business = user.details.business;
      selected = user.details;
    } else {
      business = user.name;
      selected = user;
    }

    console.log("viewPlatformSettings: ", user);
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.refresh();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      connect_user: selected
    };

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      ConnectedPlatformComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  viewOperatorSettings(user: any) {
    let business: any;
    let selected: any = {};
    if (user.details) {
      business = user.details.business;
      selected = user.details;
    } else {
      business = user.name;
      selected = user;
    }
  }

  viewConnectionSettings(user: any) {
    console.log("viewConnectionSettings: ", user);
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.refresh();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "connect",
      connect_params: user
    };

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-sm",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      ConnectSettingsComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }

  showTab(action) {
    if (action === "following") {
      this.zone.run(() => {
        this.isFollowing = true;
        this.isFollower = false;
        this.isFind = false;
      });
    } else if (action === "follower") {
      this.zone.run(() => {
        this.isFollowing = false;
        this.isFollower = true;
        this.isFind = false;
      });
    } else if (action === "find") {
      this.zone.run(() => {
        this.isFollowing = false;
        this.isFollower = false;
        this.isFind = true;
      });
    }
  }

  onKeydown(event: any) {
    if (event.key === "Enter") {
      this.getItems();
    }
  }

  getItems() {
    const params: any = {};
    params.name = this.txtSearch;

    if (this.txtSearch && this.txtSearch.trim() !== "") {
      if (this.isFind) {
        this.showContent = false;
        this.connect.connect_search(this.token, params).then(
          (data: any) => {
            if (data && data.success) {
              const result = data.data;
              if (result && result[0] !== false) {
                this.usersArr = result;
                _.each(this.usersArr, (row: any) => {
                  row.details = {
                    connected: row.connected,
                    contact: row.contact,
                    country: row.country,
                    email: row.email,
                    id: row.id,
                    name: row.name,
                    phone: row.phone,
                    postcode: row.postcode,
                    state: row.state,
                    suburb: row.suburb,
                    type: row.type,
                    url: row.url
                  };
                });

                console.log("usersArr: ", this.usersArr);
              }
            }
            this.showContent = true;
          },
          error => {
            console.log("connect_find error: ", error);
            this.showContent = true;
          }
        );
      } else if (this.isFollowing) {
        this.followingArr = this.followingArrCopy.filter(item => {
          return (
            item.details.business
              .toLowerCase()
              .indexOf(this.txtSearch.toLowerCase()) > -1
          );
        });
      } else if (this.isFollower) {
        this.followerArr = this.followerArrCopy.filter(item => {
          return (
            item.details.business
              .toLowerCase()
              .indexOf(this.txtSearch.toLowerCase()) > -1
          );
        });
      }
    } else {
      this.usersArr = _.clone(this.usersArrCopy);
      this.followerArr = _.clone(this.followerArrCopy);
      this.followingArr = _.clone(this.followingArrCopy);
    }
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
