import { ConnectedBusinessModule } from "./connected-business.module";

describe("ConnectedBusinessModule", () => {
  let connectedBusinessModule: ConnectedBusinessModule;

  beforeEach(() => {
    connectedBusinessModule = new ConnectedBusinessModule();
  });

  it("should create an instance", () => {
    expect(connectedBusinessModule).toBeTruthy();
  });
});
