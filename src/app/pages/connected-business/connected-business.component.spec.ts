import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectedBusinessComponent } from './connected-business.component';

describe('ConnectedBusinessComponent', () => {
  let component: ConnectedBusinessComponent;
  let fixture: ComponentFixture<ConnectedBusinessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectedBusinessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectedBusinessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
