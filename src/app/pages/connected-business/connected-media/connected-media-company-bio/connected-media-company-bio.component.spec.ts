import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectedMediaCompanyBioComponent } from './connected-media-company-bio.component';

describe('ConnectedMediaCompanyBioComponent', () => {
  let component: ConnectedMediaCompanyBioComponent;
  let fixture: ComponentFixture<ConnectedMediaCompanyBioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectedMediaCompanyBioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectedMediaCompanyBioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
