import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as _ from "lodash";
import * as $ from "jquery";

import { BusinessService } from "../../../../services/business.service";

@Component({
  selector: "app-connected-media-company-bio",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./connected-media-company-bio.component.html",
  styleUrls: ["./connected-media-company-bio.component.css"]
})
export class ConnectedMediaCompanyBioComponent implements OnInit {
  token: string;
  business: any = {};
  connect_params: any = {};
  connect_user: any = {};
  action: string;
  type: string;

  isSummary = true;
  isDescription = false;

  biog: any[] = [];
  descriptionArr: any[] = [];
  summaryArr: any[] = [];

  showContent: boolean;
  showContentErr: boolean;
  showCheckbox: boolean;
  contentErr: any = {};

  constructor(
    public businesses: BusinessService,
    private router: Router,
    private route: ActivatedRoute,
    public toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }

  ngOnInit() {
    this.action = this.route.snapshot.queryParamMap.get("action");
    this.type = this.route.snapshot.queryParamMap.get("type");
    console.log("type: ", this.type);
    this.connect_params =
      JSON.parse(this.route.snapshot.queryParamMap.get("params")) || {};
    this.connect_user =
      JSON.parse(this.route.snapshot.queryParamMap.get("user")) || {};

    this.descriptionArr = [];
    this.summaryArr = [];

    this.showContent = false;
    this.showContentErr = false;
    this.showCheckbox = false;

    this.initEmptyData();
    this.loadCompanyBio();
  }

  private initEmptyData() {
    this.descriptionArr = [
      {
        sortorder: 1,
        language: "english",
        size: "description",
        text: ""
      },
      {
        sortorder: 2,
        language: "chinese",
        size: "description",
        text: ""
      },
      {
        sortorder: 3,
        language: "japanese",
        size: "description",
        text: ""
      }
    ];
    this.summaryArr = [
      {
        sortorder: 1,
        language: "english",
        size: "summary",
        text: ""
      },
      {
        sortorder: 2,
        language: "chinese",
        size: "summary",
        text: ""
      },
      {
        sortorder: 3,
        language: "japanese",
        size: "summary",
        text: ""
      }
    ];
  }

  private loadCompanyBio() {
    const successResponse = (data: any) => {
      if (data && data.success) {
        const biog = data.data;
        const result: any = _.filter(biog, { size: "description" });
        const _english = _.find(result, { language: "english" });
        const _chinese = _.find(result, { language: "chinese" });
        const _japanese = _.find(result, { language: "japanese" });

        if (_.isEmpty(_english)) {
          result.push({
            language: "english",
            size: "description",
            text: ""
          });
        }

        if (_.isEmpty(_chinese)) {
          result.push({
            language: "chinese",
            size: "description",
            text: ""
          });
        }

        if (_.isEmpty(_japanese)) {
          result.push({
            language: "japanese",
            size: "description",
            text: ""
          });
        }

        _.each(result, (row: any) => {
          row.selected = false;
          row.heading_title = row.language + " description";
        });
        this.descriptionArr = _.orderBy(result, ["sortorder"], ["asc"]);

        const result2: any = _.filter(biog, { size: "summary" });
        console.log("result2: ", result2);
        const _english2 = _.find(result2, { language: "english" });
        const _chinese2 = _.find(result2, { language: "chinese" });
        const _japanese2 = _.find(result2, { language: "japanese" });

        if (_.isEmpty(_english2)) {
          result2.push({
            language: "english",
            size: "summary",
            text: ""
          });
        }

        if (_.isEmpty(_chinese2)) {
          result2.push({
            language: "chinese",
            size: "summary",
            text: ""
          });
        }

        if (_.isEmpty(_japanese2)) {
          result2.push({
            language: "japanese",
            size: "summary",
            text: ""
          });
        }

        _.each(result2, (row: any) => {
          row.selected = false;
          row.heading_title = row.language + " summary";
        });
        this.summaryArr = _.orderBy(result2, ["sortorder"], ["asc"]);
      }
      this.showContent = true;
      this.showContentErr = false;
    };

    const errorResponse = error => {
      this.showContent = true;
      this.showContentErr = true;
      console.log("error: ", error);
      if (error && !error.success) {
        this.contentErr = error;
      }
    };

    if (this.connect_params.type === "operator") {
      this.businesses
        .business_biography_operator(this.token, this.connect_params.id)
        .then(successResponse, errorResponse);
    } else if (this.connect_params.type === "distributor") {
    }
  }
}
