import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectedMediaCollectionsComponent } from './connected-media-collections.component';

describe('ConnectedMediaCollectionsComponent', () => {
  let component: ConnectedMediaCollectionsComponent;
  let fixture: ComponentFixture<ConnectedMediaCollectionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectedMediaCollectionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectedMediaCollectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
