import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as _ from "lodash";
import * as $ from "jquery";

import { CollectionsService } from "../../../../services/collections.service";

@Component({
  selector: "app-connected-media-collections",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./connected-media-collections.component.html",
  styleUrls: ["./connected-media-collections.component.css"]
})
export class ConnectedMediaCollectionsComponent implements OnInit {
  token: string;
  business: any = {};
  connect_params: any = {};
  connect_user: any = {};
  action: string;
  type: string;

  collectionArr: any[] = [];
  collectionArrCopy: any[] = [];
  imagesLoadingArr: any[] = [];

  showContentCollections: boolean = false;
  showContentCollectionsErr: boolean = false;
  collectionsContentErr: any = {};
  pageCollection: number = 0;
  totalPageCollection: number = 0;

  perPage = 20;

  constructor(
    public collections: CollectionsService,
    private router: Router,
    private route: ActivatedRoute,
    public toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    for (let i = 0; i < 20; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  ngOnInit() {
    this.action = this.route.snapshot.queryParamMap.get("action");
    this.type = this.route.snapshot.queryParamMap.get("type");
    console.log("type: ", this.type);
    this.connect_params =
      JSON.parse(this.route.snapshot.queryParamMap.get("params")) || {};
    this.connect_user =
      JSON.parse(this.route.snapshot.queryParamMap.get("user")) || {};

    this.collectionArr = [];
    this.collectionArrCopy = [];

    this.pageCollection = 1;
    this.showContentCollections = false;
    this.showContentCollectionsErr = false;
    this.loadCollections(this.pageCollection, this.perPage);
  }

  private loadCollections(page, total) {
    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] !== false) {
        this.pageCollection = parseInt(data.data.page, 0);
        this.totalPageCollection = parseInt(data.data.totalPages, 0);
        for (let i = 0; i < _.size(data.data.data); i++) {
          data.data.data[i].selected = false;
          this.collectionArr.push(data.data.data[i]);
        }
        this.collectionArrCopy = _.cloneDeep(this.collectionArr);
      } else if (data && data.success && data.data[0] === false) {
        this.showContentCollectionsErr = true;
        this.collectionsContentErr = {
          message: "No results found"
        };
      }
      this.showContentCollections = true;
      this.showContentCollectionsErr = false;
    };

    const errorResponse = error => {
      console.log("error: ", error);

      this.showContentCollections = true;
      this.showContentCollectionsErr = true;

      if (error && !error.success) {
        this.collectionsContentErr = error;
        console.log("collectionsContentErr: ", this.collectionsContentErr);
      }
    };

    if (this.connect_params.type === "operator") {
      this.collections
        .collecton_operator_list(this.token, this.connect_params.id, {
          page: page,
          total: total
        })
        .then(successResponse, errorResponse);
    } else if (this.connect_params.type === "distributor") {
      this.collections
        .collecton_distributor_list(this.token, this.connect_params.id, {
          page: page,
          total: total
        })
        .then(successResponse, errorResponse);
    }
  }
}
