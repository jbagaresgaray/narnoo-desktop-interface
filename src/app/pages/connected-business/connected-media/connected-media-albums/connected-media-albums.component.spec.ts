import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectedMediaAlbumsComponent } from './connected-media-albums.component';

describe('ConnectedMediaAlbumsComponent', () => {
  let component: ConnectedMediaAlbumsComponent;
  let fixture: ComponentFixture<ConnectedMediaAlbumsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectedMediaAlbumsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectedMediaAlbumsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
