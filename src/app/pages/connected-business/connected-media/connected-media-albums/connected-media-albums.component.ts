import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as _ from "lodash";
import * as $ from "jquery";

import { AlbumsService } from "../../../../services/albums.service";

@Component({
  selector: "app-connected-media-albums",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./connected-media-albums.component.html",
  styleUrls: ["./connected-media-albums.component.css"]
})
export class ConnectedMediaAlbumsComponent implements OnInit {
  token: string;
  business: any = {};
  connect_params: any = {};
  connect_user: any = {};
  action: string;
  type: string;

  albumsArr: any[] = [];
  albumsArrCopy: any[] = [];
  imagesLoadingArr: any[] = [];
  selectedMedia: any[] = [];
  contextMenu: any[] = [];
  selectedItem: any = {};

  showContentAlbums: boolean = false;
  showContentAlbumsErr: boolean = false;
  albumsContentErr: any = {};

  pageAlbum: number = 0;
  totalPageAlbum: number = 0;
  showLoading = false;

  perPage = 50;

  constructor(
    public albums: AlbumsService,
    private router: Router,
    private route: ActivatedRoute,
    public toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    for (let i = 0; i < 30; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  ngOnInit() {
    this.action = this.route.snapshot.queryParamMap.get("action");
    this.type = this.route.snapshot.queryParamMap.get("type");
    console.log("type: ", this.type);
    this.connect_params =
      JSON.parse(this.route.snapshot.queryParamMap.get("params")) || {};
    this.connect_user =
      JSON.parse(this.route.snapshot.queryParamMap.get("user")) || {};

    this.pageAlbum = 1;
    this.albumsArr = [];
    this.albumsArrCopy = [];
    this.showContentAlbums = false;
    this.showContentAlbumsErr = false;

    this.showLoading = true;
    this.loadAlbums(this.pageAlbum, this.perPage);
  }

  private loadAlbums(page, total) {
    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] !== false) {
        this.pageAlbum = parseInt(data.data.currentPage, 0);
        this.totalPageAlbum = parseInt(data.data.totalPages, 0);
        for (let i = 0; i < _.size(data.data.albums); i++) {
          data.data.albums[i].selected = false;
          if (_.isEmpty(data.data.albums[i].title)) {
            data.data.albums[i].title = "No caption";
          }
          this.albumsArr.push(data.data.albums[i]);
        }
        this.albumsArrCopy = _.cloneDeep(this.albumsArr);
      } else if (data && data.success && data.data[0] === false) {
        this.showContentAlbumsErr = true;
        this.albumsContentErr = {
          message: "No results found"
        };
      }
      this.showContentAlbums = true;
      this.showLoading = false;
    };

    const errorResponse = error => {
      this.showContentAlbums = true;
      this.showContentAlbumsErr = true;
      this.showLoading = false;
      if (error && !error.success) {
        this.albumsContentErr = error;
        console.log("this.albumsContentErr: ", this.albumsContentErr);
      }
    };

    if (this.connect_params.type === "operator") {
      this.albums
        .album_operator_list(this.token, this.connect_params.id, {
          page: page,
          total: total
        })
        .then(successResponse, errorResponse);
    } else if (this.connect_params.type === "distributor") {
      this.albums
        .album_distributor_list(this.token, this.connect_params.id, {
          page: page,
          total: total
        })
        .then(successResponse, errorResponse);
    }
  }

  viewAlbumImages(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const navigationExtras: NavigationExtras = {
      queryParams: {
        action: "connect",
        album: JSON.stringify(item),
        params: JSON.stringify(this.connect_params)
      },
      preserveFragment: true
    };

    this.router.navigate(["/media/albums/", item.id], navigationExtras);
  }

  viewMore() {
    this.pageAlbum = this.pageAlbum + 1;
    if (this.pageAlbum <= this.totalPageAlbum) {
      this.showLoading = true;
      this.loadAlbums(this.pageAlbum, this.perPage);
    }
  }

  selectMedia(item: any, event) {
    item.selected = !item.selected;
    _.each(this.albumsArr, (row: any) => {
      if (row.id !== item.id) {
        row.selected = false;
      }
    });
    if (item.selected) {
      this.selectedItem = item;
    }
  }
}
