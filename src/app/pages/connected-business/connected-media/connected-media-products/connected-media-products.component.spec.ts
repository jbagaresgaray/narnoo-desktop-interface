import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectedMediaProductsComponent } from './connected-media-products.component';

describe('ConnectedMediaProductsComponent', () => {
  let component: ConnectedMediaProductsComponent;
  let fixture: ComponentFixture<ConnectedMediaProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectedMediaProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectedMediaProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
