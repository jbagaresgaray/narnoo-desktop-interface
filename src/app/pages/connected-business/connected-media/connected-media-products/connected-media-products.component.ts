import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as _ from "lodash";
import * as $ from "jquery";

import { ProductsService } from "../../../../services/products.service";
import { UtilitiesService } from "src/app/services/utilities.service";

@Component({
  selector: "app-connected-media-products",
  templateUrl: "./connected-media-products.component.html",
  styleUrls: ["./connected-media-products.component.css"]
})
export class ConnectedMediaProductsComponent implements OnInit {
  token: string;
  business: any = {};
  connect_params: any = {};
  connect_user: any = {};
  action: string;
  type: string;

  imagesLoadingArr: any[] = [];
  productArr: any[] = [];
  selectedMedia: any[] = [];
  contextMenu: any[] = [];
  selectedItem: any = {};

  showContentProducts: boolean = false;
  showContentProductsErr: boolean = false;
  productsContentErr: any = {};
  pageProduct: number = 0;
  totalPageProduct: number = 0;

  perPage: number = 20;

  constructor(
    public products: ProductsService,
    public utilities: UtilitiesService,
    private router: Router,
    private route: ActivatedRoute,
    public toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    for (let i = 0; i < 20; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  ngOnInit() {
    this.action = this.route.snapshot.queryParamMap.get("action");
    this.type = this.route.snapshot.queryParamMap.get("type");
    console.log("type: ", this.type);
    this.connect_params =
      JSON.parse(this.route.snapshot.queryParamMap.get("params")) || {};
    this.connect_user =
      JSON.parse(this.route.snapshot.queryParamMap.get("user")) || {};

    this.pageProduct = 1;
    this.productArr = [];
    this.showContentProducts = false;
    this.showContentProductsErr = false;
    this.loadProducts(this.connect_params.id);
  }

  private loadProducts(optId) {
    this.showContentProducts = false;
    this.products
      .operator_products(this.token, optId, {
        page: this.pageProduct,
        total: this.perPage
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            this.pageProduct = parseInt(data.data.currentPage, 0);
            this.totalPageProduct = parseInt(data.data.totalPages, 0);
            const products = data.data.data;
            for (let i = 0; i < _.size(products); i++) {
              products[i].selected = false;
              this.productArr.push(products[i]);
            }
          } else if (data && data.success && data.data[0] === false) {
            this.showContentProducts = true;
            this.productsContentErr = {
              message: "No results found"
            };
          }
          this.showContentProducts = true;
        },
        error => {
          this.showContentProducts = true;
          this.showContentProductsErr = true;
          if (error && !error.success) {
            this.productsContentErr = error;
          }
        }
      );
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }

  viewDetail(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const navigationExtras: NavigationExtras = {
      queryParams: {
        action: "connect",
        image: JSON.stringify(item),
        params: JSON.stringify(this.connect_params)
      },
      preserveFragment: true
    };

    this.router.navigate(["/products", item.productId], navigationExtras);
  }

  viewMore() {
    this.pageProduct = this.pageProduct + 1;
    if (this.pageProduct <= this.totalPageProduct) {
      this.loadProducts(this.connect_params.id);
    }
  }

  selectMedia(item: any, event) {
    item.selected = !item.selected;
    _.each(this.productArr, (row: any) => {
      if (row.id !== item.id) {
        row.selected = false;
      }
    });
    if (item.selected) {
      this.selectedItem = item;
    }
  }
}
