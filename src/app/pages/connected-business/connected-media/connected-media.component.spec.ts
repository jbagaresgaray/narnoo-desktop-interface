import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectedMediaComponent } from './connected-media.component';

describe('ConnectedMediaComponent', () => {
  let component: ConnectedMediaComponent;
  let fixture: ComponentFixture<ConnectedMediaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectedMediaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectedMediaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
