import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectedMediaPrintsComponent } from './connected-media-prints.component';

describe('ConnectedMediaPrintsComponent', () => {
  let component: ConnectedMediaPrintsComponent;
  let fixture: ComponentFixture<ConnectedMediaPrintsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectedMediaPrintsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectedMediaPrintsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
