import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as _ from "lodash";
import * as $ from "jquery";

import { PrintService } from "../../../../services/print.service";

@Component({
  selector: "app-connected-media-prints",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./connected-media-prints.component.html",
  styleUrls: ["./connected-media-prints.component.css"]
})
export class ConnectedMediaPrintsComponent implements OnInit {
  token: string;
  business: any = {};
  connect_params: any = {};
  connect_user: any = {};
  action: string;
  type: string;

  imagesLoadingArr: any[] = [];
  printArr: any[] = [];
  printCopyArr: any[] = [];
  selectedMedia: any[] = [];
  contextMenu: any[] = [];
  selectedItem: any = {};

  filters: any = {};
  showPreview: boolean = false;
  showLoading: boolean = false;

  showContentPrint: boolean = false;
  showContentPrintErr: boolean = false;
  printContentErr: any = {};

  pagePrint: number = 0;
  totalPagePrint: number = 0;
  fileAdded = 0;

  printstatus: string;
  filterType: string;

  perPage = 50;

  constructor(
    public prints: PrintService,
    private router: Router,
    private route: ActivatedRoute,
    public toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    for (let i = 0; i < 30; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  ngOnInit() {
    this.action = this.route.snapshot.queryParamMap.get("action");
    this.type = this.route.snapshot.queryParamMap.get("type");
    console.log("type: ", this.type);
    this.connect_params =
      JSON.parse(this.route.snapshot.queryParamMap.get("params")) || {};
    this.connect_user =
      JSON.parse(this.route.snapshot.queryParamMap.get("user")) || {};

    this.pagePrint = 1;
    this.printArr = [];
    this.printCopyArr = [];
    this.printstatus = "all";
    this.filterType = "All";

    this.showContentPrint = false;
    this.showContentPrintErr = false;
    this.showLoading = true;

    this.loadPrints(this.pagePrint, this.perPage);
  }

  loadPrints(page, total) {
    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] !== false) {
        this.pagePrint = parseInt(data.data.currentPage, 0);
        this.totalPagePrint = parseInt(data.data.totalPages, 0);
        for (let i = 0; i < _.size(data.data.prints); i++) {
          data.data.prints[i].selected = false;
          if (_.isEmpty(data.data.prints[i].brochure_caption)) {
            data.data.prints[i].brochure_caption = "No caption";
          }
          this.printArr.push(data.data.prints[i]);
        }
        this.printCopyArr = _.cloneDeep(this.printArr);
        this.printSegmentChanged();
      } else if (data && data.success && data.data[0] === false) {
        this.showContentPrintErr = true;
        this.printContentErr = {
          message: "No results found"
        };
      }
      this.showContentPrint = true;

      setTimeout(() => {
        $(".progressive-image").each(function() {
          const image = new Image();
          const previewImage = $(this).find(".loadingImage");
          const newImage = $(this).find(".overlay");
          image.src = previewImage.data("image");
          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(../assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
          };
        });
      }, 1000);
    };

    const errorResponse = error => {
      this.showContentPrint = true;
      this.showContentPrintErr = true;
      if (error && !error.success) {
        this.printContentErr = error;
      }
    };

    if (this.connect_params.type === "operator") {
      this.prints
        .brochure_operator_list(this.token, this.connect_params.id, {
          page: page,
          total: total
        })
        .then(successResponse, errorResponse);
    } else if (this.connect_params.type === "distributor") {
      this.prints
        .brochure_distributor_list(this.token, this.connect_params.id, {
          page: page,
          total: total
        })
        .then(successResponse, errorResponse);
    }
  }

  printSegmentChanged() {
    const date1 = new Date();
    date1.setHours(0, 0, 0, 0);

    this.printstatus = this.printstatus == "all" ? "expired" : "all";
    if (this.printstatus == "all") {
      this.printArr = _.cloneDeep(this.printCopyArr);
    } else if (this.printstatus == "expired") {
      this.printArr = _.filter(this.printCopyArr, (row: any) => {
        const date2 = new Date(row.validityDate);
        date2.setHours(0, 0, 0, 0);
        return date2 <= date1;
      });
    }
    console.log("this.printstatus: ", this.printstatus);

    setTimeout(() => {
      $(".progressive-image").each(function() {
        const image = new Image();
        const previewImage = $(this).find(".loadingImage");
        const newImage = $(this).find(".overlay");
        image.src = previewImage.data("image");
        image.onload = function() {
          newImage.css("background-image", "url(" + image.src + ")");
          newImage.css("opacity", "1");
        };
        image.onerror = function() {
          newImage.css("background-image", "url(../assets/img/thumb.jpg)");
          newImage.css("opacity", "1");
        };
      });
    }, 1000);
  }

  filterPrint(type) {
    const params: any = {};
    params.media = "print";
    params.type = type;

    this.filterType = type;

    this.showContentPrint = false;
    this.printArr = _.filter(this.printCopyArr, { type: params.type });
    setTimeout(() => {
      this.showContentPrint = true;

      setTimeout(() => {
        $(".progressive-image").each(function() {
          const image = new Image();
          const previewImage = $(this).find(".loadingImage");
          const newImage = $(this).find(".overlay");
          image.src = previewImage.data("image");
          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(../assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
          };
        });
      }, 300);
    }, 2000);
  }

  viewDetail(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const navigationExtras: NavigationExtras = {
      queryParams: {
        action: "connect",
        image: JSON.stringify(item),
        params: JSON.stringify(this.connect_params)
      },
      preserveFragment: true
    };

    this.router.navigate(["/media/prints/", item.id], navigationExtras);
  }

  viewMore() {
    this.pagePrint = this.pagePrint + 1;
    if (this.pagePrint <= this.totalPagePrint) {
      this.showLoading = true;
      this.loadPrints(this.pagePrint, this.perPage);
    }
  }

  selectMedia(item: any, event) {
    item.selected = !item.selected;
    _.each(this.printArr, (row: any) => {
      if (row.id !== item.id) {
        row.selected = false;
      }
    });
    if (item.selected) {
      this.selectedItem = item;
    }
  }
}
