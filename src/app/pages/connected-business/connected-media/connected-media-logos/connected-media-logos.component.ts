import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as _ from "lodash";
import * as $ from "jquery";

import { LogosService } from "../../../../services/logos.service";

@Component({
  selector: "app-connected-media-logos",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./connected-media-logos.component.html",
  styleUrls: ["./connected-media-logos.component.css"]
})
export class ConnectedMediaLogosComponent implements OnInit {
  token: string;
  business: any = {};
  connect_params: any = {};
  connect_user: any = {};
  action: string;
  type: string;

  imagesLoadingArr: any[] = [];
  logosArr: any[] = [];
  logosArrCopy: any[] = [];
  selectedMedia: any[] = [];
  contextMenu: any[] = [];
  selectedItem: any = {};

  filters: any = {};
  showPreview: boolean = false;

  showContentLogo: boolean = false;
  showContentLogoErr: boolean = false;
  showLoading: boolean = false;

  logoContentErr: any = {};
  pageLogo = 0;
  totalPageLogo = 0;
  fileAdded = 0;

  perPage = 50;

  constructor(
    public logos: LogosService,
    private router: Router,
    private route: ActivatedRoute,
    public toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    for (let i = 0; i < 30; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  ngOnInit() {
    this.action = this.route.snapshot.queryParamMap.get("action");
    this.type = this.route.snapshot.queryParamMap.get("type");
    console.log("type: ", this.type);
    this.connect_params =
      JSON.parse(this.route.snapshot.queryParamMap.get("params")) || {};
    this.connect_user =
      JSON.parse(this.route.snapshot.queryParamMap.get("user")) || {};

    this.pageLogo = 1;
    this.logosArr = [];
    this.logosArrCopy = [];
    this.showContentLogo = false;
    this.showContentLogoErr = false;
    this.showLoading = true;

    this.loadLogos(this.pageLogo, this.perPage);
  }

  loadLogos(page, total) {
    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] !== false) {
        this.pageLogo = parseInt(data.data.currentPage, 0);
        this.totalPageLogo = parseInt(data.data.totalPages, 0);
        for (let i = 0; i < _.size(data.data.logos); i++) {
          data.data.logos[i].selected = false;
          this.logosArr.push(data.data.logos[i]);
        }
        console.log("logosArr: ", this.logosArr);
      } else if (data && data.success && data.data[0] === false) {
        this.showContentLogoErr = true;
        this.logoContentErr = {
          message: "No results found"
        };
      }
      this.showContentLogo = true;
      this.showLoading = false;

      setTimeout(() => {
        $(".progressive-image").each(function() {
          const image = new Image();
          const previewImage = $(this).find(".loadingImage");
          const newImage = $(this).find(".overlay");
          image.src = previewImage.data("image");
          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(../assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
          };
        });
      }, 1000);
    };

    const errorResponse = (error: any) => {
      this.showContentLogo = true;
      this.showContentLogoErr = true;
      this.showLoading = false;

      if (error && !error.success) {
        this.logoContentErr = error;
      }
    };

    if (this.connect_params.type === "operator") {
      this.logos
        .logos_list_operator(this.token, this.connect_params.id, {
          page: page,
          total: total
        })
        .then(successResponse, errorResponse);
    } else if (this.connect_params.type === "distributor") {
      this.logos
        .logos_list_distributor(this.token, this.connect_params.id, {
          page: page,
          total: total
        })
        .then(successResponse, errorResponse);
    }
  }

  viewDetail(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const navigationExtras: NavigationExtras = {
      queryParams: {
        action: "connect",
        image: JSON.stringify(item),
        params: JSON.stringify(this.connect_params)
      },
      preserveFragment: true
    };

    this.router.navigate(["/media/logos/", item.id], navigationExtras);
  }

  viewMore() {
    this.pageLogo = this.pageLogo + 1;
    if (this.pageLogo <= this.totalPageLogo) {
      this.showLoading = true;
      this.loadLogos(this.pageLogo, this.perPage);
    }
  }

  selectMedia(item: any, event) {
    item.selected = !item.selected;
    _.each(this.logosArr, (row: any) => {
      if (row.id !== item.id) {
        row.selected = false;
      }
    });
    if (item.selected) {
      this.selectedItem = item;
    }
  }
}
