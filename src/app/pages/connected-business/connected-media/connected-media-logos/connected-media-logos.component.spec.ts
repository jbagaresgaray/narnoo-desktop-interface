import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectedMediaLogosComponent } from './connected-media-logos.component';

describe('ConnectedMediaLogosComponent', () => {
  let component: ConnectedMediaLogosComponent;
  let fixture: ComponentFixture<ConnectedMediaLogosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectedMediaLogosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectedMediaLogosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
