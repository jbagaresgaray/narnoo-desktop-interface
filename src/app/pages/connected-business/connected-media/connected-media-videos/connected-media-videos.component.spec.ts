import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectedMediaVideosComponent } from './connected-media-videos.component';

describe('ConnectedMediaVideosComponent', () => {
  let component: ConnectedMediaVideosComponent;
  let fixture: ComponentFixture<ConnectedMediaVideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectedMediaVideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectedMediaVideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
