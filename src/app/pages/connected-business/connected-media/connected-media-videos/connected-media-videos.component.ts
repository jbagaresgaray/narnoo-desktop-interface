import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as _ from "lodash";
import * as $ from "jquery";

import { VideosService } from "../../../../services/videos.service";

@Component({
  selector: "app-connected-media-videos",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./connected-media-videos.component.html",
  styleUrls: ["./connected-media-videos.component.css"]
})
export class ConnectedMediaVideosComponent implements OnInit {
  token: string;
  business: any = {};
  connect_params: any = {};
  connect_user: any = {};
  action: string;
  type: string;

  imagesLoadingArr: any[] = [];
  videosArr: any[] = [];
  videosArrCopy: any[] = [];
  selectedMedia: any[] = [];
  contextMenu: any[] = [];
  selectedItem: any = {};

  filters: any = {};
  showPreview: boolean = false;

  showContentVideos: boolean = false;
  showContentVideosErr: boolean = false;
  showLoading: boolean = false;
  videosContentErr: any = {};

  pageVideo: number = 0;
  totalPageVideo: number = 0;

  perPage = 50;

  constructor(
    public videos: VideosService,
    private router: Router,
    private route: ActivatedRoute,
    public toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    for (let i = 0; i < 30; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  ngOnInit() {
    this.action = this.route.snapshot.queryParamMap.get("action");
    this.type = this.route.snapshot.queryParamMap.get("type");
    console.log("type: ", this.type);
    this.connect_params =
      JSON.parse(this.route.snapshot.queryParamMap.get("params")) || {};
    this.connect_user =
      JSON.parse(this.route.snapshot.queryParamMap.get("user")) || {};

    this.videosArr = [];
    this.videosArrCopy = [];
    this.pageVideo = 1;
    this.showContentVideos = true;
    this.showContentVideosErr = false;
    this.showLoading = true;

    this.loadVideos(this.pageVideo, this.perPage);
  }

  loadVideos(page, total, refresher?: any, infinite?: any) {
    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] !== false) {
        this.pageVideo = parseInt(data.data.currentPage, 0);
        this.totalPageVideo = parseInt(data.data.totalPages, 0);
        for (let i = 0; i < _.size(data.data.videos); i++) {
          data.data.videos[i].selected = false;
          if (_.isEmpty(data.data.videos[i].caption)) {
            data.data.videos[i].caption = "No caption";
          }
          this.videosArr.push(data.data.videos[i]);
        }
        this.videosArrCopy = _.cloneDeep(this.videosArr);
      } else if (data && data.success && data.data[0] === false) {
        this.showContentVideosErr = true;
        this.videosContentErr = {
          message: "No results found"
        };
      }
      this.showContentVideos = true;
      this.showLoading = false;

      setTimeout(() => {
        $(".progressive-image").each(function() {
          const image = new Image();
          const previewImage = $(this).find(".loadingImage");
          const newImage = $(this).find(".overlay");
          image.src = previewImage.data("image");
          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(../assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
          };
        });
      }, 1000);
    };

    const errorResponse = error => {
      this.showContentVideos = true;
      this.showContentVideosErr = true;
      this.showLoading = false;

      if (error && !error.success) {
        this.videosContentErr = error;
      }
    };

    if (this.connect_params.type === "operator") {
      this.videos
        .video_operator_list(this.token, this.connect_params.id, {
          page: page,
          total: total
        })
        .then(successResponse, errorResponse);
    } else if (this.connect_params.type === "distributor") {
      this.videos
        .video_distributor_list(this.token, this.connect_params.id, {
          page: page,
          total: total
        })
        .then(successResponse, errorResponse);
    }
  }

  viewDetail(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const navigationExtras: NavigationExtras = {
      queryParams: {
        action: "connect",
        image: JSON.stringify(item),
        params: JSON.stringify(this.connect_params)
      },
      preserveFragment: true
    };

    this.router.navigate(["/media/videos/", item.id], navigationExtras);
  }

  viewMore() {
    this.pageVideo = this.pageVideo + 1;
    if (this.pageVideo <= this.totalPageVideo) {
      this.showLoading = true;
      this.loadVideos(this.pageVideo, this.perPage);
    }
  }

  selectMedia(item: any, event) {
    item.selected = !item.selected;
    _.each(this.videosArr, (row: any) => {
      if (row.id !== item.id) {
        row.selected = false;
      }
    });
    if (item.selected) {
      this.selectedItem = item;
    }
  }
}
