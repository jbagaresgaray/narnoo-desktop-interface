import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectedMediaChannelsComponent } from './connected-media-channels.component';

describe('ConnectedMediaChannelsComponent', () => {
  let component: ConnectedMediaChannelsComponent;
  let fixture: ComponentFixture<ConnectedMediaChannelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectedMediaChannelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectedMediaChannelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
