import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { combineLatest, Subscription } from "rxjs";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import * as _ from "lodash";
import * as $ from "jquery";

import { ChannelsService } from "../../../../services/channels.service";

@Component({
  selector: "app-connected-media-channels",
  templateUrl: "./connected-media-channels.component.html",
  styleUrls: ["./connected-media-channels.component.css"]
})
export class ConnectedMediaChannelsComponent implements OnInit {
  token: string;
  business: any = {};
  connect_params: any = {};
  connect_user: any = {};
  action: string;
  type: string;

  imagesLoadingArr: any[] = [];
  channelArr: any[] = [];
  selectedMedia: any[] = [];
  contextMenu: any[] = [];
  selectedItem: any = {};

  showContentChannels: boolean = false;
  showContentChannelsErr: boolean = false;
  showLoading: boolean = false;

  channelsContentErr: any = {};
  pageChannel = 0;
  totalPageChannel = 0;

  perPage = 20;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public channels: ChannelsService,
    public toastr: ToastrService,
    private changeDetection: ChangeDetectorRef,
    private coolDialogs: NgxCoolDialogsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    for (let i = 0; i < 10; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  ngOnInit() {
    this.action = this.route.snapshot.queryParamMap.get("action");
    this.type = this.route.snapshot.queryParamMap.get("type");
    console.log("type: ", this.type);
    this.connect_params =
      JSON.parse(this.route.snapshot.queryParamMap.get("params")) || {};
    this.connect_user =
      JSON.parse(this.route.snapshot.queryParamMap.get("user")) || {};

    this.channelArr = [];
    this.pageChannel = 1;
    this.totalPageChannel = 0;

    this.showContentChannels = false;
    this.showContentChannelsErr = false;
    this.showLoading = true;

    this.loadChannels(this.pageChannel, this.perPage);
  }

  private loadChannels(page, total) {
    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] !== false) {
        this.pageChannel = parseInt(data.data.page, 0);
        this.totalPageChannel = parseInt(data.data.totalPages, 0);
        for (let i = 0; i < _.size(data.data.data); i++) {
          data.data.data[i].selected = false;
          this.channelArr.push(data.data.data[i]);
        }
      } else if (data && data.success && data.data[0] === false) {
        this.showContentChannelsErr = true;
        this.channelsContentErr = {
          message: "No results found"
        };
      }
      this.showContentChannels = true;
      this.showLoading = false;

      setTimeout(() => {
        $(".progressive-image").each(function() {
          const image = new Image();
          const previewImage = $(this).find(".loadingImage");
          const newImage = $(this).find(".overlay");
          image.src = previewImage.data("image");
          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(../assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
          };
        });
      }, 1000);
    };

    const errorResponse = error => {
      this.showContentChannels = true;
      this.showContentChannelsErr = true;
      this.showLoading = false;
      if (error && !error.success) {
        this.channelsContentErr = error;
      }
    };

    if (this.connect_params.type === "operator") {
      this.channels
        .channel_operator_list(this.token, this.connect_params.id, {
          page: this.pageChannel,
          total: this.perPage
        })
        .then(successResponse, errorResponse);
    } else if (this.connect_params.type === "distributor") {
      this.channels
        .channel_distributor_list(this.token, this.connect_params.id, {
          page: this.pageChannel,
          total: this.perPage
        })
        .then(successResponse, errorResponse);
    }
  }

  viewDetail(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const navigationExtras: NavigationExtras = {
      queryParams: {
        action: "connect",
        image: JSON.stringify(item),
        params: JSON.stringify(this.connect_params)
      },
      preserveFragment: true
    };

    this.router.navigate(["/media/images/", item.id], navigationExtras);
  }

  viewMore() {
    this.pageChannel = this.pageChannel + 1;
    if (this.pageChannel <= this.totalPageChannel) {
      this.showLoading = true;
      this.loadChannels(this.pageChannel, this.perPage);
    }
  }

  selectMedia(item: any, event) {
    item.selected = !item.selected;
    _.each(this.channelArr, (row: any) => {
      if (row.id !== item.id) {
        row.selected = false;
      }
    });
    if (item.selected) {
      this.selectedItem = item;
    }
  }
}
