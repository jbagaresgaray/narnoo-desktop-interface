import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as _ from "lodash";
import * as $ from "jquery";

import { ImagesService } from "../../../../services/images.service";

@Component({
  selector: "app-connected-media-images",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./connected-media-images.component.html",
  styleUrls: ["./connected-media-images.component.css"]
})
export class ConnectedMediaImagesComponent implements OnInit {
  token: string;
  business: any = {};
  connect_params: any = {};
  connect_user: any = {};
  action: string;
  type: string;

  imagesArr: any[] = [];
  imagesLoadingArr: any[] = [];
  selectedMedia: any[] = [];
  contextMenu: any[] = [];
  selectedItem: any = {};

  showContentImage = false;
  showContentImageErr = false;
  showLoading = false;

  imageContentErr: any = {};
  pageImage: number = 0;
  totalPageImage: number = 0;
  fileAdded: number = 0;

  perPage = 50;

  constructor(
    public images: ImagesService,
    private router: Router,
    private route: ActivatedRoute,
    public toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    for (let i = 0; i < 30; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  ngOnInit() {
    this.action = this.route.snapshot.queryParamMap.get("action");
    this.type = this.route.snapshot.queryParamMap.get("type");
    console.log("type: ", this.type);
    this.connect_params =
      JSON.parse(this.route.snapshot.queryParamMap.get("params")) || {};
    this.connect_user =
      JSON.parse(this.route.snapshot.queryParamMap.get("user")) || {};

    this.pageImage = 1;
    this.imagesArr = [];
    this.showContentImage = false;
    this.showContentImageErr = false;
    this.showLoading = true;
    this.loadImage(this.pageImage, this.perPage);
  }

  private loadImage(page, total) {
    console.log("loadImage");
    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] !== false) {
        this.pageImage = parseInt(data.data.currentPage, 0);
        this.totalPageImage = parseInt(data.data.totalPages, 0);
        for (let i = 0; i < _.size(data.data.images); i++) {
          data.data.images[i].selected = false;

          if (_.isEmpty(data.data.images[i].caption)) {
            data.data.images[i].caption = "No Caption";
          }
          if (_.isEmpty(data.data.images[i].location)) {
            data.data.images[i].location = "No Location";
          }

          this.imagesArr.push(data.data.images[i]);
        }
      } else if (data && data.success && data.data[0] == false) {
        this.showContentImageErr = true;
        this.imageContentErr = {
          message: "No results found"
        };
      }
      this.showContentImage = true;
      this.showLoading = false;

      setTimeout(() => {
        $(".progressive-image").each(function() {
          const image = new Image();
          const previewImage = $(this).find(".loadingImage");
          const newImage = $(this).find(".overlay");
          image.src = previewImage.data("image");
          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(../assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
          };
        });
      }, 1000);
    };

    const errorResponse = (error: any) => {
      this.showContentImage = true;
      this.showContentImageErr = true;
      this.showLoading = false;

      if (error && !error.success) {
        this.imageContentErr = error;
      }
    };

    if (this.connect_params.type === "operator") {
      this.images
        .image_operator_list(this.token, this.connect_params.id, {
          page: page,
          total: total
        })
        .then(successResponse, errorResponse);
    } else if (this.connect_params.type === "distributor") {
      this.images
        .image_distributor_list(this.token, this.connect_params.id, {
          page: page,
          total: total
        })
        .then(successResponse, errorResponse);
    }
  }

  viewDetail(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const navigationExtras: NavigationExtras = {
      queryParams: {
        action: "connect",
        image: JSON.stringify(item),
        params: JSON.stringify(this.connect_params)
      },
      preserveFragment: true
    };

    this.router.navigate(["/media/images/", item.id], navigationExtras);
  }

  viewMore() {
    this.pageImage = this.pageImage + 1;
    if (this.pageImage <= this.totalPageImage) {
      this.showLoading = true;
      this.loadImage(this.pageImage, this.perPage);
    }
  }

  selectMedia(item: any, event) {
    item.selected = !item.selected;
    _.each(this.imagesArr, (row: any) => {
      if (row.id !== item.id) {
        row.selected = false;
      }
    });
    if (item.selected) {
      this.selectedItem = item;
    }
  }
}
