import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectedMediaImagesComponent } from './connected-media-images.component';

describe('ConnectedMediaImagesComponent', () => {
  let component: ConnectedMediaImagesComponent;
  let fixture: ComponentFixture<ConnectedMediaImagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectedMediaImagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectedMediaImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
