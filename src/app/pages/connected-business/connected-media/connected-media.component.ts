import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";

import { UtilitiesService } from "../../../services/utilities.service";

@Component({
  selector: "app-connected-media",
  templateUrl: "./connected-media.component.html",
  styleUrls: ["./connected-media.component.css"]
})
export class ConnectedMediaComponent implements OnInit {
  token: string;
  business: any = {};
  connect_params: any = {};
  connect_user: any = {};

  action: string;
  type: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public utilities: UtilitiesService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }

  ngOnInit() {
    this.initData();
  }

  back() {
    this.router.navigate(["/connected-business"]);
  }

  private initData() {
    this.action = this.route.snapshot.queryParamMap.get("action");
    this.type = this.route.snapshot.queryParamMap.get("type");
    this.connect_params =
      JSON.parse(this.route.snapshot.queryParamMap.get("params")) || {};
    this.connect_user =
      JSON.parse(this.route.snapshot.queryParamMap.get("user")) || {};

    console.log("action: ", this.action);
    console.log("type: ", this.type);
    console.log("connect_params: ", this.connect_params);
    console.log("connect_user: ", this.connect_user);
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }

  openMedia(type) {
    let action: string = "";

    if (type === "image") {
      action = "image";
    } else if (type === "logos") {
      action = "logos";
    } else if (type === "prints") {
      action = "prints";
    } else if (type === "videos") {
      action = "videos";
    } else if (type === "albums") {
      action = "albums";
    } else if (type === "collections") {
      action = "collections";
    } else if (type === "channels") {
      action = "channels";
    } else if (type === "products") {
      action = "products";
    } else if (type === "bio") {
      action = "bio";
    }

    const navigationExtras: NavigationExtras = {
      queryParams: {
        action: "connect",
        type: action,
        params: JSON.stringify(this.connect_params),
        user: JSON.stringify(this.connect_user)
      },
      preserveFragment: true
    };

    this.router
      .navigate(
        ["/connected-business/" + this.connect_params.id + "/business"],
        navigationExtras
      )
      .then(() => {
        this.initData();
      });
  }
}
