import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import * as _ from "lodash";
import { ConnectService } from "src/app/services/connect.service";

@Component({
  selector: "app-connect-settings",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./connect-settings.component.html",
  styleUrls: ["./connect-settings.component.css"]
})
export class ConnectSettingsComponent implements OnInit {
  public connect_params: any = {};
  public action: string;
  token: string;
  business: any = {};

  connectSettings: any;
  isSaving: boolean = false;

  constructor(
    public bsModalRef: BsModalRef,
    public toastr: ToastrService,
    public coolDialog: NgxCoolDialogsService,
    private modalService: BsModalService,
    public connects: ConnectService
  ) {
    this.token = localStorage.getItem("bus.token");
    // this.business = JSON.parse(localStorage.getItem("business")) || {};
  }

  ngOnInit() {
    if (this.connect_params.details) {
      this.business = this.connect_params.details.business;
    } else {
      this.business = this.connect_params.name;
    }
    console.log("this.connect_params: ", this.connect_params);
    console.log("this.business: ", this.business);
  }

  saveChanges() {
    if (_.isEmpty(this.connectSettings)) {
      this.toastr.warning("Please select a role!", "WARNING");
      return;
    }

    this.coolDialog.confirm("Save changes?").subscribe(res => {
      if (res) {
        this.toastr.info("Saving...");
        this.isSaving = true;
        this.connects
          .connect_edit(this.token, {
            type: this.connect_params.details.type,
            id: this.connect_params.details.id,
            role: this.connectSettings
          })
          .then(
            (data: any) => {
              if (data && data.success) {
                this.toastr.success(data.data, "SUCCESS");
                setTimeout(() => {
                  this.bsModalRef.hide();
                }, 1000);
                return;
              } else if (data && !data.success) {
                this.toastr.warning(data.message, "WARNING");
                return;
              }
              this.isSaving = false;
            },
            error => {
              console.log("connectSettings Error: ", error);
            }
          );
      }
    });
  }
}
