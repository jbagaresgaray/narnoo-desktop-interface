import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateBusinessComponent } from './create-business.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CreateBusinessComponent]
})
export class CreateBusinessModule { }
