import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import * as _ from "lodash";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { ContextMenuComponent, ContextMenuService } from "ngx-contextmenu";
import { BroadcasterService } from "ng-broadcaster";
import { Subscription } from "rxjs";
import * as $ from "jquery";

import { UsersService } from "../../services/users.service";
import { ConnectService } from "../../services/connect.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  businessArr: any[] = [];
  fakeArr: any[] = [];
  contextMenu: any[] = [];
  searchArr: any[] = [];

  showContent: boolean = false;
  showSearch: boolean = false;
  isGrid: boolean = false;

  selectedItem: any = {};
  txtSearch: string;

  @ViewChild(ContextMenuComponent)
  public basicMenu: ContextMenuComponent;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public services: UsersService,
    public utilities: UtilitiesService,
    private contextMenuService: ContextMenuService,
    private broadcaster: BroadcasterService,
    private connect: ConnectService
  ) {
    for (let i = 0; i < 12; i++) {
      this.fakeArr.push(i);
    }
  }

  ngOnInit() {
    $("body").removeClass("o-page--center");

    this.initData();
  }

  initData() {
    this.showContent = false;
    this.showSearch = false;
    this.services
      .businesses()
      .then(
        (data: any) => {
          if (data && data.success) {
            this.businessArr = [];

            if (_.isArray(data.accounts)) {
              this.businessArr = data.accounts;
              _.each(this.businessArr, (row: any) => {
                if (row.type === 1) {
                  row.businessType = "Operator";
                } else if (row.type === 2) {
                  row.businessType = "Distributor";
                }

                if (row.role === 1) {
                  row.businessRole = "Administrator";
                } else if (row.role === 2) {
                  row.businessRole = "Staff";
                } else if (row.role === 3) {
                  row.businessRole = "Trade";
                } else if (row.role === 4) {
                  row.businessRole = "Media";
                }
                row.selected = false;
              });
            }
            console.log("this.businessArr: ", this.businessArr);
          }
        },
        error => {
          console.log("error: ", error);
          this.showContent = true;
          if (error && !error.success && error.error === "Token has expired") {
            // this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'back' });
          }
        }
      )
      .then(() => {
        this.showContent = true;
        this.showSearch = false;
      });
  }

  createBusiness() {}

  refresh() {
    this.initData();
  }

  searchBusiness(value) {
    const params: any = {};
    params.name = value;

    this.showContent = false;
    this.showSearch = false;
    this.connect.search_businesses(params).then(
      (data: any) => {
        if (data && data.success) {
          const result = data.data;
          if (result && result[0] !== false) {
            this.searchArr = result;
            _.each(this.searchArr, (row: any) => {
              if (row.type === "operator") {
                row.businessType = "Operator";
              } else if (row.type === "distributor") {
                row.businessType = "Distributor";
              }
              row.business = row.name;
              row.selected = false;
            });
            console.log("this.businessArr: ", this.searchArr);
          }
        }
        this.showContent = false;
        this.showSearch = true;
      },
      (error: any) => {
        console.log("Error: ", error);
      }
    );
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }

  openDashboard(item) {
    localStorage.setItem("bus.token", item.token);
    localStorage.setItem("business", JSON.stringify(item));
    this.router.navigate(["/user-profile"]);
  }

  unlinkBusiness(business) {
    const ctrl = this;
    function requestAccess() {
      ctrl.services
        .user_unlink({ businessId: business.id, businessType: business.type })
        .then(
          (data: any) => {
            if (data && data.success) {
              ctrl.toastr.success(data.data, "SUCCESS");
              ctrl.initData();
            } else {
              ctrl.toastr.warning(data.message, "WARNING");
            }
          },
          error => {
            console.log("error: ", error);
          }
        );
    }

    this.coolDialogs
      .confirm("Are you sure to unlink account?")
      .subscribe(res => {
        if (res) {
          requestAccess();
        } else {
          console.log("You clicked Cancel. You smart.");
        }
      });
  }

  requestAccess(business) {
    console.log("business: ", business);
    const ctrl = this;

    function requestAccess() {
      ctrl.services
        .request_access({ accountId: business.id, type: business.type })
        .then((data: any) => {
          if (data && data.success) {
            ctrl.toastr.success(data.message, "SUCCESS");
          } else {
            ctrl.toastr.warning(data.message, "WARNING");
          }
        });
    }

    this.coolDialogs
      .confirm("Do you want to request access to this business account?")
      .subscribe(res => {
        if (res) {
          requestAccess();
        } else {
          console.log("You clicked Cancel. You smart.");
        }
      });
  }

  logOutApp() {
    this.coolDialogs
      .confirm("Are you sure to logout application?")
      .subscribe(res => {
        if (res) {
          localStorage.removeItem("app.adminData");
          localStorage.removeItem("app.admintoken");
          localStorage.removeItem("admin.bus.token");
          localStorage.removeItem("admin.business");

          setTimeout(() => {
            this.router.navigate(["/login"]);
          }, 600);
        }
      });
  }

  onKeydown(event) {
    if (event.key === "Enter") {
      console.log("searchBusiness");
      this.searchBusiness(this.txtSearch);
    }
  }
}
