import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";
import { ContextMenuModule } from "ngx-contextmenu";
import {
  BsDropdownModule,
  TabsModule,
  AccordionModule,
  PaginationModule
} from "ngx-bootstrap";

import { HomeComponent } from "./home.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    PipesModule,
    ContextMenuModule,
    ComponentsModule,
    BsDropdownModule,
    TabsModule,
    AccordionModule,
    PaginationModule
  ],
  declarations: [HomeComponent]
})
export class HomeModule {}
