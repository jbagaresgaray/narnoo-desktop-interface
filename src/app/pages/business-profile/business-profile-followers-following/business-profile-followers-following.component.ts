import { Component, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import * as _ from "lodash";
import * as async from "async";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { ToastrService } from "ngx-toastr";

import { BusinessService } from "../../../services/business.service";
import { UtilitiesService } from "../../../services/utilities.service";
import { ConnectService } from "../../../services/connect.service";

@Component({
  selector: "app-business-profile-followers-following",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./business-profile-followers-following.component.html",
  styleUrls: ["./business-profile-followers-following.component.css"]
})
export class BusinessProfileFollowersFollowingComponent implements OnInit {
  isFollowers = true;
  isFollowing = false;

  token: string;
  business: any = {};
  showContent: boolean = false;

  userFollowing: any[] = [];
  usersArr: any[] = [];
  fakeArr: any[] = [];

  @ViewChild("optionsList") optionsList;

  constructor(
    public bsModalRef: BsModalRef,
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private modalService: BsModalService,
    private businesses: BusinessService,
    public connect: ConnectService,
    public utilities: UtilitiesService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    for (let i = 0; i < 10; ++i) {
      this.fakeArr.push(i);
    }
  }

  ngOnInit() {
    this.showContent = false;
    async.parallel(
      [
        callback => {
          this.getFollowers().then(() => {
            callback();
          });
        },
        callback => {
          this.getFollowing().then(() => {
            callback();
          });
        }
      ],
      () => {
        this.showContent = true;
      }
    );
  }

  showTab(action) {
    if (action === "followers") {
      this.isFollowers = true;
      this.isFollowing = false;
    } else if (action === "following") {
      this.isFollowers = false;
      this.isFollowing = true;
    }
  }

  isOptionsVisible() {
    let visible = true;
    if (this.optionsList) {
      visible =
        this.optionsList.nativeElement.querySelectorAll("li").length > 0;
    }
    return visible;
  }

  private async getFollowers() {
    await this.connect.connect_followers(this.token).then(
      (data: any) => {
        if (data && data.success) {
          this.usersArr = [];

          const result = data.data.data;
          _.each(result, (row: any) => {
            if (
              _.find(this.userFollowing, (item: any) => {
                return item.details.id === row.details.id;
              })
            ) {
              row.details.connected = true;
            } else {
              row.details.connected = false;
            }
          });

          this.usersArr = result;
          console.log("getFollowers: ", this.usersArr);
        }
      },
      error => {
        console.log("error: ", error);
      }
    );
  }

  private async getFollowing() {
    await this.connect.connect_following(this.token).then(
      (data: any) => {
        if (data && data.success) {
          this.userFollowing = [];
          this.userFollowing = data.data.data;

          console.log("getFollowing: ", this.userFollowing);
        }
      },
      error => {
        console.log("error: ", error);
      }
    );
  }

  followBusiness(item: any) {
    if (this.business.type === 1 && item.details.type === "distributor") {
      this.toastr.warning(
        "Operator account can’t follow distributor account",
        "WARNING!"
      );
      return;
    }

    const followBusiness = () => {
      this.toastr.info("Following...", "INFO");
      this.connect
        .connect_add(this.token, {
          type: item.details.type,
          id: item.details.id
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              console.log("data: ", data);
              this.toastr.success(data.data, "SUCCESS");
              setTimeout(() => {
                if (this.isFollowers) {
                  this.getFollowers();
                } else if (this.isFollowing) {
                  this.getFollowing();
                }
              }, 1000);
            } else if (data && !data.success) {
              this.toastr.warning(data.data, "WARNING");
            }
          },
          error => {
            console.log("error: ", error);
          }
        );
    };

    this.coolDialogs
      .confirm("Do you want to connect with " + item.details.business + "?")
      .subscribe(res => {
        if (res) {
          followBusiness();
        }
      });
  }

  unfollowBusiness(item: any) {
    this.toastr.info("Unfollowing...", "INFO");
    this.connect
      .connect_remove(this.token, {
        type: item.details.type,
        id: item.details.id
      })
      .then(
        (data: any) => {
          console.log("data: ", data);
          if (data && data.success) {
            this.toastr.success(data.data, "SUCCESS");
            setTimeout(() => {
              if (this.isFollowers) {
                this.getFollowers();
              } else if (this.isFollowing) {
                this.getFollowing();
              }
            }, 1000);
          } else if (data && !data.success) {
            this.toastr.warning(data.data, "WARNING");
          }
        },
        error => {
          console.log("error: ", error);
        }
      );
  }

  editConnection(item: any) {
    if (item.details.type !== "distributor") {
      this.toastr.warning(
        "Only distributor are allowed on this feature!",
        "WARNING!"
      );
      return;
    }

    const connect_edit = (data: any) => {
      this.connect
        .connect_edit(this.token, {
          type: data.type,
          id: data.id,
          role: data.role
        })
        .then(
          (data1: any) => {
            if (data1 && data1.success) {
              this.toastr.success(data1.data, "SUCCESS!");
            } else if (data1 && !data1.success) {
              this.toastr.warning(data1.message, "WARNING!");
            }
          },
          error => {
            console.log("Error: ", error);
          }
        );
    };

    // this.coolDialogs.confirm("Modify business connection?").sub

    /* const actionSheet = this.actionSheetCtrl.create({
      title: "Modify business connection",
      buttons: [
        {
          text: "Full Role",
          handler: () => {
            console.log("Full Role clicked");
            connect_edit({
              type: item.details.type,
              id: item.details,
              role: "full"
            });
          }
        },
        {
          text: "Products",
          handler: () => {
            console.log("Products clicked");
            connect_edit({
              type: item.details.type,
              id: item.details,
              role: "products"
            });
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present(); */
  }
}
