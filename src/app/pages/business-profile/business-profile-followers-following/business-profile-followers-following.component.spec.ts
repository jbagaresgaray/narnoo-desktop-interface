import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessProfileFollowersFollowingComponent } from './business-profile-followers-following.component';

describe('BusinessProfileFollowersFollowingComponent', () => {
  let component: BusinessProfileFollowersFollowingComponent;
  let fixture: ComponentFixture<BusinessProfileFollowersFollowingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessProfileFollowersFollowingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessProfileFollowersFollowingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
