import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef,
  NgZone,
  ViewChild,
  ElementRef
} from "@angular/core";
import * as CryptoJS from "crypto-js";
import * as _ from "lodash";
import * as async from "async";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { combineLatest, Subscription } from "rxjs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import { UtilitiesService } from "../../services/utilities.service";
import { BusinessService } from "../../services/business.service";
import { ConnectService } from "../../services/connect.service";

import { BusinessProfileEntryComponent } from "./business-profile-entry/business-profile-entry.component";
// tslint:disable-next-line:max-line-length
import { BusinessProfileFollowersFollowingComponent } from "./business-profile-followers-following/business-profile-followers-following.component";

declare const google: any;

@Component({
  selector: "app-business-profile",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./business-profile.component.html",
  styleUrls: ["./business-profile.component.css"]
})
export class BusinessProfileComponent implements OnInit {
  token: string;
  showContent: boolean;
  business: any = {};
  profile: any = {};

  followerCount: number = 0;
  followingCount: number = 0;
  followingArr: any[] = [];

  map: any;
  @ViewChild("map") mapElement: ElementRef;
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    private zone: NgZone,
    private router: Router,
    private route: ActivatedRoute,
    private changeDetection: ChangeDetectorRef,
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private modalService: BsModalService,
    private businesses: BusinessService,
    private utilities: UtilitiesService,
    private connect: ConnectService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }

  ngOnInit() {
    this.initData();
  }

  private initData() {
    this.showContent = false;
    this.businesses.business_profile(this.token).then(
      (data: any) => {
        if (data && data.success) {
          this.profile = data.data;
          this.profile.gravatar =
            "https://www.gravatar.com/avatar/" +
            CryptoJS.MD5(this.profile.email.toLowerCase(), "hex");

          const keywords = data.data.keywords;
          if (keywords) {
            this.profile.tags = _.map(keywords.split(","), (row: any) => {
              return {
                display: row,
                value: row
              };
            });
          } else {
            this.profile.tags = null;
          }
          console.log("this.info : ", this.profile);

          async.parallel([
            (callback: any) => {
              this.connect.connect_followers(this.token).then(
                (data1: any) => {
                  if (data1 && data1.success) {
                    const result = data1.data.data;
                    this.followerCount = _.size(result);
                    console.log("this.followerCount: ", this.followerCount);
                  }
                  callback();
                },
                error => {
                  console.log("error: ", error);
                  this.followerCount = 0;
                  callback();
                }
              );
            },
            (callback: any) => {
              this.connect.connect_following(this.token).then(
                (data2: any) => {
                  if (data2 && data2.success) {
                    const result = data2.data.data;
                    this.followingCount = _.size(result);
                    this.followingArr = result;
                    console.log("this.followingCount: ", this.followingCount);
                  }
                  callback();
                },
                error => {
                  console.log("error: ", error);
                  this.followingCount = 0;
                  this.followingArr = [];
                  callback();
                }
              );
            }
          ]);

          this.loadInteractiveMap(
            this.profile.latitude,
            this.profile.longitude
          );
        }
        this.showContent = true;
      },
      (error: any) => {
        console.log("error: ", error);
        this.showContent = true;
      }
    );
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }

  private loadInteractiveMap(latitude, longitude) {
    if (typeof google === "object" && typeof google.maps === "object") {
      const mapOptions2 = {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 6,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      this.zone.runOutsideAngular(() => {
        this.map = new google.maps.Map(
          this.mapElement.nativeElement,
          mapOptions2
        );
        new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: this.map.getCenter()
        });
      });
    }
  }

  updateProfile() {
    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.initData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const modalConfig: any = {
      animated: true,
      class: "modal-lg"
    };

    this.bsModalRef = this.modalService.show(
      BusinessProfileEntryComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  viewFollowers() {
    const modalConfig: any = {
      animated: true,
      class: "modal-lg"
    };

    this.bsModalRef = this.modalService.show(
      BusinessProfileFollowersFollowingComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
