import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { ModalModule, BsDropdownModule } from "ngx-bootstrap";
import { TagInputModule } from "ngx-chips";
import { NgSelectModule } from "@ng-select/ng-select";

import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

import { BusinessProfileComponent } from "./business-profile.component";
import { BusinessProfileEntryComponent } from "./business-profile-entry/business-profile-entry.component";
// tslint:disable-next-line:max-line-length
import { BusinessProfileFollowersFollowingComponent } from "./business-profile-followers-following/business-profile-followers-following.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ModalModule,
    BsDropdownModule,
    ComponentsModule,
    PipesModule,
    TagInputModule,
    NgSelectModule,
    ReactiveFormsModule
  ],
  declarations: [
    BusinessProfileComponent,
    BusinessProfileEntryComponent,
    BusinessProfileFollowersFollowingComponent
  ],
  entryComponents: [
    BusinessProfileEntryComponent,
    BusinessProfileFollowersFollowingComponent
  ]
})
export class BusinessProfileModule {}
