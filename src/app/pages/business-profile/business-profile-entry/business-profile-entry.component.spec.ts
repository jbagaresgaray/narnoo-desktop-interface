import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessProfileEntryComponent } from './business-profile-entry.component';

describe('BusinessProfileEntryComponent', () => {
  let component: BusinessProfileEntryComponent;
  let fixture: ComponentFixture<BusinessProfileEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessProfileEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessProfileEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
