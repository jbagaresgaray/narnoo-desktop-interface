import {
  Component,
  OnInit,
  ViewEncapsulation,
  NgZone,
  ViewChild,
  ElementRef
} from "@angular/core";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { ToastrService } from "ngx-toastr";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import * as _ from "lodash";
import * as async from "async";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";

import { BusinessService } from "../../../services/business.service";
import { UtilitiesService } from "../../../services/utilities.service";
import { ConnectService } from "../../../services/connect.service";

declare const google: any;

@Component({
  selector: "app-business-profile-entry",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./business-profile-entry.component.html",
  styleUrls: ["./business-profile-entry.component.css"]
})
export class BusinessProfileEntryComponent implements OnInit {
  token: string;
  action: string;
  profile: any = {};
  isSaving: boolean;
  submitted: boolean;
  showContent: boolean;
  map: any;

  isSameAddress: boolean;
  needAccess: boolean;
  showSubcats: boolean;
  isCollapsedOperator: boolean;
  isCollapsedDistributor: boolean;
  isShowOperator: boolean;
  isShowDistributor: boolean;
  isSearching: boolean;
  isRequesting: boolean;

  distributorCat: any[] = [];
  operatorCat: any[] = [];
  platformsArr: any[] = [];
  subCategoryArr: any[] = [];
  countriesArr: any[] = [];

  entryForm: FormGroup;

  @ViewChild("map") mapElement: ElementRef;
  constructor(
    private formBuilder: FormBuilder,
    private zone: NgZone,
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    private businesses: BusinessService,
    public connect: ConnectService,
    public utilities: UtilitiesService
  ) {
    this.token = localStorage.getItem("bus.token");
  }

  ngOnInit() {
    this.isSaving = false;
    this.showContent = false;
    this.submitted = false;

    this.isSameAddress = false;
    this.needAccess = false;
    this.showSubcats = false;
    this.isCollapsedOperator = false;
    this.isCollapsedDistributor = false;
    this.isShowOperator = false;
    this.isShowDistributor = false;
    this.isSearching = false;
    this.isRequesting = false;

    this.entryForm = this.formBuilder.group({
      name: [{ value: "", disabled: true }, Validators.required],
      type: ["", Validators.required],
      category: ["", Validators.required],
      subCategory: ["", Validators.required],
      bookingPlatform: ["", Validators.required],
      contact: ["", Validators.required],
      email: ["", Validators.required],
      url: [""],
      phone: ["", Validators.required],
      tags: [""],
      postalNumber: ["", Validators.required],
      postalSuburb: ["", Validators.required],
      postalStreet: ["", Validators.required],
      postalState: ["", Validators.required],
      postalPostcode: ["", Validators.required],
      postalCountry: ["", Validators.required],
      physicalNumber: [""],
      physicalStreet: [""],
      physicalSuburb: [""],
      physicalState: [""],
      physicalPostcode: [""],
      physicalCountry: [""],
      isSameAddress: [false]
    });

    this.initData();
  }

  get f(): any {
    return this.entryForm.controls;
  }

  private toggleFormState() {
    const state = this.isSaving ? "disable" : "enable";
    Object.keys(this.entryForm.controls).forEach(controlName => {
      this.entryForm.controls[controlName][state](); // disables/enables each form control based on 'this.formDisabled'
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  private initData() {
    this.businesses.business_profile(this.token).then(
      (data: any) => {
        if (data && data.success) {
          this.profile = data.data;

          const keywords = data.data.keywords;
          this.profile.businessContact = this.profile.contact;

          if (keywords) {
            this.profile.tags = _.map(keywords.split(","), (row: any) => {
              return {
                display: row,
                value: row
              };
            });
          } else {
            this.profile.tags = [];
          }

          this.loadInteractiveMap(
            this.profile.latitude,
            this.profile.longitude
          );

          async.parallel(
            [
              callback => {
                this.utilities.getCountries().then(
                  (data4: any) => {
                    if (data4 && data4.success) {
                      this.countriesArr = data4.data;
                    }
                    callback();
                  },
                  error => {
                    console.log("error: ", error);
                  }
                );
              },
              callback => {
                this.utilities.category().then(
                  (data3: any) => {
                    if (data3 && data3.success) {
                      this.operatorCat = data3.data;
                      console.log("operatorCat: ", this.operatorCat);
                      callback();
                    } else {
                      callback();
                    }
                  },
                  error => {
                    callback();
                  }
                );
              },
              callback => {
                this.utilities.distributor_category().then(
                  (data1: any) => {
                    if (data1 && data1.success) {
                      this.distributorCat = data1.data;
                      console.log("distributorCat: ", this.distributorCat);
                      callback();
                    } else {
                      callback();
                    }
                  },
                  error => {
                    callback();
                  }
                );
              },
              callback => {
                this.utilities.booking_platforms().then(
                  (data2: any) => {
                    if (data2 && data2.success) {
                      this.platformsArr = data2.data;
                      console.log("platformsArr: ", this.platformsArr);
                      callback();
                    } else {
                      callback();
                    }
                  },
                  error => {
                    callback();
                  }
                );
              }
            ],
            () => {
              if (this.profile.type === "operator") {
                this.showOperators();
                const category: any = _.find(this.operatorCat, {
                  title: this.profile.category
                });
                if (category) {
                  // this.profile.categoryObj = category;
                  this.profile.category = category.id;
                  this.selectCategory(this.profile.category, "opt");
                }
              } else if (this.profile.type === "distributor") {
                this.showDistributor();

                const category: any = _.find(this.distributorCat, {
                  title: this.profile.category
                });
                if (category) {
                  // this.profile.categoryObj = category;
                  this.profile.category = category.id;
                  this.selectCategory(this.profile.category, "dist");
                }
              }

              this.entryForm.patchValue(this.profile);

              this.showSubcats = true;
              console.log("this.info : ", this.profile);
            }
          );
        }
        this.showContent = true;
      },
      (error: any) => {
        console.log("error: ", error);
        this.showContent = true;
      }
    );
  }

  onBusinessType(ev) {
    if (ev === "operator") {
      this.showOperators();
    } else if (ev === "distributor") {
      this.showDistributor();
    }
  }

  showOperators() {
    this.isShowOperator = true;
    this.isShowDistributor = false;
  }

  showDistributor() {
    this.isShowOperator = false;
    this.isShowDistributor = true;
  }

  selectCategory(index, type) {
    console.log("selectCategory: ", index);
    if (type === "opt") {
      const category = _.find(this.operatorCat, { id: index });
      console.log("category 1: ", category);
      if (category) {
        this.populateSubCategory(category.id);
      }
    } else if (type === "dist") {
      const category = _.find(this.distributorCat, { id: index });
      console.log("category 2: ", category);
      if (category) {
        this.populateSubCategory(category.id);
      }
    }
  }

  populateSubCategory(category) {
    this.toastr.info("Loading Subcategory...", "INFO");
    this.utilities.subcategory(category).then(
      (data: any) => {
        if (data && data.success) {
          this.subCategoryArr = data.data;
          this.showSubcats = true;
          console.log("this.subCategoryArr: ", this.subCategoryArr);

          if (this.profile.subCategory) {
            const subcategory = _.find(this.subCategoryArr, {
              title: this.profile.subCategory
            });
            if (subcategory) {
              this.profile.subCategory = subcategory.id;
            }
          }
        }
      },
      error => {
        console.log("error: ", error);
      }
    );
  }

  toggleAddress(e) {
    if (this.isSameAddress) {
      this.profile.physicalNumber = this.profile.postalNumber;
      this.profile.physicalStreet = this.profile.postalStreet;
      this.profile.physicalSuburb = this.profile.postalSuburb;
      this.profile.physicalState = this.profile.postalState;
      this.profile.physicalPostcode = this.profile.postalPostcode;
      this.profile.physicalCountry = this.profile.postalCountry;
    } else {
      this.profile.physicalNumber = null;
      this.profile.physicalStreet = null;
      this.profile.physicalSuburb = null;
      this.profile.physicalState = null;
      this.profile.physicalPostcode = null;
      this.profile.physicalCountry = null;
    }
  }

  loadInteractiveMap(latitude, longitude) {
    const handleEvent = event => {
      const markerlatlong: any = event.latLng;
      console.log("latt1", markerlatlong.lat());
      console.log("long1", markerlatlong.lng());
      this.profile.latitude = markerlatlong.lat();
      this.profile.longitude = markerlatlong.lng();
    };

    if (typeof google === "object" && typeof google.maps === "object") {
      const mapOptions2 = {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 6,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      this.zone.runOutsideAngular(() => {
        this.map = new google.maps.Map(
          this.mapElement.nativeElement,
          mapOptions2
        );
        const marker = new google.maps.Marker({
          animation: google.maps.Animation.DROP,
          position: this.map.getCenter(),
          draggable: true,
          optimized: false,
          zIndex: 5
        });
        marker.setMap(this.map);
        marker.addListener("drag", handleEvent);
        marker.addListener("dragend", handleEvent);
      });
    }
  }

  validateBusinessName() {
    const checkBusinessName = () => {
      this.isSearching = true;
      this.connect
        .search_businesses({
          name: this.profile.name
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              if (_.isArray(data.data) && data.data[0] === false) {
                this.needAccess = false;
                this.isSearching = false;
              } else if (_.isArray(data.data) && data.data[0] !== false) {
                this.needAccess = true;
                this.isSearching = false;

                this.profile = data.data[0];
              } else {
                this.needAccess = true;
                this.isSearching = false;
              }
            } else {
              this.isSearching = false;
            }
          },
          error => {
            this.needAccess = true;
            this.isSearching = false;
          }
        );
    };

    if (this.profile.name && this.profile.name.trim() !== "") {
      setTimeout(() => {
        checkBusinessName();
      }, 1000);
    } else {
      return;
    }
  }

  saveChanges() {
    this.submitted = true;

    if (this.entryForm.invalid) {
      this.isSaving = false;
      this.validateAllFormFields(this.entryForm);
      this.toggleFormState();
      this.toastr.warning("Fillup the required fields!");
      return;
    }

    if (!_.isEmpty(this.profile.tags)) {
      this.profile.keywords = _.map(this.profile.tags, (row: any) => {
        return row.value;
      }).join(",");
    }
    this.profile.businessContact = this.profile.contact;
    console.log("this.profile: ", this.profile);

    const updateBusinessProfile = () => {
      this.isSaving = true;
      this.toastr.info("Updating information...", "INFO");
      this.businesses.business_profile_edit(this.token, this.profile).then(
        (data: any) => {
          if (data && data.success) {
            this.toastr.success(data.data, "SUCCESS");
            this.modalService.setDismissReason("save");
            setTimeout(() => {
              this.bsModalRef.hide();
            }, 600);
          } else {
            this.toastr.warning(data.message, "WARNING");
          }
          this.isSaving = false;
          this.toggleFormState();
        },
        error => {
          console.log("error: ", error);
          this.isSaving = false;
          this.toggleFormState();
        }
      );
    };

    this.coolDialogs
      .confirm("Do you want to update changes?")
      .subscribe(res => {
        if (res) {
          this.isSaving = true;
          this.toggleFormState();

          updateBusinessProfile();
        }
      });
  }
}
