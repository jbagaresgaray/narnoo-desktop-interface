import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyBioEntryComponent } from './company-bio-entry.component';

describe('CompanyBioEntryComponent', () => {
  let component: CompanyBioEntryComponent;
  let fixture: ComponentFixture<CompanyBioEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyBioEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyBioEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
