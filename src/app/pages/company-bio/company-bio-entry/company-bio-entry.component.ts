import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  NgZone,
  ViewEncapsulation
} from "@angular/core";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { BusinessService } from "../../../services/business.service";

@Component({
  selector: "app-company-bio-entry",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./company-bio-entry.component.html",
  styleUrls: ["./company-bio-entry.component.css"]
})
export class CompanyBioEntryComponent implements OnInit {
  token: string;
  action: string;
  type: string;
  item: any = {};
  business: any = {};
  mode: string;
  textEditorConfig: any = {};

  isSaving: boolean;

  constructor(
    public bsModalRef: BsModalRef,
    private changeDetection: ChangeDetectorRef,
    public zone: NgZone,
    public toastr: ToastrService,
    public coolDialog: NgxCoolDialogsService,
    private modalService: BsModalService,
    private businesses: BusinessService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }

  ngOnInit() {
    this.isSaving = false;

    this.textEditorConfig = {
      toolbar: [
        ["bold", "italic", "underline", "strike"],
        [{ align: [] }],
        [{ list: "ordered" }, { list: "bullet" }],
        [{ size: ["small", false, "large", "huge"] }],
        [{ font: [] }]
      ]
    };
  }

  saveChanges() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const updateEntry = () => {
      this.toastr.info("Saving...", "INFO");
      this.businesses
        .business_biography_edit(this.token, this.item)
        .then((data: any) => {
          if (data && data.success) {
            console.log("success: ", data);
            this.toastr.success(data.data, "SUCCESS");
            this.modalService.setDismissReason("save");
            setTimeout(() => {
              this.bsModalRef.hide();
            }, 600);
          }
        })
        .catch(error => {
          console.log("error: ", error);
          if (error) {
            this.toastr.error(error.error, "ERROR!");
          }
        });
    };

    this.coolDialog.confirm("Do you want to save changes?").subscribe(res => {
      if (res) {
        updateEntry();
      }
    });
  }
}
