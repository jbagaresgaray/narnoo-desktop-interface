import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { QuillModule } from "ngx-quill";
import { FormsModule } from "@angular/forms";
import {
  CollapseModule,
  AccordionModule,
  TabsModule,
  BsDropdownModule
} from "ngx-bootstrap";
import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

import { CompanyBioComponent } from "./company-bio.component";
import { CompanyBioEntryComponent } from "./company-bio-entry/company-bio-entry.component";
import { CreatePageComponent } from "./create-page/create-page.component";

@NgModule({
  declarations: [
    CompanyBioComponent,
    CompanyBioEntryComponent,
    CreatePageComponent
  ],
  imports: [
    CommonModule,
    CollapseModule,
    AccordionModule,
    ComponentsModule,
    PipesModule,
    TabsModule,
    BsDropdownModule,
    FormsModule,
    QuillModule
  ],
  entryComponents: [CompanyBioEntryComponent, CreatePageComponent]
})
export class CompanyBioModule {}
