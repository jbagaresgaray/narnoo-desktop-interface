import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { ModalModule, BsDropdownModule } from "ngx-bootstrap";

import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

import { BusinessUsersComponent } from "./business-users.component";
import { UserEntryComponent } from "./user-entry/user-entry.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ModalModule,
    BsDropdownModule,
    ComponentsModule,
    PipesModule,
    ReactiveFormsModule
  ],
  declarations: [BusinessUsersComponent, UserEntryComponent],
  entryComponents: [UserEntryComponent]
})
export class BusinessUsersModule {}
