import { BusinessUsersModule } from './business-users.module';

describe('BusinessUsersModule', () => {
  let businessUsersModule: BusinessUsersModule;

  beforeEach(() => {
    businessUsersModule = new BusinessUsersModule();
  });

  it('should create an instance', () => {
    expect(businessUsersModule).toBeTruthy();
  });
});
