import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import {
  Router,
  ActivatedRoute,
  NavigationStart,
  NavigationEnd
} from "@angular/router";
import { UtilitiesService } from "../../../services/utilities.service";
import { BroadcasterService } from "ng-broadcaster";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { Subscription } from "rxjs";
import { filter } from "rxjs/operators";
import * as _ from "lodash";

declare let $: any;
declare var jQuery: any;

@Component({
  selector: "app-basiclayout",
  templateUrl: "./basiclayout.component.html",
  styleUrls: ["./basiclayout.component.css"]
})
export class BasiclayoutComponent implements OnInit {
  isCollapsed = true;
  hasSearch: boolean;
  appModule: string;
  profilePicture: string;
  txtSearch: string;
  currentFeature: string;

  users: any = {};
  business: any = {};
  businessName: string;

  private subscription: Subscription;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private coolDialogs: NgxCoolDialogsService,
    private utilities: UtilitiesService,
    private broadcaster: BroadcasterService
  ) {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe((res: any) => {
        const routeData: any = this.route.snapshot.firstChild.data;
        this.appModule = routeData.title;
        this.hasSearch = routeData.hasSearchbar;
        this.currentFeature = routeData.name;
      });
  }

  ngOnInit() {
    const $document = $(document),
      $sidebar = $(".js-page-sidebar"), // page sidebar itself
      $sidebarToggle = $(".c-sidebar-toggle"), // sidebar toggle icon
      $sidebarToggleContainer = $(".c-navbar"), // component that contains sidebar toggle icon
      $sidebarItem = $(".c-sidebar__item"),
      $sidebarSubMenu = $(".c-sidebar__submenu");

    $sidebarToggleContainer.on("click", function(e) {
      const $target = $(e.target);
      if ($target.closest($sidebarToggle).length) {
        $sidebar.addClass("is-visible");
        return false;
      }
    });

    // Bootstrap collapse.js plugin is used for handling sidebar submenu.
    $sidebarSubMenu.on("show.bs.collapse", function() {
      $(this)
        .parent($sidebarItem)
        .addClass("is-open");
    });

    $sidebarSubMenu.on("hide.bs.collapse", function() {
      $(this)
        .parent($sidebarItem)
        .removeClass("is-open");
    });

    $sidebarItem.on("click", function(e) {
      $sidebar.removeClass("is-visible");
    });
    this.getUser();
  }

  getUser() {
    this.business = JSON.parse(localStorage.getItem("business")) || {};
    if (this.business.avatar) {
      const avatarUrl = this.business.avatar.image200.startsWith("//")
        ? "http:" + this.business.avatar.image200
        : this.business.avatar.image200;

      this.profilePicture = avatarUrl;
    } else {
      this.profilePicture = this.utilities.radomizeAvatar();
    }
    this.businessName = !_.isEmpty(this.business.name)
      ? this.business.name
      : this.business.business;
  }

  activeRoute(routename: string): boolean {
    return this.router.url.indexOf(routename) > -1;
  }

  logOutApp() {
    this.coolDialogs
      .confirm("Are you sure to logout application?")
      .subscribe(res => {
        if (res) {
          localStorage.removeItem("app.adminData");
          localStorage.removeItem("app.admintoken");
          localStorage.removeItem("admin.bus.token");
          localStorage.removeItem("admin.business");

          setTimeout(() => {
            this.router.navigate(["/login"]);
          }, 600);
        }
      });
  }

  openHome() {
    this.router.navigate(["/home"]);
  }

  openDashboard() {
    this.router.navigate(["/dashboard"]);
  }

  openMedia(params: string) {
    if (params === "images") {
      this.router.navigate(["/media/images"]);
    } else if (params === "logos") {
      this.router.navigate(["/media/logos"]);
    } else if (params === "prints") {
      this.router.navigate(["/media/prints"]);
    } else if (params === "videos") {
      this.router.navigate(["/media/videos"]);
    } else if (params === "albums") {
      this.router.navigate(["/media/albums"]);
    } else if (params === "collections") {
      this.router.navigate(["/media/collections"]);
    } else if (params === "channels") {
      this.router.navigate(["/media/channels"]);
    }
  }

  openCompany() {
    this.router.navigate(["/company-bio"]);
  }

  openProducts() {
    this.router.navigate(["/products"]);
  }

  openImportProducts() {
    this.router.navigate(["/import-products"]);
  }

  openUsers() {
    this.router.navigate(["/users"]);
  }

  openSocialLinks() {
    this.router.navigate(["/social-links"]);
  }

  openUserProfile() {
    this.router.navigate(["/user-profile"]);
  }

  openConnectedBusiness() {
    this.router.navigate(["/connected-business"]);
  }

  openStaffs() {
    this.router.navigate(["/staffs"]);
  }

  openLocation() {
    this.router.navigate(["/locations"]);
  }

  openCreators() {
    this.router.navigate(["/creators"]);
  }
}
