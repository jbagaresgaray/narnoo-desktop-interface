import { Component, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as async from "async";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";

import { VideosService } from "../../../../services/videos.service";

@Component({
  selector: "app-video-entry",
  templateUrl: "./video-entry.component.html",
  styleUrls: ["./video-entry.component.css"]
})
export class VideoEntryComponent implements OnInit {
  item: any = {};
  business: any = {};
  token: string;

  isSaving: boolean;
  submitted: boolean;

  entryForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    public toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public videos: VideosService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }

  ngOnInit() {
    this.isSaving = false;
    this.submitted = false;

    if (this.item) {
      this.item.privacy = this.item.privilege;
    }

    this.entryForm = this.formBuilder.group({
      id: ["", Validators.required],
      caption: ["", Validators.required],
      privilege: ["", Validators.required],
      privacy: ["", Validators.required]
    });

    if (this.item) {
      this.entryForm.patchValue(this.item);
    }
  }

  get f(): any {
    return this.entryForm.controls;
  }

  private toggleFormState() {
    const state = this.isSaving ? "disable" : "enable";
    Object.keys(this.entryForm.controls).forEach(controlName => {
      this.entryForm.controls[controlName][state](); // disables/enables each form control based on 'this.formDisabled'
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  saveChanges() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.submitted = true;
    this.toggleFormState();

    if (this.entryForm.invalid) {
      this.isSaving = false;
      this.validateAllFormFields(this.entryForm);
      this.toggleFormState();
      this.toastr.warning("Fillup the required fields!");
      return;
    }

    const saveEntry = () => {
      this.toastr.info("SAVING...", "INFO");
      this.isSaving = true;
      this.videos.video_edit(this.token, this.entryForm.value).then(
        (data: any) => {
          if (data && data.success) {
            this.isSaving = false;
            this.submitted = false;
            this.toastr.success(data.data, "SUCCESS");
            this.modalService.setDismissReason("save");
            this.bsModalRef.hide();
          } else {
            this.isSaving = false;
            this.submitted = false;
            this.toastr.warning(data.messsage, "WARNING!");
          }
        },
        error => {
          console.log("error: ", error);
          this.isSaving = false;
          this.submitted = false;
        }
      );
    };

    this.coolDialogs.confirm("Do you want to save entry?").subscribe(res => {
      if (res) {
        saveEntry();
      }
    });
  }
}
