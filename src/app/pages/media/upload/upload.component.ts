import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  NgZone,
  ViewEncapsulation
} from "@angular/core";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import _ from "lodash";
import xml2js from "xml2js";
import {
  DropzoneComponent,
  DropzoneDirective,
  DropzoneConfigInterface
} from "ngx-dropzone-wrapper";

import { environment } from "../../../../environments/environment";

import { UploadService } from "../../../services/upload.service";

@Component({
  selector: "app-upload",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./upload.component.html",
  styleUrls: ["./upload.component.css"]
})
export class UploadComponent implements OnInit {
  action: string;
  title: string;
  token: string;
  uploadMode: string;
  printId: string;
  videoId: string;

  business: any = {};

  showLoading: boolean = false;
  isUploading: boolean = false;
  fileAdded: number = 0;

  accessKey: string;
  action_uri: string;
  file_name: string;
  policy: string;
  signature: string;
  file_key: string;
  contentType: string;
  successActionStatus: string;
  acl: string;

  public config: DropzoneConfigInterface = {};

  @ViewChild(DropzoneDirective)
  componentRef?: DropzoneDirective;
  constructor(
    public bsModalRef: BsModalRef,
    private changeDetection: ChangeDetectorRef,
    public zone: NgZone,
    public toastr: ToastrService,
    public coolDialog: NgxCoolDialogsService,
    private modalService: BsModalService,
    private uploadService: UploadService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }

  ngOnInit() {
    console.log("this.action: ", this.action);
    console.log("this.title: ", this.title);

    if (this.action === "media") {
      this.config.maxFilesize = environment.imageFileSize;
      this.generateImagePolicy();
    } else if (this.action === "logos") {
      this.config.maxFilesize = environment.logoFileSize;
      this.generateLogoPolicy();
    } else if (this.action === "print") {
      this.config.maxFilesize = environment.printFileSize;
      this.generatePrintPolicy();
      this.title = "Upload Brochure";
    } else if (this.action === "videos") {
      this.config.maxFilesize = environment.videoThumbnailSize;
      this.generateVideoPolicy();
      this.title = "Upload Video";
    } else if (this.action === "gallery") {
      if (this.uploadMode === "upload") {
        this.config.maxFilesize = environment.imageFileSize;
        this.generateImagePolicy();
      }
    } else if (this.action === "update_print_policy") {
      console.log("this.printId: ", this.printId);
      this.generateUpdatePrintPolicy(this.printId);
    } else if (this.action === "update_video_policy") {
      console.log("this.videoId: ", this.videoId);
      this.generateUpdateVideoPolicy(this.videoId);
    } else if (this.action === "update_video_thumbnail_policy") {
      console.log("this.videoId: ", this.videoId);
      this.generateUpdateVideoThumbnailPolicy(this.videoId);
    }
  }

  onUploadError(args: any): void {
    console.log("onUploadError:", args);
    if (args && _.isArray(args) && args[0].status === "error") {
      this.parseXML(args[1]).then((data: any) => {
        console.log("parseXML: ", data);
        if (data && data.Error) {
          this.toastr.error(data.Error.Message[0], "ERROR");
        } else {
          this.toastr.error(args[1], "ERROR");
        }
      });
      if (this.componentRef) {
        this.componentRef.dropzone().removeFile(args[0]);
      }
      return;
    } else if (args && _.isArray(args) && args[0].status === "canceled") {
      this.toastr.warning(args[1], "WARNING");
      return;
    }
    this.isUploading = false;
  }

  onUploadSuccess(args: any): void {
    console.log("onUploadSuccess:", args);
    this.isUploading = false;
    if (args && _.isArray(args) && args[0].status === "success") {
      this.toastr.success(args[1] || "File upload successfully!", "SUCCESS");
      setTimeout(() => {
        this.componentRef.dropzone().removeFile(args[0]);
      }, 1000);
    }
  }

  saveChanges(): void {
    this.isUploading = true;
    this.componentRef.dropzone().processQueue();
  }

  private configureDropZone() {
    const vm = this;
    this.config.url = this.action_uri;
    this.config.paramName = this.file_name;
    this.config.dictDefaultMessage =
      "Click Here or Drag & Drop files to instantly upload them.";

    this.config.init = function() {
      const dz = this;
      dz.on("sending", function(file, xhr, formData) {
        console.log("file: ", file);
        formData.append("acl", vm.acl);
        formData.append("Policy", vm.policy);
        formData.append("X-Amz-Signature", vm.signature);
        formData.append("AWSAccessKeyId", vm.accessKey);
        formData.append("Signature", vm.signature);
        formData.append("success_action_status", vm.successActionStatus);
        formData.append("Content-Type", file.type);
        formData.append("key", vm.file_key.split("/")[0] + "/" + file.name);
      });

      dz.on("addedfile", function(file) {
        console.log("file: ", file);
        if (vm.action === "update_print_policy" || vm.action === "print") {
          if (file.size < environment.printFileSize) {
            vm.zone.run(() => {
              vm.toastr.warning(
                "Your proposed upload file is smaller than the minimum allowed size of 500kb",
                "WARNING"
              );
              setTimeout(() => {
                vm.componentRef.dropzone().removeFile(file);
              }, 600);
            });
            return;
          }
        } else if (
          vm.action === "update_video_policy" ||
          vm.action === "videos"
        ) {
          if (file.size < environment.videoThumbnailSize) {
            vm.zone.run(() => {
              vm.toastr.warning(
                "Your proposed upload file is smaller than the minimum allowed size of 800kb",
                "WARNING"
              );
              setTimeout(() => {
                vm.componentRef.dropzone().removeFile(file);
              }, 600);
            });
            return;
          }
        } else if (vm.action === "media") {
          if (file.size < environment.imageFileSize) {
            console.log("WARNING");
            vm.zone.run(() => {
              vm.toastr.warning(
                "Your proposed upload file is smaller than the minimum allowed size of 800kb",
                "WARNING"
              );
              setTimeout(() => {
                vm.componentRef.dropzone().removeFile(file);
              }, 600);
            });
            return;
          }
        } else if (vm.action === "logos") {
          if (file.size < environment.logoFileSize) {
            vm.toastr.warning(
              "Your proposed upload file is smaller than the minimum allowed size of 100kb",
              "WARNING"
            );
            return;
          }
        } else if (vm.action === "gallery") {
          if (vm.uploadMode === "upload") {
            if (file.size < environment.imageFileSize) {
              vm.zone.run(() => {
                vm.toastr.warning(
                  "Your proposed upload file is smaller than the minimum allowed size of 800kb",
                  "WARNING"
                );
                setTimeout(() => {
                  vm.componentRef.dropzone().removeFile(file);
                }, 600);
              });
              return;
            }
          }
        }

        vm.changeDetection.detectChanges();
      });

      dz.on("success", function(file) {
        setTimeout(() => {
          vm.componentRef.dropzone().removeFile(file);
        }, 1000);
      });

      dz.on("complete", function(file) {
        console.log("complete");
        setTimeout(() => {
          vm.zone.run(() => {
            vm.modalService.setDismissReason("save");
            vm.bsModalRef.hide();
          });
        }, 600);
      });
    };
  }

  private parseXML(data) {
    return new Promise(resolve => {
      const parser = new xml2js.Parser({
        trim: true
      });

      parser.parseString(data, function(err, result) {
        console.log("result: ", result);
        resolve(result);
      });
    });
  }

  private generateImagePolicy() {
    this.showLoading = true;
    this.uploadService
      .upload_image_policy(this.token)
      .then(
        (data: any) => {
          if (data && data.success) {
            console.log("upload_image_policy: ", data.data);
            this.accessKey = data.data.accessKey;
            this.action_uri = data.data.action_uri;
            this.file_name = data.data.name;
            this.file_key = data.data.key;
            this.policy = data.data.policy;
            this.signature = data.data.signature;
            this.contentType = data.data.contentType;
            this.successActionStatus = data.data.successActionStatus;
            this.acl = data.data.acl;
          }
          this.showLoading = false;
          this.configureDropZone();
        },
        error => {
          console.error("error: ", error);
          this.showLoading = false;
        }
      )
      .catch(error => {
        console.error("error: ", error);
        this.showLoading = false;
      });
  }

  private generatePrintPolicy() {
    this.showLoading = true;
    this.uploadService
      .upload_print_policy(this.token)
      .then(
        (data: any) => {
          if (data && data.success) {
            this.accessKey = data.data.accessKey;
            this.action_uri = data.data.action_uri;
            this.file_name = data.data.name;
            this.file_key = data.data.key;
            this.policy = data.data.policy;
            this.signature = data.data.signature;
            this.contentType = data.data.contentType;
            this.successActionStatus = data.data.successActionStatus;
            this.acl = data.data.acl;
          }
          this.showLoading = false;
          this.configureDropZone();
        },
        error => {
          console.error("error: ", error);
          this.showLoading = false;
        }
      )
      .catch(error => {
        console.error("error: ", error);
        this.showLoading = false;
      });
  }

  private generateLogoPolicy() {
    this.showLoading = true;
    this.uploadService
      .upload_logo_policy(this.token)
      .then(
        (data: any) => {
          if (data && data.success) {
            this.accessKey = data.data.accessKey;
            this.action_uri = data.data.action_uri;
            this.file_name = data.data.name;
            this.file_key = data.data.key;
            this.policy = data.data.policy;
            this.signature = data.data.signature;
            this.contentType = data.data.contentType;
            this.successActionStatus = data.data.successActionStatus;
            this.acl = data.data.acl;
          }
          this.showLoading = false;
          this.configureDropZone();
        },
        error => {
          console.error("error: ", error);
          this.showLoading = false;
        }
      )
      .catch(error => {
        console.error("error: ", error);
        this.showLoading = false;
      });
  }

  private generateVideoPolicy() {
    this.showLoading = true;
    this.uploadService
      .upload_video_policy(this.token)
      .then(
        (data: any) => {
          if (data && data.success) {
            console.log("upload_video_policy: ", data);
            this.accessKey = data.data.accessKey;
            this.action_uri = data.data.action_uri;
            this.file_name = data.data.name;
            this.file_key = data.data.key;
            this.policy = data.data.policy;
            this.signature = data.data.signature;
            this.contentType = data.data.contentType;
            this.successActionStatus = data.data.successActionStatus;
            this.acl = data.data.acl;
          }
          this.showLoading = false;
          this.configureDropZone();
        },
        error => {
          console.error("error: ", error);
          this.showLoading = false;
        }
      )
      .catch(error => {
        console.error("error: ", error);
        this.showLoading = false;
      });
  }

  private generateUpdatePrintPolicy(printId: any) {
    this.showLoading = true;
    this.uploadService.update_print_policy(this.token, printId).then(
      (data: any) => {
        if (data && data.success) {
          console.log("update_print_policy: ", data);
          this.accessKey = data.data.accessKey;
          this.action_uri = data.data.action_uri;
          this.file_name = data.data.name;
          this.file_key = data.data.key;
          this.policy = data.data.policy;
          this.signature = data.data.signature;
          this.contentType = data.data.contentType;
          this.successActionStatus = data.data.successActionStatus;
          this.acl = data.data.acl;
        }
        this.showLoading = false;
        this.configureDropZone();
      },
      error => {
        console.error("error: ", error);
        this.showLoading = false;
      }
    );
  }

  private generateUpdateVideoPolicy(videoId: string) {
    this.showLoading = true;
    this.uploadService.update_video_policy(this.token, videoId).then(
      (data: any) => {
        if (data && data.success) {
          console.log("update_video_policy: ", data);
          this.accessKey = data.data.accessKey;
          this.action_uri = data.data.action_uri;
          this.file_name = data.data.name;
          this.file_key = data.data.key;
          this.policy = data.data.policy;
          this.signature = data.data.signature;
          this.contentType = data.data.contentType;
          this.successActionStatus = data.data.successActionStatus;
          this.acl = data.data.acl;
        }
        this.showLoading = false;
        this.configureDropZone();
      },
      error => {
        console.error("error: ", error);
        this.showLoading = false;
      }
    );
  }

  private generateUpdateVideoThumbnailPolicy(videoId: string) {
    this.showLoading = true;
    this.uploadService.update_video_thumbnail_policy(this.token, videoId).then(
      (data: any) => {
        if (data && data.success) {
          console.log("update_video_thumbnail_policy: ", data);
          this.accessKey = data.data.accessKey;
          this.action_uri = data.data.action_uri;
          this.file_name = data.data.name;
          this.file_key = data.data.key;
          this.policy = data.data.policy;
          this.signature = data.data.signature;
          this.contentType = data.data.contentType;
          this.successActionStatus = data.data.successActionStatus;
          this.acl = data.data.acl;
        }
        this.showLoading = false;
        this.configureDropZone();
      },
      error => {
        console.error("error: ", error);
        this.showLoading = false;
      }
    );
  }
  g;
}
