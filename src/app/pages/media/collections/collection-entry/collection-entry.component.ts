import { Component, OnInit } from "@angular/core";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import { CollectionsService } from "../../../../services/collections.service";

@Component({
  selector: "app-collection-entry",
  templateUrl: "./collection-entry.component.html",
  styleUrls: ["./collection-entry.component.css"]
})
export class CollectionEntryComponent implements OnInit {
  collection: any = {};
  business: any = {};
  token: string;
  action: string;

  showLoading: boolean = false;

  constructor(
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    public collections: CollectionsService,
    public toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }

  ngOnInit() {
    console.log("action: ", this.action);
  }

  saveEntry() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.coolDialogs.confirm("Save collection entry?").subscribe(res => {
      if (res) {
        if (this.action === "update") {
          this.updateEntry();
        } else if (this.action === "create") {
          this.createEntry();
        }
      }
    });
  }

  private createEntry() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.showLoading = true;

    const formData = new FormData();
    formData.append("title", this.collection.title);

    this.collections.collection_create(formData, this.token).then(
      (data: any) => {
        if (data && data.success) {
          this.modalService.setDismissReason("save");
          this.toastr.success(
            data.message || "Collection successfully created",
            "SUCCESS"
          );

          setTimeout(() => {
            this.bsModalRef.hide();
          }, 600);
        } else {
          this.toastr.warning(data.message, "WARNING");
        }
        this.showLoading = false;
      },
      (error: any) => {
        console.log("error: ", error);
        this.showLoading = false;
        if (error && !error.success) {
          this.toastr.error(error.message, "ERROR");
          return;
        }
      }
    );
  }

  private updateEntry() {
    this.showLoading = true;

    this.collections.collection_edit(this.collection, this.token).then(
      (data: any) => {
        if (data && data.success) {
          this.modalService.setDismissReason("save");
          this.toastr.success(data.data, "SUCCESS");

          setTimeout(() => {
            this.bsModalRef.hide();
          }, 600);
        } else {
          this.toastr.warning(data.message, "WARNING");
        }
        this.showLoading = false;
      },
      (error: any) => {
        console.log("error: ", error);
        this.showLoading = false;
        if (error && !error.success) {
          this.toastr.error(error.message, "ERROR");
        }
      }
    );
  }
}
