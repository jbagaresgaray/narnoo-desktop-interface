import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Location } from "@angular/common";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { combineLatest, Subscription } from "rxjs";

import * as _ from "lodash";
import * as async from "async";
import * as $ from "jquery";

import { CollectionsService } from "../../../../services/collections.service";

import { CollectionEntryComponent } from "../collection-entry/collection-entry.component";
import { PickuplistComponent } from "../../pickuplist/pickuplist.component";

@Component({
  selector: "app-collection-details",
  templateUrl: "./collection-details.component.html",
  styleUrls: ["./collection-details.component.css"]
})
export class CollectionDetailsComponent implements OnInit {
  collectionId: any;
  business: any = {};
  token: string;
  collection: any = {};
  slides: any;
  selectedSegment: string = "image";
  imagesLoadingArr: any[] = [];

  action: string;
  connect_params: any = {};

  showImageContent: boolean = false;
  showCheckbox: boolean = false;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    public toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private _location: Location,
    private route: ActivatedRoute,
    private modalService: BsModalService,
    private changeDetection: ChangeDetectorRef,
    private router: Router,
    public collections: CollectionsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    this.collection.images = [];
    this.collection.prints = [];
    this.collection.videos = [];
    this.collection.products = [];

    for (let i = 0; i < 20; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  ngOnInit() {
    this.action = this.route.snapshot.queryParamMap.get("action");
    this.connect_params = this.route.snapshot.queryParamMap.get("params");
    this.collectionId = this.route.snapshot.paramMap.get("id");

    console.log("this.collectionId: ", this.collectionId);
    console.log("this.action: ", this.action);

    if (this.action === "connect") {
    } else {
      // this.showImageContent = false;
      this.initData();
    }
  }

  initData() {
    this.showImageContent = false;
    this.collections.collecton_details(this.collectionId, this.token).then(
      (data: any) => {
        if (data && data.success) {
          this.collection = data.data;
          this.collection.totalImages = this.collection.totalImages || 0;
          this.collection.totalPrints = this.collection.totalPrints || 0;
          this.collection.totalVideos = this.collection.totalVideos || 0;

          /*if (this.collection.dateCreated) {
					if (this.platform.is('ios')) {
						this.collection.dateCreated = new Date(this.collection.dateCreated.replace(/-/g, "/"));
					}
				}

				if (this.collection.dateModified) {
					if (this.platform.is('ios')) {
						this.collection.dateModified = new Date(this.collection.dateModified.replace(/-/g, "/"));
					}
				}*/

          _.each(this.collection.images, (row: any) => {
            row.selected = false;
          });

          _.each(this.collection.prints, (row: any) => {
            row.selected = false;
          });

          _.each(this.collection.videos, (row: any) => {
            row.selected = false;
          });
          this.showImageContent = true;

          setTimeout(() => {
            $(".progressive-image").each(function() {
              const image = new Image();
              const previewImage = $(this).find(".loadingImage");
              const newImage = $(this).find(".overlay");
              image.src = previewImage.data("image");
              image.onload = function() {
                newImage.css("background-image", "url(" + image.src + ")");
                newImage.css("opacity", "1");
              };
              image.onerror = function() {
                newImage.css(
                  "background-image",
                  "url(../assets/img/thumb.jpg)"
                );
                newImage.css("opacity", "1");
              };
            });
          }, 1000);
        } else {
          this.showImageContent = true;
        }
        console.log("collection: ", this.collection);
      },
      error => {
        this.showImageContent = true;
      }
    );
  }

  refresh() {
    if (this.action == "connect") {
    } else {
      this.initData();
    }
  }

  onSegmentChanged() {
    setTimeout(() => {
      $(".progressive-image").each(function() {
        const image = new Image();
        const previewImage = $(this).find(".loadingImage");
        const newImage = $(this).find(".overlay");
        image.src = previewImage.data("image");
        image.onload = function() {
          newImage.css("background-image", "url(" + image.src + ")");
          newImage.css("opacity", "1");
        };
        image.onerror = function() {
          newImage.css("background-image", "url(../assets/img/thumb.jpg)");
          newImage.css("opacity", "1");
        };
      });
    }, 1000);
  }

  viewImageDetail(item) {
    console.log("viewImageDetail: ", item);
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.router.navigate(["/media/images/", item.id]);
  }

  viewVideoDetail(item) {
    console.log("viewVideoDetail: ", item);
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.router.navigate(["/media/videos/", item.id]);
  }

  viewPrintDetail(item) {
    console.log("viewPrintDetail: ", item);
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.router.navigate(["/media/prints/", item.id]);
  }

  viewProductDetail(item) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.router.navigate(["/products", item.productId]);
  }

  deleteCollection() {
    this.coolDialogs
      .confirm("Are you sure to delete collection?")
      .subscribe(res => {
        if (res) {
          const formData = new FormData();
          formData.append("id", this.collection.id);

          this.collections
            .collection_delete(formData, this.token)
            .then((data: any) => {
              if (data && data.success) {
                console.log("result: ", data);
                this.toastr.success(data.data, "SUCCESS");
                setTimeout(() => {
                  this._location.back();
                }, 600);
              }
            })
            .catch(error => {
              console.log("error: ", error);
              if (error) {
                this.toastr.error(error.message, "ERROR");
              }
            });
        }
      });
  }

  editCollection() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason == "save") {
          this.refresh();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "update",
      collection: this.collection
    };

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      CollectionEntryComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }

  addMediaFile(action) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.refresh();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: action,
      request: "collection",
      params: this.collection
    };

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(PickuplistComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }
}
