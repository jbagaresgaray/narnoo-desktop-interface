import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { combineLatest, Subscription } from "rxjs";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import * as _ from "lodash";
import * as async from "async";

import { CollectionsService } from "../../../services/collections.service";
import { CollectionEntryComponent } from "./collection-entry/collection-entry.component";

@Component({
  selector: "app-collections",
  templateUrl: "./collections.component.html",
  styleUrls: ["./collections.component.css"]
})
export class CollectionsComponent implements OnInit {
  token: string;
  business: any = {};
  action: string;

  imagesLoadingArr: any[] = [];
  collectionArr: any[] = [];

  showContentCollections: boolean = false;
  showContentCollectionsErr: boolean = false;
  collectionsContentErr: any = {};
  pageCollection: number = 0;
  totalPageCollection: number = 0;

  perPage: number = 20;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public collections: CollectionsService,
    private modalService: BsModalService,
    public toastr: ToastrService,
    private changeDetection: ChangeDetectorRef,
    private coolDialogs: NgxCoolDialogsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
    for (let i = 0; i < 20; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  ngOnInit() {
    this.action = this.route.snapshot.paramMap.get("action");

    this.pageCollection = 1;
    this.showContentCollections = false;
    this.showContentCollectionsErr = false;
    this.loadCollections(this.pageCollection, this.perPage);
  }

  loadCollections(page, total) {
    this.collections
      .collecton_list(this.token, {
        page: page,
        total: total
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            this.pageCollection = parseInt(data.data.page, 0);
            this.totalPageCollection = parseInt(data.data.totalPages, 0);
            for (let i = 0; i < _.size(data.data.data); i++) {
              data.data.data[i].selected = false;
              this.collectionArr.push(data.data.data[i]);
            }
          } else if (data && data.success && data.data[0] === false) {
            this.showContentCollectionsErr = true;
            this.collectionsContentErr = {
              message: "No results found"
            };
          }
          this.showContentCollections = true;
        },
        error => {
          this.showContentCollections = true;
          this.showContentCollectionsErr = true;
          if (error && !error.success) {
            this.collectionsContentErr = error;
          }
        }
      );
  }

  refresh() {
    this.pageCollection = 1;
    this.collectionArr = [];
    this.showContentCollections = false;
    this.showContentCollectionsErr = false;

    this.loadCollections(this.pageCollection, this.perPage);
  }

  createCollection(action, item?: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.refresh();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: action
    };

    if (action === "update") {
      initialState.collection = item;
    }

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      CollectionEntryComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }

  deleteCollection(collection: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.coolDialogs
      .confirm("Are you sure to delete collection?")
      .subscribe(res => {
        if (res) {
          const formData = new FormData();
          formData.append("id", collection.id);

          this.collections
            .collection_delete(formData, this.token)
            .then((data: any) => {
              if (data && data.success) {
                console.log("result: ", data);
                this.toastr.success(data.data, "SUCCESS");
                this.refresh();
              }
            })
            .catch(error => {
              console.log("error: ", error);
              if (error) {
                this.toastr.error(error.message, "ERROR");
              }
            });
        }
      });
  }

  viewCollections(item: any) {
    this.router.navigate(["/media/collections", item.id]);
  }
}
