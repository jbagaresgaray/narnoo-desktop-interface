import { Component, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as async from "async";

import { ImagesService } from "../../../../services/images.service";

@Component({
  selector: "app-image-entry",
  templateUrl: "./image-entry.component.html",
  styleUrls: ["./image-entry.component.css"]
})
export class ImageEntryComponent implements OnInit {
  image: any = {};
  item: any = {};
  business: any = {};
  token: string;
  tags: any[] = ["Ionic", "Angular", "TypeScript"];
  imageId: string;

  disabledMarket: boolean = false;
  showLoading: boolean = false;
  isSaving: boolean = false;

  constructor(
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    public toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public images: ImagesService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }

  ngOnInit() {
    console.log("item: ", this.imageId);
    this.image.markets = {};
    if (this.business.role === 3 && this.business.role === 4) {
      this.disabledMarket = true;
    }

    this.initData();
  }

  initData() {
    this.showLoading = true;
    this.images
      .image_detail(this.token, this.imageId)
      .then(
        (data: any) => {
          if (data && data.success) {
            this.image = data.data;
            this.image.imageId = data.data.id;

            if (this.image.markets) {
              this.image.markets.australia =
                this.image.markets.australia === 1 ? true : false;
              this.image.markets.france =
                this.image.markets.france === 1 ? true : false;
              this.image.markets.germany =
                this.image.markets.germany === 1 ? true : false;
              this.image.markets.greaterChina =
                this.image.markets.greaterChina === 1 ? true : false;
              this.image.markets.india =
                this.image.markets.india === 1 ? true : false;
              this.image.markets.japan =
                this.image.markets.japan === 1 ? true : false;
              this.image.markets.korea =
                this.image.markets.korea === 1 ? true : false;
              this.image.markets.newZealand =
                this.image.markets.newZealand === 1 ? true : false;
              this.image.markets.northAmerica =
                this.image.markets.northAmerica === 1 ? true : false;
              this.image.markets.singapore =
                this.image.markets.singapore === 1 ? true : false;
              this.image.markets.unitedKindom =
                this.image.markets.unitedKindom === 1 ? true : false;
            } else {
              this.image.markets = {};
            }
          }
          console.log("this.image: ", this.image);
          this.showLoading = false;
        },
        error => {
          console.log("error: ", error);
          this.showLoading = false;
        }
      )
      .catch(error => {
        console.log("catch: ", error);
        this.showLoading = false;
      });
  }

  saveEntry() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const updateImage = () => {
      this.toastr.info("Saving...", "INFO");
      this.isSaving = true;
      async.waterfall(
        [
          callback => {
            if (this.disabledMarket) {
              this.toastr.warning("Invalid Access to Image Market!", "WARNING");
              callback();
            } else {
              this.image.markets.imageId = this.image.imageId;
              this.images
                .image_market_edit(this.token, this.image.markets)
                .then(
                  (data: any) => {
                    if (data && data.success) {
                      this.toastr.success("Success! " + data.data, "SUCCESS");
                      callback();
                    } else {
                      this.toastr.error(data.message, "ERROR");
                    }
                  },
                  error => {
                    callback();
                  }
                );
            }
          },
          callback => {
            this.images.image_edit(this.token, this.image).then(
              (data: any) => {
                if (data && data.success) {
                  this.toastr.success(data.data, "SUCCESS");
                  callback();
                } else {
                  this.toastr.error(data.message, "ERROR");
                  this.isSaving = false;
                  return;
                }
              },
              error => {
                callback();
              }
            );
          }
        ],
        () => {
          this.isSaving = false;
          this.modalService.setDismissReason("save");
          setTimeout(() => {
            this.bsModalRef.hide();
          }, 300);
        }
      );
    };

    this.coolDialogs
      .confirm("Do you want to save the entry?")
      .subscribe(res => {
        if (res) {
          updateImage();
        }
      });
  }
}
