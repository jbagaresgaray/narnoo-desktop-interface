import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  NgZone,
  ViewEncapsulation
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ContextMenuComponent, ContextMenuService } from "ngx-contextmenu";
import { combineLatest, Subscription } from "rxjs";
import { BroadcasterService } from "ng-broadcaster";
import xml2js from "xml2js";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { LoadingBarService } from "@ngx-loading-bar/core";
import {
  DropzoneComponent,
  DropzoneDirective,
  DropzoneConfigInterface
} from "ngx-dropzone-wrapper";
import { ElectronService } from "ngx-electron";

import * as _ from "lodash";
import * as $ from "jquery";

import { ImagesService } from "../../../services/images.service";
import { UtilitiesService } from "../../../services/utilities.service";
import { UploadService } from "../../../services/upload.service";

import { ImageEntryComponent } from "./image-entry/image-entry.component";
import { UploadComponent } from "../upload/upload.component";
import { AlbumGalleryComponent } from "../albums/album-gallery/album-gallery.component";
import { CollectionChannelPickerComponent } from "../../collection-channel-picker/collection-channel-picker.component";
import { ProductPickerComponent } from "../../product-picker/product-picker.component";

import { environment } from "../../../../environments/environment";

@Component({
  selector: "app-images",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./images.component.html",
  styleUrls: ["./images.component.css"]
})
export class ImagesComponent implements OnInit {
  token: string;
  business: any = {};
  action: string;

  imagesArr: any[] = [];
  imagesLoadingArr: any[] = [];
  selectedMedia: any[] = [];
  contextMenu: any[] = [];
  selectedItem: any = {};

  accessKey: string;
  action_uri: string;
  file_name: string;
  policy: string;
  signature: string;
  file_key: string;
  contentType: string;
  successActionStatus: string;
  acl: string;

  showPreview = false;
  showUpload = false;
  showDragDropUpload = false;

  showContentImage = false;
  showContentImageErr = false;
  showLoading = false;

  imageContentErr: any = {};
  pageImage: number = 0;
  totalPageImage: number = 0;
  fileAdded: number = 0;

  perPage = 100;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  @ViewChild(ContextMenuComponent)
  public basicMenu: ContextMenuComponent;

  @ViewChild(DropzoneDirective)
  componentRef?: DropzoneDirective;

  public config: DropzoneConfigInterface = {};

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private changeDetection: ChangeDetectorRef,
    private zone: NgZone,
    public images: ImagesService,
    private modalService: BsModalService,
    public toastr: ToastrService,
    private contextMenuService: ContextMenuService,
    private utilities: UtilitiesService,
    private broadcaster: BroadcasterService,
    private uploadService: UploadService,
    private coolDialogs: NgxCoolDialogsService,
    private loadingBar: LoadingBarService,
    private _electronService: ElectronService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
    for (let i = 0; i < 30; ++i) {
      this.imagesLoadingArr.push(i);
    }

    this.utilities._contextMenu = [];
  }

  ngOnInit() {
    this.pageImage = 1;
    this.imagesArr = [];
    this.showContentImage = false;
    this.showContentImageErr = false;
    this.showLoading = true;
    this.showUpload = false;
    this.showDragDropUpload = false;

    this.action = this.route.snapshot.paramMap.get("action");

    if (
      this.business.type === 1 &&
      (this.business.role === 2 || this.business.role === 1)
    ) {
      this.showUpload = true;
    } else {
      this.showUpload = false;
    }

    this.config.dictDefaultMessage =
      "Click Here or Drag & Drop files to instantly upload them.";

    this.loadImage(this.pageImage, this.perPage);
    this.generateImagePolicy();
  }

  private loadImage(page, total) {
    console.log("loadImage");
    this.images
      .image_list(this.token, {
        page: page,
        total: total
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            this.pageImage = parseInt(data.data.currentPage, 0);
            this.totalPageImage = parseInt(data.data.totalPages, 0);
            for (let i = 0; i < _.size(data.data.images); i++) {
              data.data.images[i].selected = false;

              if (_.isEmpty(data.data.images[i].caption)) {
                data.data.images[i].caption = "No Caption";
              }
              if (_.isEmpty(data.data.images[i].location)) {
                data.data.images[i].location = "No Location";
              }

              this.imagesArr.push(data.data.images[i]);
            }
          } else if (data && data.success && data.data[0] == false) {
            this.showContentImageErr = true;
            this.imageContentErr = {
              message: "No results found"
            };
          }
          this.showContentImage = true;
          this.showLoading = false;

          setTimeout(() => {
            $(".progressive-image").each(function() {
              const image = new Image();
              const previewImage = $(this).find(".loadingImage");
              const newImage = $(this).find(".overlay");
              image.src = previewImage.data("image");
              image.onload = function() {
                newImage.css("background-image", "url(" + image.src + ")");
                newImage.css("opacity", "1");
              };
              image.onerror = function() {
                newImage.css(
                  "background-image",
                  "url(../assets/img/thumb.jpg)"
                );
                newImage.css("opacity", "1");
              };
            });
          }, 1000);
        },
        (error: any) => {
          this.showContentImage = true;
          this.showContentImageErr = true;
          this.showLoading = false;

          if (error && !error.success) {
            this.imageContentErr = error;
          }
        }
      );
  }

  private generateImagePolicy() {
    this.uploadService
      .upload_image_policy(this.token)
      .then(
        (data: any) => {
          if (data && data.success) {
            console.log("upload_image_policy: ", data.data);
            this.accessKey = data.data.accessKey;
            this.action_uri = data.data.action_uri;
            this.file_name = data.data.name;
            this.file_key = data.data.key;
            this.policy = data.data.policy;
            this.signature = data.data.signature;
            this.contentType = data.data.contentType;
            this.successActionStatus = data.data.successActionStatus;
            this.acl = data.data.acl;
          }

          const vm = this;
          this.config.url = this.action_uri;
          this.config.paramName = this.file_name;
          this.config.autoProcessQueue = true;

          this.config.init = function() {
            const dz = this;
            dz.on("sending", function(file, xhr, formData) {
              formData.append("acl", vm.acl);
              formData.append("Policy", vm.policy);
              formData.append("X-Amz-Signature", vm.signature);
              formData.append("AWSAccessKeyId", vm.accessKey);
              formData.append("Signature", vm.signature);
              formData.append("success_action_status", vm.successActionStatus);
              formData.append("Content-Type", file.type);
              formData.append(
                "key",
                vm.file_key.split("/")[0] + "/" + file.name
              );
            });

            dz.on("addedfile", function(file) {
              if (file.size < environment.imageFileSize) {
                console.log("WARNING");
                vm.zone.run(() => {
                  vm.toastr.warning(
                    "Your proposed upload file is smaller than the minimum allowed size of 800kb",
                    "WARNING"
                  );
                  setTimeout(() => {
                    vm.componentRef.dropzone().removeFile(file);
                  }, 600);
                });
                return;
              }
              vm.fileAdded += 1;
              console.log("addedfile: ", vm.fileAdded);
              vm.changeDetection.detectChanges();
            });

            dz.on("success", function(file) {
              setTimeout(() => {
                vm.fileAdded -= 1;
                console.log("success: ", vm.fileAdded);
                vm.componentRef.dropzone().removeFile(file);

                if (vm.fileAdded === 0) {
                  setTimeout(() => {
                    vm.zone.run(() => {
                      vm.showDragDropUpload = false;
                      vm.refresh();
                    });
                  }, 600);
                }
              }, 1000);
            });

            dz.on("uploadprogress", function(file, progress, bytesSent) {
              // Display the progress
              console.log("progress: ", progress);
            });
          };
        },
        error => {
          console.error("error: ", error);
          this.showLoading = false;
        }
      )
      .catch(error => {
        console.error("error: ", error);
        this.showLoading = false;
      });
  }

  refresh() {
    this.pageImage = 1;
    this.imagesArr = [];
    this.showContentImage = false;
    this.showContentImageErr = false;

    this.loadImage(this.pageImage, this.perPage);
  }

  viewDetail(item: any) {
    this.router.navigate(["/media/images/", item.id]);
  }

  viewMore() {
    if (this.action === "connect") {
    } else {
      this.pageImage = this.pageImage + 1;
      if (this.pageImage <= this.totalPageImage) {
        this.showLoading = true;
        this.loadImage(this.pageImage, this.perPage);
      }
    }
  }

  cancelUploadMedia() {
    this.showDragDropUpload = false;
    this.componentRef.dropzone().removeAllFiles(true);

    setTimeout(() => {
      $(".progressive-image").each(function() {
        const image = new Image();
        const previewImage = $(this).find(".loadingImage");
        const newImage = $(this).find(".overlay");
        image.src = previewImage.data("image");
        image.onload = function() {
          newImage.css("background-image", "url(" + image.src + ")");
          newImage.css("opacity", "1");
        };
        image.onerror = function() {
          newImage.css("background-image", "url(../assets/img/thumb.jpg)");
          newImage.css("opacity", "1");
        };
      });
    }, 1000);
  }

  uploadMedia() {
    console.log("uploadMedia");
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    if (this.showDragDropUpload) {
      this.toastr.warning(
        "Unable to proceed, file upload is on-going!",
        "WARNING"
      );
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.refresh();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "media",
      title: "Upload Image"
    };

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(UploadComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  selectMedia(item: any, event) {
    if (event.ctrlKey || event.metaKey) {
      if (_.find(this.selectedMedia, { id: item.id })) {
        item.selected = false;
        _.remove(this.selectedMedia, (row: any) => {
          return row.id === item.id;
        });
        this.utilities.selectedData = this.selectedMedia;
      } else {
        item.selected = true;
        this.selectedMedia.push(item);
        this.utilities.selectedData = this.selectedMedia;
      }
    } else {
      item.selected = !item.selected;
      _.each(this.imagesArr, (row: any) => {
        if (row.id !== item.id) {
          row.selected = false;
        }
      });
      if (item.selected) {
        this.selectedItem = item;
        this.utilities.selectedItem = item;
      }
    }
  }

  allowDrop(ev) {
    ev.preventDefault();

    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      this.showDragDropUpload = false;
      return;
    }

    if (!this.showContentImage) {
      return;
    }

    this.showDragDropUpload = true;
  }

  drop(ev) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      this.showDragDropUpload = false;
      return;
    }

    ev.preventDefault();
    const data = ev.dataTransfer.getData("text");
    console.log("drop data: ", data);
  }

  onUploadError(args: any): void {
    console.log("onUploadError:", args);
    if (args && _.isArray(args) && args[0].status === "error") {
      this.parseXML(args[1]).then((data: any) => {
        console.log("parseXML: ", data);
        if (data && data.Error) {
          this.toastr.error(data.Error.Message[0], "ERROR");
        } else {
          this.toastr.error(args[1], "ERROR");
        }
      });
      this.componentRef.dropzone().removeFile(args[0]);
      return;
    } else if (args && _.isArray(args) && args[0].status === "canceled") {
      this.toastr.warning(args[1], "WARNING");
      return;
    }
  }

  onUploadSuccess(args: any): void {
    console.log("onUploadSuccess:", args);
    if (args && _.isArray(args) && args[0].status === "success") {
      this.toastr.success(args[1] || "File upload successfully!", "SUCCESS");
    }
  }

  private parseXML(data) {
    return new Promise(resolve => {
      const parser = new xml2js.Parser({
        trim: true
      });

      parser.parseString(data, function(err, result) {
        console.log("result: ", result);
        resolve(result);
      });
    });
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }

  private addToAlbumImage(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const initialState: any = {
      image: item
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      AlbumGalleryComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private addToProductImage(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const initialState: any = {
      image: item,
      action: "add_image"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      ProductPickerComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private addToProductFeature(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const initialState: any = {
      image: item,
      action: "feature",
      type: "image"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      ProductPickerComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private setImageAsBusinessFeature(item: any) {
    this.coolDialogs.confirm("Set image as feature?").subscribe(res => {
      if (res) {
        this.toastr.info("Featuring...", "INFO");
        this.images.image_feature(this.token, { id: item.id }).then(
          (data: any) => {
            if (data && data.success) {
              this.toastr.success(data.data, "SUCCESS");
            } else if (data && !data.success) {
              this.toastr.warning(data.message, "WARNING");
            }
          },
          (error: any) => {
            console.log("error: ", error);
          }
        );
      }
    });
  }

  private addImageToCollection(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const initialState: any = {
      params: item,
      type: "image"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      CollectionChannelPickerComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private deleteImage(item: any) {
    console.log("deleteImage: ", item);
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.coolDialogs
      .confirm("Are you sure to delete this image?")
      .subscribe(res => {
        if (res) {
          this.loadingBar.progress$.subscribe(ev => {
            console.log("image_delete PROGRESS: ", ev);
          });
          this.toastr.info("Deleting...", "INFO");
          this.images.image_delete(this.token, item).then(
            (data: any) => {
              if (data && data.success) {
                console.log("image_delete: ", data);
                this.toastr.success("Image successfully deleted!", "SUCCESS");

                this.loadingBar.progress$.unsubscribe();
                setTimeout(() => {
                  this.refresh();
                }, 600);
              }
            },
            (error: any) => {
              console.log("error: ", error);
            }
          );
        }
      });
  }

  private updateImageDetail(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const initialState: any = {
      item: item
    };
    console.log("initialState: ", initialState);

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(ImageEntryComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private downloadImage(item: any) {
    const ctrl = this;
    if (!this.business.paid) {
      this.toastr.warning(
        "This feature is only available to premium users only.",
        "WARNING"
      );
      return;
    }

    this.toastr.info("Downloading ...");
    function download(dataurl, filename) {
      const a = document.createElement("a");
      a.href = dataurl;
      a.setAttribute("download", filename);
      const b = document.createEvent("MouseEvents");
      b.initEvent("click", false, true);
      a.dispatchEvent(b);
      return false;
    }

    function getFilename(url) {
      url = url
        .split("/")
        .pop()
        .replace(/\#(.*?)$/, "")
        .replace(/\?(.*?)$/, "");
      url = url.split("."); // separates filename and extension
      return { filename: url[0] || "", ext: url[1] || "" };
    }

    this.images.image_download(this.token, item.id).then(
      (data: any) => {
        if (data && data.success) {
          const filename =
            getFilename(data.data).filename + "." + getFilename(data.data).ext;
          if (!window["electron"]) {
            download(data.data, filename);
            this.toastr.success("Image successfully downloaded", "SUCCESS");
          } else {
            // DOWNLOAD NATIVE VIA ELECTRON
            download(data.data, filename);
          }
        }
      },
      error => {
        if (error && !error.success) {
          this.toastr.error(
            "Error while downloading file. " + error.message,
            "ERROR"
          );
          return;
        }
      }
    );
  }
}
