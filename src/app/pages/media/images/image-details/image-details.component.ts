import { Component, OnInit, NgZone, ChangeDetectorRef } from "@angular/core";
import { Location } from "@angular/common";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import $ from "jquery";
import _ from "lodash";
import { combineLatest, Subscription } from "rxjs";
import { ElectronService } from "ngx-electron";

import { ImagesService } from "../../../../services/images.service";
import { AlbumsService } from "../../../../services/albums.service";
import { ProductsService } from "../../../../services/products.service";

import { SharerComponent } from "../../sharer/sharer.component";
import { ImageEntryComponent } from "../image-entry/image-entry.component";
import { ProductPickerComponent } from "../../../product-picker/product-picker.component";
import { CollectionChannelPickerComponent } from "../../../collection-channel-picker/collection-channel-picker.component";
import { AlbumGalleryComponent } from "../../albums/album-gallery/album-gallery.component";

@Component({
  selector: "app-image-details",
  templateUrl: "./image-details.component.html",
  styleUrls: ["./image-details.component.css"]
})
export class ImageDetailsComponent implements OnInit {
  token: string;
  action: string;
  imageId: string;

  item: any = {};
  params: any = {};
  business: any = {};

  connect_params: any = {};
  notif_business: any = {};

  showDownload: boolean;

  showError: boolean = false;
  showLoading: boolean = true;

  isPreview: boolean = false;

  isGeneral: boolean = false;
  isMarket: boolean = false;
  isEmbededInfo: boolean = false;
  isMore: boolean = false;

  disabledMarket: boolean = true;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    public zone: NgZone,
    public location: Location,
    private router: Router,
    private changeDetection: ChangeDetectorRef,
    public toastr: ToastrService,
    private modalService: BsModalService,
    public coolDialogs: NgxCoolDialogsService,
    private _electronService: ElectronService,
    private route: ActivatedRoute,
    public images: ImagesService,
    public albums: AlbumsService,
    public products: ProductsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }

  ngOnInit() {
    console.log("ImageDetailsComponent");

    this.isGeneral = true;
    this.item.markets = {};
    this.imageId = this.route.snapshot.paramMap.get("id");
    this.action = this.route.snapshot.queryParamMap.get("action");
    this.connect_params =
      JSON.parse(this.route.snapshot.queryParamMap.get("params")) || {};
    this.notif_business = this.route.snapshot.queryParamMap.get("business");

    console.log("action: ", this.action);
    console.log("imageId: ", this.imageId);
    console.log("connect_params: ", this.connect_params);
    console.log("notif_business: ", this.notif_business);

    if (this.imageId) {
      this.showLoading = true;
      this.initData();
    }

    if (!_.isEmpty(this.notif_business)) {
      if (this.business.id !== this.notif_business.businessId) {
        this.showError = true;
        this.showLoading = false;
        return;
      }
    }
  }

  private initData() {
    console.log("initData");
    const successResponse = (data: any) => {
      if (data && data.success) {
        console.log("data: ", data);
        this.item = data.data;
        this.item.imageId = data.data.id;
        this.item.imageSize = parseFloat(this.item.imageSize);
        if (_.isEmpty(this.item.caption)) {
          this.item.caption = "No caption available";
        }

        if (this.item.markets) {
          this.item.markets.australia =
            this.item.markets.australia === "1" ? true : false;
          this.item.markets.france =
            this.item.markets.france === "1" ? true : false;
          this.item.markets.germany =
            this.item.markets.germany === "1" ? true : false;
          this.item.markets.greaterChina =
            this.item.markets.greaterChina === "1" ? true : false;
          this.item.markets.india =
            this.item.markets.india === "1" ? true : false;
          this.item.markets.japan =
            this.item.markets.japan === "1" ? true : false;
          this.item.markets.korea =
            this.item.markets.korea === "1" ? true : false;
          this.item.markets.newZealand =
            this.item.markets.newZealand === "1" ? true : false;
          this.item.markets.northAmerica =
            this.item.markets.northAmerica === "1" ? true : false;
          this.item.markets.singapore =
            this.item.markets.singapore === "1" ? true : false;
          this.item.markets.unitedKindom =
            this.item.markets.unitedKindom === "1" ? true : false;
        } else {
          this.item.markets = {};
        }
      }
      this.showError = false;
      this.showLoading = false;

      setTimeout(() => {
        $(".progressive-image").each(function() {
          const image = new Image();
          const previewImage = $(this).find(".loadingImage");
          const newImage = $(this).find(".overlay");
          image.src = previewImage.data("image");
          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(./assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
          };
        });
      }, 1000);
    };

    const errorResponse = error => {
      console.log("errorResponse: ", error);
      this.showError = true;
      this.showLoading = false;
    };

    if (this.action === "connect") {
      if (this.connect_params.type === "operator") {
        this.images
          .image_operator_image(
            this.token,
            this.connect_params.id,
            this.imageId
          )
          .then(successResponse, errorResponse)
          .catch(errorResponse);
      } else if (this.connect_params.type === "distributor") {
        this.images
          .image_distributor_image(
            this.token,
            this.connect_params.id,
            this.imageId
          )
          .then(successResponse, errorResponse)
          .catch(errorResponse);
      }
    } else {
      this.images
        .image_detail(this.token, this.imageId)
        .then(successResponse, errorResponse);
    }
  }

  updateImage() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.refresh();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      imageId: this.item.id,
      action: "albums"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(ImageEntryComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  addToAlbumImage() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const initialState: any = {
      imageId: this.item.id,
      action: "albums"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      AlbumGalleryComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  addToProductImage() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const initialState: any = {
      imageId: this.item.id,
      action: "add_image"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      ProductPickerComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  addToProductFeature() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const initialState: any = {
      imageId: this.item.id,
      action: "feature"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      ProductPickerComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  addImageToCollection() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const initialState: any = {
      imageId: this.item.id,
      action: "image"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      CollectionChannelPickerComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  refresh() {
    this.initData();
  }

  downloadImage() {
    if (!this.business.paid) {
      this.toastr.warning(
        "This feature is only available to premium users only.",
        "WARNING"
      );
      return;
    }

    this.toastr.info("Downloading ...");
    const download = (dataurl, filename) => {
      const a = document.createElement("a");
      a.href = dataurl;
      a.setAttribute("download", filename);
      const b = document.createEvent("MouseEvents");
      b.initEvent("click", false, true);
      a.dispatchEvent(b);
      return false;
    };

    const getFilename = url => {
      url = url
        .split("/")
        .pop()
        .replace(/\#(.*?)$/, "")
        .replace(/\?(.*?)$/, "");
      url = url.split("."); // separates filename and extension
      return { filename: url[0] || "", ext: url[1] || "" };
    };

    this.images.image_download(this.token, this.item.id).then(
      (data: any) => {
        console.log("data: ", data);
        if (data && data.success) {
          const filename =
            getFilename(data.data).filename + "." + getFilename(data.data).ext;
          console.log("filename: ", filename);
          if (this._electronService.isElectronApp) {
            download(data.data, filename);
            this.toastr.success("Image successfully downloaded", "SUCCESS");
          } else {
            download(data.data, filename);
          }
        }
      },
      error => {
        if (error && !error.success) {
          this.toastr.error(
            "Error while downloading file. " + error.message,
            "ERROR"
          );
          return;
        }
      }
    );
  }

  shareImage() {
    if (!this.business.paid) {
      this.toastr.warning(
        "This feature is only available to premium users only.",
        "WARNING"
      );
      return;
    }

    const initialState: any = {
      imageId: this.item.id,
      action: "image"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(SharerComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  deleteImage() {
    this.coolDialogs
      .confirm("Are you sure to delete this image?")
      .subscribe(res => {
        if (res) {
          this.toastr.info("Deleting...", "INFO");
          this.images.image_delete(this.token, [this.item.id]).then(
            (data: any) => {
              if (data && data.success) {
                console.log("image_delete: ", data);

                this.toastr.success("Image successfully deleted!", "SUCCESS");
                setTimeout(() => {
                  this.router.navigate(["/media"]);
                }, 600);
              } else if (data && !data.success) {
                this.toastr.warning(data.message, "WARNING");
              }
            },
            (error: any) => {
              console.log("error: ", error);
            }
          );
        }
      });
  }

  featureImage() {
    this.coolDialogs
      .confirm("Are you sure to feature this image?")
      .subscribe(res => {
        if (res) {
          this.toastr.info("Featuring...", "INFO");
          this.images.image_feature(this.token, this.item).then(
            (data: any) => {
              if (data && data.success) {
                console.log("image_delete: ", data);
                this.toastr.success(data.data, "SUCCESS");
                setTimeout(() => {
                  this.initData();
                }, 600);
              }
            },
            (error: any) => {
              console.log("error: ", error);
            }
          );
        }
      });
  }

  showTab(action) {
    if (this.showLoading) {
      return;
    }

    if (action === "general") {
      this.zone.run(() => {
        this.isGeneral = true;
        this.isMarket = false;
        this.isEmbededInfo = false;
      });
    } else if (action === "market") {
      this.zone.run(() => {
        this.isGeneral = false;
        this.isMarket = true;
        this.isEmbededInfo = false;
      });
    } else if (action === "embed") {
      this.zone.run(() => {
        this.isGeneral = false;
        this.isMarket = false;
        this.isEmbededInfo = true;
      });
    }

    setTimeout(() => {
      $(".progressive-image").each(function() {
        const image = new Image();
        const previewImage = $(this).find(".loadingImage");
        const newImage = $(this).find(".overlay");
        image.src = previewImage.data("image");
        image.onload = function() {
          newImage.css("background-image", "url(" + image.src + ")");
          newImage.css("opacity", "1");
        };
        image.onerror = function() {
          newImage.css("background-image", "url(../assets/img/thumb.jpg)");
          newImage.css("opacity", "1");
        };
      });
    }, 300);
  }

  copied(event) {
    if (event.isSuccess) {
      this.toastr.info("Copied!");
    }
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
