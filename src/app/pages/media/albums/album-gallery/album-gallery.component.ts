import { Component, OnInit } from "@angular/core";
import * as _ from "lodash";
import * as async from "async";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { ToastrService } from "ngx-toastr";

import { AlbumsService } from "../../../../services/albums.service";

@Component({
  selector: "app-album-gallery",
  templateUrl: "./album-gallery.component.html",
  styleUrls: ["./album-gallery.component.css"]
})
export class AlbumGalleryComponent implements OnInit {
  image: any = {};
  selected: any = {};
  albumsContentErr: any = {};
  token: string;
  albumsArr: any[] = [];
  fakeArr: any[] = [];
  business: any = {};

  showContent: boolean = false;
  showContentErr: boolean = false;
  isSaving: boolean = false;

  constructor(
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    public toastr: ToastrService,
    private albums: AlbumsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
    for (let i = 0; i < 20; ++i) {
      this.fakeArr.push(i);
    }
  }

  ngOnInit() {
    this.initData();
  }

  private initData() {
    this.albums.album_list(this.token).then(
      (data: any) => {
        if (data && data.success) {
          this.albumsArr = data.data.albums;
          _.each(this.albumsArr, (item: any) => {
            if (item.media) {
              item.imageCount = _.size(item.media.images);
              item.images = item.media.images;
            } else {
              item.imageCount = 0;
              item.images = [];
            }
            item.checked = false;
          });
          this.showContent = true;
          console.log("this.albumsArr: ", this.albumsArr);
        }
      },
      error => {
        this.showContent = true;
      }
    );
  }

  refresh() {
    this.initData();
  }

  checkChanged(album) {
    this.selected = album;
    _.each(this.albumsArr, (row: any) => {
      if (album.id === row.id) {
        row.checked = true;
      } else {
        row.checked = false;
      }
    });
  }

  saveChanges() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.toastr.info("Adding image...", "INFO");
    this.isSaving = true;
    this.albums
      .album_add_image(this.token, {
        albumId: this.selected.id,
        image: [
          {
            id: this.image.id
          }
        ]
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            this.isSaving = false;
            this.toastr.success("Image added to album", "SUCCESS");
            this.modalService.setDismissReason("save");
            setTimeout(() => {
              this.bsModalRef.hide();
            }, 600);
          }
        },
        error => {
          this.isSaving = false;
          if (error) {
            this.toastr.error(error.error, "ERROR");
          }
        }
      );
  }
}
