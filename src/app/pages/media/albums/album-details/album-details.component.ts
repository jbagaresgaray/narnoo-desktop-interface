import { Component, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import { Location } from "@angular/common";

import * as _ from "lodash";
import * as $ from "jquery";

import { AlbumsService } from "../../../../services/albums.service";
import { UtilitiesService } from "../../../../services/utilities.service";

@Component({
  selector: "app-album-details",
  templateUrl: "./album-details.component.html",
  styleUrls: ["./album-details.component.css"]
})
export class AlbumDetailsComponent implements OnInit {
  token: string;
  imagesArr: any[] = [];
  imagesLoadingArr: any[] = [];
  selectedMedia: any[] = [];
  selectedItem: any = {};
  albumId: any;
  title: string;
  navParams: any;

  business: any = {};
  action: string;
  connect_params: any = {};

  showPreview: boolean = false;
  showContent: boolean = false;
  showCheckbox: boolean = false;

  constructor(
    public toastr: ToastrService,
    public location: Location,
    private coolDialogs: NgxCoolDialogsService,
    private route: ActivatedRoute,
    private router: Router,
    public albums: AlbumsService,
    private utilities: UtilitiesService
  ) {
    for (let i = 0; i < 20; ++i) {
      this.imagesLoadingArr.push(i);
    }

    this.business = JSON.parse(localStorage.getItem("business")) || {};
    this.token = localStorage.getItem("bus.token");
    this.navParams = this.route.snapshot.paramMap;
  }

  ngOnInit() {
    this.albumId = this.navParams.get("id");
    this.title = this.route.snapshot.queryParamMap.get("album");
    this.action = this.route.snapshot.queryParamMap.get("action");
    this.connect_params =
      JSON.parse(this.route.snapshot.queryParamMap.get("params")) || {};

    this.initData();
  }

  back() {
    this.location.back();
  }

  initData() {
    const successResponse = (data: any) => {
      if (data && data.success) {
        this.imagesArr = data.data.images;
        _.each(this.imagesArr, (row: any) => {
          row.selected = false;
        });
        console.log("this.imagesArr: ", this.imagesArr);
      }
      this.showContent = true;

      setTimeout(() => {
        $(".progressive-image").each(function() {
          const image = new Image();
          const previewImage = $(this).find(".loadingImage");
          const newImage = $(this).find(".overlay");
          image.src = previewImage.data("image");
          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(../assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
          };
        });
      }, 1000);
    };

    const errorResponse = error => {
      this.showContent = true;
    };

    this.showContent = false;
    if (this.action === "connect") {
      if (this.connect_params.type === "operator") {
        this.albums
          .album_operator_images(
            this.token,
            this.connect_params.id,
            this.albumId
          )
          .then(successResponse, errorResponse);
      } else if (this.connect_params.type === "distributor") {
        this.albums
          .album_distributor_images(
            this.token,
            this.connect_params.id,
            this.albumId
          )
          .then(successResponse, errorResponse);
      }
    } else {
      this.albums
        .album_images(this.token, this.albumId)
        .then(successResponse, errorResponse);
    }
  }

  refresh() {
    this.initData();
  }

  selectMedia(item: any, event) {
    if (event.ctrlKey || event.metaKey) {
      if (_.find(this.selectedMedia, { id: item.id })) {
        item.selected = false;
        _.remove(this.selectedMedia, (row: any) => {
          return row.id === item.id;
        });
        this.utilities.selectedData = this.selectedMedia;
      } else {
        item.selected = true;
        this.selectedMedia.push(item);
        this.utilities.selectedData = this.selectedMedia;
      }
    } else {
      item.selected = !item.selected;
      _.each(this.imagesArr, (row: any) => {
        if (row.id !== item.id) {
          row.selected = false;
        }
      });
      if (item.selected) {
        this.selectedItem = item;
        this.utilities.selectedItem = item;
      }
    }
  }

  viewImageDetail(item: any) {
    if (this.action === "connect") {
      const navigationExtras: NavigationExtras = {
        queryParams: {
          action: "connect",
          image: JSON.stringify(item),
          params: JSON.stringify(this.connect_params)
        },
        preserveFragment: true
      };
      this.router.navigate(["/media/images/", item.id], navigationExtras);
    } else {
      this.router.navigate(["/media/images/", item.id]);
    }
  }

  uploadFile() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }
  }

  deleteSelected() {
    let selectedDatas: any[] = [];

    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    selectedDatas = _.filter(this.imagesArr, (row: any) => {
      return row.selected;
    });

    this.coolDialogs
      .confirm("Are you sure to delete selected images?")
      .subscribe(res => {
        if (res) {
          selectedDatas = _.map(selectedDatas, (row: any) => {
            return {
              id: row.id
            };
          });

          this.albums
            .album_remove_image(this.token, {
              albumId: this.albumId,
              image: selectedDatas
            })
            .then(
              (data: any) => {
                if (data && data.success) {
                  setTimeout(() => {
                    this.initData();
                    this.showCheckbox = false;
                  }, 1000);
                }
              },
              error => {
                console.log("error: ", error);
                if (error && !error.success) {
                  this.toastr.error(error.message, "ERROR");
                  return;
                }
              }
            );
        }
      });
  }
}
