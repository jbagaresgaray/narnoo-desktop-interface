import { Component, OnInit } from "@angular/core";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { ToastrService } from "ngx-toastr";

import { AlbumsService } from "../../../../services/albums.service";

@Component({
  selector: "app-album-entry",
  templateUrl: "./album-entry.component.html",
  styleUrls: ["./album-entry.component.css"]
})
export class AlbumEntryComponent implements OnInit {
  album: any = {};
  business: any = {};

  token: string;
  showLoading: boolean = false;

  constructor(
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    public albums: AlbumsService,
    public toastr: ToastrService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }

  ngOnInit() {}

  saveEntry() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.showLoading = true;
    this.albums
      .album_create(this.token, this.album)
      .then((data: any) => {
        if (data && data.success) {
          this.toastr.success(
            data.message || "Album successfully created!",
            "SUCCESS"
          );
          this.modalService.setDismissReason("save");
          setTimeout(() => {
            this.bsModalRef.hide();
          }, 600);
        } else {
          this.toastr.warning(data.message, "WARNING");
        }
        this.showLoading = false;
      })
      .catch((error: any) => {
        console.log("error: ", error);
        this.showLoading = false;
        if (error && !error.success) {
          this.toastr.error(error.message, "ERROR");
          return;
        }
      });
  }
}
