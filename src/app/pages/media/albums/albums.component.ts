import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { combineLatest, Subscription } from "rxjs";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import * as _ from "lodash";
import * as async from "async";

import { AlbumsService } from "../../../services/albums.service";

import { AlbumEntryComponent } from "./album-entry/album-entry.component";

@Component({
  selector: "app-albums",
  templateUrl: "./albums.component.html",
  styleUrls: ["./albums.component.css"]
})
export class AlbumsComponent implements OnInit {
  token: string;
  business: any = {};
  action: string;

  albumsArr: any[] = [];
  imagesLoadingArr: any[] = [];

  showLoading: boolean = false;
  showContentAlbums: boolean = false;
  showContentAlbumsErr: boolean = false;
  albumsContentErr: any = {};
  pageAlbum: number = 0;
  totalPageAlbum: number = 0;

  perPage = 20;
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public albums: AlbumsService,
    private modalService: BsModalService,
    public toastr: ToastrService,
    private changeDetection: ChangeDetectorRef,
    private coolDialogs: NgxCoolDialogsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
    for (let i = 0; i < 20; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  ngOnInit() {
    this.action = this.route.snapshot.paramMap.get("action");
    this.pageAlbum = 1;
    this.albumsArr = [];

    this.showContentAlbums = false;
    this.showContentAlbumsErr = false;

    this.loadAlbums(this.pageAlbum, this.perPage);
  }

  loadAlbums(page, total) {
    this.showLoading = true;
    this.albums
      .album_list(this.token, {
        page: page,
        total: total
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            this.pageAlbum = parseInt(data.data.currentPage, 0);
            this.totalPageAlbum = parseInt(data.data.totalPages, 0);
            for (let i = 0; i < _.size(data.data.albums); i++) {
              data.data.albums[i].selected = false;
              if (_.isEmpty(data.data.albums[i].title)) {
                data.data.albums[i].title = "No caption";
              }
              this.albumsArr.push(data.data.albums[i]);
            }
          } else if (data && data.success && data.data[0] == false) {
            this.showContentAlbumsErr = true;
            this.albumsContentErr = {
              message: "No results found"
            };
          }
          this.showContentAlbums = true;
          this.showLoading = false;
        },
        error => {
          this.showContentAlbums = true;
          this.showContentAlbumsErr = true;
          this.showLoading = false;
          if (error && !error.success) {
            this.albumsContentErr = error;
          }
        }
      );
  }

  refresh() {
    this.pageAlbum = 1;
    this.albumsArr = [];
    this.showContentAlbums = false;
    this.showContentAlbumsErr = false;

    this.loadAlbums(this.pageAlbum, this.perPage);
  }

  viewMore() {
    if (this.action === "connect") {
    } else {
      this.pageAlbum = this.pageAlbum + 1;
      if (this.pageAlbum <= this.totalPageAlbum) {
        this.showContentAlbums = true;
        this.loadAlbums(this.pageAlbum, this.perPage);
      }
    }
  }

  createAlbums() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason == "save") {
          this.refresh();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "create"
    };

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(AlbumEntryComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }

  deleteAlbums(album: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.coolDialogs
      .confirm("Are you sure to delete selected albums?")
      .subscribe(res => {
        if (res) {
          const formData = new FormData();
          formData.append("id", album.id);
          this.albums.album_delete(this.token, formData).then(
            (data: any) => {
              if (data && data.success) {
                console.log("data: ", data);
                this.toastr.success(
                  data.data || "Album successfully deleted!",
                  "SUCCESS"
                );
                this.refresh();
              }
            },
            (error: any) => {
              console.log("error: ", error);
              if (error && error.error) {
                this.toastr.error(error.message, "ERROR");
              }
            }
          );
        }
      });
  }

  viewAlbumImages(item: any) {
    const navigationExtras: NavigationExtras = {
      queryParams: { album: item.title },
      preserveFragment: true
    };
    this.router.navigate(["/media/albums", item.id], navigationExtras);
  }
}
