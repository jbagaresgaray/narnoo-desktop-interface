import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ClipboardModule } from "ngx-clipboard";
import {
  TabsModule,
  BsDropdownModule,
  ButtonsModule,
  CollapseModule,
  ModalModule,
  CarouselModule,
  PopoverModule
} from "ngx-bootstrap";
import { ContextMenuModule } from "ngx-contextmenu";
import { DropzoneModule } from "ngx-dropzone-wrapper";

import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

import { LogosComponent } from "./logos/logos.component";
import { LogoDetailsComponent } from "./logos/logo-details/logo-details.component";

import { PrintsComponent } from "./prints/prints.component";
import { PrintDetailsComponent } from "./prints/print-details/print-details.component";
import { PrintEntryComponent } from "./prints/print-entry/print-entry.component";

import { VideosComponent } from "./videos/videos.component";
import { VideoDetailsComponent } from "./videos/video-details/video-details.component";
import { VideoEntryComponent } from "./videos/video-entry/video-entry.component";

import { ImagesComponent } from "./images/images.component";
import { ImageDetailsComponent } from "./images/image-details/image-details.component";

import { SharerComponent } from "./sharer/sharer.component";

import { ChannelsComponent } from "./channels/channels.component";
import { CollectionsComponent } from "./collections/collections.component";

import { UploadComponent } from "./upload/upload.component";
import { PickuplistComponent } from "./pickuplist/pickuplist.component";
import { ImageEntryComponent } from "./images/image-entry/image-entry.component";

import { AlbumsComponent } from "./albums/albums.component";
import { AlbumDetailsComponent } from "./albums/album-details/album-details.component";
import { AlbumEntryComponent } from "./albums/album-entry/album-entry.component";
import { AlbumGalleryComponent } from "./albums/album-gallery/album-gallery.component";

import { ChannelDetailsComponent } from "./channels/channel-details/channel-details.component";
import { ChannelEntryComponent } from "./channels/channel-entry/channel-entry.component";

import { CollectionDetailsComponent } from "./collections/collection-details/collection-details.component";
import { CollectionEntryComponent } from "./collections/collection-entry/collection-entry.component";
import { CollectionGalleryComponent } from "./collections/collection-gallery/collection-gallery.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    PipesModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonsModule,
    BsDropdownModule,
    CollapseModule,
    ComponentsModule,
    ContextMenuModule,
    ModalModule,
    DropzoneModule,
    ClipboardModule,
    PopoverModule,
    TabsModule,
    CarouselModule
  ],
  declarations: [
    ImagesComponent,
    LogosComponent,
    PrintsComponent,
    VideosComponent,
    ImageDetailsComponent,
    PrintDetailsComponent,
    LogoDetailsComponent,
    VideoDetailsComponent,
    UploadComponent,
    PickuplistComponent,
    ImageEntryComponent,
    SharerComponent,
    AlbumsComponent,
    AlbumDetailsComponent,
    AlbumEntryComponent,
    AlbumGalleryComponent,
    ChannelsComponent,
    ChannelDetailsComponent,
    ChannelEntryComponent,
    CollectionsComponent,
    CollectionDetailsComponent,
    CollectionEntryComponent,
    CollectionGalleryComponent,
    PrintEntryComponent,
    VideoEntryComponent
  ],
  entryComponents: [
    ImagesComponent,
    LogosComponent,
    PrintsComponent,
    VideosComponent,
    ImageDetailsComponent,
    PrintDetailsComponent,
    LogoDetailsComponent,
    VideoDetailsComponent,
    UploadComponent,
    PickuplistComponent,
    ImageEntryComponent,
    PrintEntryComponent,
    SharerComponent,
    AlbumDetailsComponent,
    AlbumEntryComponent,
    AlbumGalleryComponent,
    ChannelEntryComponent,
    CollectionEntryComponent,
    CollectionGalleryComponent,
    VideoEntryComponent
  ],
  exports: [ImagesComponent, LogosComponent, PrintsComponent, VideosComponent]
})
export class MediaModule {}
