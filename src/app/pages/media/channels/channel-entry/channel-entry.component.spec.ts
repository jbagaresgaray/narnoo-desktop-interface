import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelEntryComponent } from './channel-entry.component';

describe('ChannelEntryComponent', () => {
  let component: ChannelEntryComponent;
  let fixture: ComponentFixture<ChannelEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChannelEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
