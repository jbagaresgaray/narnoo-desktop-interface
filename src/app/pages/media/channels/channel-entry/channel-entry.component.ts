import { Component, OnInit } from "@angular/core";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import { ChannelsService } from "../../../../services/channels.service";

@Component({
  selector: "app-channel-entry",
  templateUrl: "./channel-entry.component.html",
  styleUrls: ["./channel-entry.component.css"]
})
export class ChannelEntryComponent implements OnInit {
  channel: any = {};
  business: any = {};
  token: string;
  action: string;

  showLoading: boolean = false;

  constructor(
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    public channels: ChannelsService,
    public toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }

  ngOnInit() {}

  saveEntry() {
    this.coolDialogs.confirm("Save channel entry?").subscribe(res => {
      if (res) {
        if (this.action === "update") {
          this.updateEntry();
        } else if (this.action === "create") {
          this.createEntry();
        }
      }
    });
  }

  private updateEntry() {}

  private createEntry() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.showLoading = true;

    this.channels
      .channel_create(this.channel, this.token)
      .then((data: any) => {
        if (data && data.success) {
          this.modalService.setDismissReason("save");
          this.toastr.success(
            data.message || "Channel successfully created",
            "SUCCESS"
          );
          setTimeout(() => {
            this.bsModalRef.hide();
          }, 600);
        } else {
          this.toastr.warning(data.message, "WARNING");
        }
        this.showLoading = false;
      })
      .catch((error: any) => {
        console.log("error: ", error);
        this.showLoading = false;
        if (error && !error.success) {
          this.toastr.error(error.message, "ERROR");
          return;
        }
      });
  }
}
