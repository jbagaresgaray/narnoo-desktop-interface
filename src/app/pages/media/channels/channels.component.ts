import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { combineLatest, Subscription } from "rxjs";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import * as _ from "lodash";
import * as $ from "jquery";

import { ChannelsService } from "../../../services/channels.service";
import { ChannelEntryComponent } from "./channel-entry/channel-entry.component";

@Component({
  selector: "app-channels",
  templateUrl: "./channels.component.html",
  styleUrls: ["./channels.component.css"]
})
export class ChannelsComponent implements OnInit {
  token: string;
  business: any = {};
  action: string;

  imagesLoadingArr: any[] = [];
  channelArr: any[] = [];

  showContentChannels = false;
  showContentChannelsErr = false;
  channelsContentErr: any = {};
  pageChannel = 0;
  totalPageChannel = 0;

  perPage = 20;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public channels: ChannelsService,
    private modalService: BsModalService,
    public toastr: ToastrService,
    private changeDetection: ChangeDetectorRef,
    private coolDialogs: NgxCoolDialogsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
    for (let i = 0; i < 10; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  ngOnInit() {
    this.action = this.route.snapshot.paramMap.get("action");

    this.channelArr = [];

    this.showContentChannels = false;
    this.showContentChannelsErr = false;

    this.loadChannels(this.pageChannel, this.perPage);
  }

  loadChannels(page, total) {
    this.channels
      .channel_list(this.token, {
        page: this.pageChannel,
        total: this.perPage
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            this.pageChannel = parseInt(data.data.page, 0);
            this.totalPageChannel = parseInt(data.data.totalPages, 0);
            for (let i = 0; i < _.size(data.data.data); i++) {
              data.data.data[i].selected = false;
              this.channelArr.push(data.data.data[i]);
            }
          } else if (data && data.success && data.data[0] === false) {
            this.showContentChannelsErr = true;
            this.channelsContentErr = {
              message: "No results found"
            };
          }
          this.showContentChannels = true;

          setTimeout(() => {
            $(".progressive-image").each(function() {
              const image = new Image();
              const previewImage = $(this).find(".loadingImage");
              const newImage = $(this).find(".overlay");
              image.src = previewImage.data("image");
              image.onload = function() {
                newImage.css("background-image", "url(" + image.src + ")");
                newImage.css("opacity", "1");
              };
              image.onerror = function() {
                newImage.css(
                  "background-image",
                  "url(../assets/img/thumb.jpg)"
                );
                newImage.css("opacity", "1");
              };
            });
          }, 1000);
        },
        error => {
          this.showContentChannels = true;
          this.showContentChannelsErr = true;
          if (error && !error.success) {
            this.channelsContentErr = error;
          }
        }
      );
  }

  refresh() {
    this.channelArr = [];

    this.showContentChannels = false;
    this.showContentChannelsErr = false;
    this.pageChannel = 1;
    this.loadChannels(this.pageChannel, this.perPage);
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }

  createChannel(action, item?: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason == "save") {
          this.refresh();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: action
    };

    if (action === "update") {
      initialState.collection = item;
    }

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      ChannelEntryComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  deleteChannels(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.coolDialogs
      .confirm("Are you sure to delete channel?")
      .subscribe(res => {
        if (res) {
          this.channels
            .channel_delete(this.token, { id: item.id })
            .then((data: any) => {
              if (data && data.success) {
                console.log("result: ", data);
                this.toastr.success(data.data, "SUCCESS");
                this.refresh();
              }
            })
            .catch(error => {
              console.log("error: ", error);
              if (error) {
                this.toastr.error(error.message, "ERROR");
              }
            });
        }
      });
  }

  viewChannel(item: any) {
    this.router.navigate(["/media/channels", item.id]);
  }
}
