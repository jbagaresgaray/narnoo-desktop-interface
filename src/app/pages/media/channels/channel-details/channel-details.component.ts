import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Location } from "@angular/common";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { combineLatest, Subscription } from "rxjs";

import * as _ from "lodash";
import * as async from "async";
import * as $ from "jquery";

import { ChannelsService } from "../../../../services/channels.service";

import { ChannelEntryComponent } from "../channel-entry/channel-entry.component";
import { PickuplistComponent } from "../../pickuplist/pickuplist.component";

@Component({
  selector: "app-channel-details",
  templateUrl: "./channel-details.component.html",
  styleUrls: ["./channel-details.component.css"]
})
export class ChannelDetailsComponent implements OnInit {
  channelId: any;
  token: string;
  item: any = {};
  channel: any = {};
  business: any = {};
  imagesLoadingArr: any[] = [];

  action: string;
  connect_params: any = {};
  navParams: any;
  showLoading: boolean = true;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    public toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private _location: Location,
    private route: ActivatedRoute,
    private modalService: BsModalService,
    private changeDetection: ChangeDetectorRef,
    private router: Router,
    public channels: ChannelsService
  ) {
    this.navParams = this.route.snapshot.paramMap;
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    this.channel.video = [];

    for (let i = 0; i < 20; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  private initData() {
    this.showLoading = true;
    this.channels.channel_details(this.channelId, this.token).then(
      (data: any) => {
        if (data && data.success) {
          this.channel = data.data;
          /* if (this.platform.is('ios')) {
					_.each(this.channel.videos, (row: any) => {
						row.uploadedAt = new Date(row.uploadedAt.replace(/-/g, "/"));
					});
				} */
          console.log("this.channel: ", this.channel);
        }
        this.showLoading = false;

        setTimeout(() => {
          $(".progressive-image").each(function() {
            const image = new Image();
            const previewImage = $(this).find(".loadingImage");
            const newImage = $(this).find(".overlay");
            image.src = previewImage.data("image");
            image.onload = function() {
              newImage.css("background-image", "url(" + image.src + ")");
              newImage.css("opacity", "1");
            };
            image.onerror = function() {
              newImage.css("background-image", "url(../assets/img/thumb.jpg)");
              newImage.css("opacity", "1");
            };
          });
        }, 1000);
      },
      error => {
        this.showLoading = false;
      }
    );
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }

  ngOnInit() {
    this.action = this.navParams.get("action");
    this.connect_params = this.navParams.get("params");
    this.channelId = this.navParams.get("id");

    this.initData();
  }

  refresh() {
    this.initData();
  }

  deleteChannels() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.coolDialogs
      .confirm("Are you sure to delete this channel?")
      .subscribe(res => {
        if (res) {
          this.channels
            .channel_delete(this.token, {
              id: this.channelId
            })
            .then((data: any) => {
              if (data && data.success) {
                console.log("result: ", data);
                this.toastr.success(data.data, "SUCCESS");
                setTimeout(() => {
                  this._location.back();
                }, 600);
              }
            })
            .catch(error => {
              console.log("error: ", error);
              if (error) {
                this.toastr.error(error.message, "ERROR");
              }
            });
        }
      });
  }

  updateChannnel() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        if (reason === "save") {
          this.refresh();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "update",
      channel: this.channel
    };

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      ChannelEntryComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  addMediaFile(action) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.refresh();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: action,
      request: "channel",
      params: this.channel
    };

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(PickuplistComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  viewVideoDetail(item) {
    this.router.navigate(["/media/videos/", item.id]);
  }
}
