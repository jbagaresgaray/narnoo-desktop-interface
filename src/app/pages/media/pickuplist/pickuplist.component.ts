import { Component, OnInit } from "@angular/core";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import * as $ from "jquery";
import * as _ from "lodash";
import * as async from "async";

import { ImagesService } from "../../../services/images.service";
import { LogosService } from "../../../services/logos.service";
import { PrintService } from "../../../services/print.service";
import { VideosService } from "../../../services/videos.service";
import { AlbumsService } from "../../../services/albums.service";

import { CollectionsService } from "../../../services/collections.service";
import { ChannelsService } from "../../../services/channels.service";

@Component({
  selector: "app-pickuplist",
  templateUrl: "./pickuplist.component.html",
  styleUrls: ["./pickuplist.component.css"]
})
export class PickuplistComponent implements OnInit {
  action: string;
  request: string;
  token: string;
  params: any = {};
  business: any = {};
  perPage = 16;
  showLoading: boolean = false;
  isUploading: boolean = false;

  showContentImage: boolean = false;
  showContentImageErr: boolean = false;
  imageContentErr: any = {};
  pageImage: number = 1;
  totalPageImage: number = 0;

  showContentLogo: boolean = false;
  showContentLogoErr: boolean = false;
  logoContentErr: any = {};
  pageLogo: number = 1;
  totalPageLogo: number = 0;

  showContentPrint: boolean = false;
  showContentPrintErr: boolean = false;
  printContentErr: any = {};
  pagePrint: number = 1;
  totalPagePrint: number = 0;

  showContentVideos: boolean = false;
  showContentVideosErr: boolean = false;
  videosContentErr: any = {};
  pageVideo: number = 1;
  totalPageVideo: number = 0;

  showContentAlbums: boolean = false;
  showContentAlbumsErr: boolean = false;
  albumsContentErr: any = {};
  pageAlbum: number = 1;
  totalPageAlbum: number = 0;

  imagesLoadingArr: any[] = [];
  selectedMedia: any[] = [];

  imagesArr: any[] = [];
  logosArr: any[] = [];
  printArr: any[] = [];
  videosArr: any[] = [];
  albumsArr: any[] = [];

  constructor(
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    public toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private images: ImagesService,
    private logos: LogosService,
    private prints: PrintService,
    private videos: VideosService,
    private collections: CollectionsService,
    private channels: ChannelsService,
    private albums: AlbumsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
    for (let i = 0; i < 16; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  ngOnInit() {
    this.showLoading = true;
    console.log("this.action: ", this.action);
    console.log("this.params: ", this.params);
    console.log("this.request: ", this.request);

    if (this.action === "image") {
      this.showContentImage = false;
      this.showContentImageErr = false;

      this.loadImage(this.pageImage, this.perPage);
    } else if (this.action === "logo") {
      this.showContentLogo = false;
      this.showContentLogoErr = false;

      this.loadLogos(this.pageLogo, this.perPage);
    } else if (this.action === "print") {
      this.showContentPrint = false;
      this.showContentPrintErr = false;

      this.loadPrints(this.pagePrint, this.perPage);
    } else if (this.action === "video") {
      this.showContentVideos = false;
      this.showContentVideosErr = false;

      this.loadVideos(this.pageVideo, this.perPage);
    } else if (this.action === "albums") {
      this.showContentAlbums = false;
      this.showContentAlbumsErr = false;

      this.loadAlbums(this.pageAlbum, this.perPage);
    }
  }

  loadImage(page, total) {
    this.images
      .image_list(this.token, {
        page: page,
        total: total
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            this.pageImage = parseInt(data.data.currentPage, 0);
            this.totalPageImage = parseInt(data.data.totalPages, 0);
            for (let i = 0; i < _.size(data.data.images); i++) {
              data.data.images[i].selected = false;

              if (_.isEmpty(data.data.images[i].caption)) {
                data.data.images[i].caption = "No Caption";
              }
              if (_.isEmpty(data.data.images[i].location)) {
                data.data.images[i].location = "No Location";
              }

              this.imagesArr.push(data.data.images[i]);
            }
          } else if (data && data.success && data.data[0] == false) {
            this.showContentImageErr = true;
            this.imageContentErr = {
              message: "No results found"
            };
          }
          this.showContentImage = true;
          this.showLoading = false;

          setTimeout(() => {
            $(".progressive-image").each(function() {
              const image = new Image();
              const previewImage = $(this).find(".loadingImage");
              const newImage = $(this).find(".overlay");
              image.src = previewImage.data("image");
              image.onload = function() {
                newImage.css("background-image", "url(" + image.src + ")");
                newImage.css("opacity", "1");
              };
              image.onerror = function() {
                newImage.css(
                  "background-image",
                  "url(../assets/img/thumb.jpg)"
                );
                newImage.css("opacity", "1");
              };
            });
          }, 1000);
        },
        (error: any) => {
          this.showContentImage = true;
          this.showContentImageErr = true;
          this.showLoading = false;

          if (error && !error.success) {
            this.imageContentErr = error;
          }
        }
      );
  }

  loadLogos(page, total) {
    this.logos
      .logos_list(this.token, {
        page: page,
        total: total
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            this.pageLogo = parseInt(data.data.currentPage, 0);
            this.totalPageLogo = parseInt(data.data.totalPages, 0);
            for (let i = 0; i < _.size(data.data.logos); i++) {
              data.data.logos[i].selected = false;
              this.logosArr.push(data.data.logos[i]);
            }
          } else if (data && data.success && data.data[0] === false) {
            this.showContentLogoErr = true;
            this.logoContentErr = {
              message: "No results found"
            };
          }
          this.showContentLogo = true;
          this.showLoading = false;

          setTimeout(() => {
            $(".progressive-image").each(function() {
              const image = new Image();
              const previewImage = $(this).find(".loadingImage");
              const newImage = $(this).find(".overlay");
              image.src = previewImage.data("image");
              image.onload = function() {
                newImage.css("background-image", "url(" + image.src + ")");
                newImage.css("opacity", "1");
              };
              image.onerror = function() {
                newImage.css(
                  "background-image",
                  "url(../assets/img/thumb.jpg)"
                );
                newImage.css("opacity", "1");
              };
            });
          }, 1000);
        },
        error => {
          this.showContentLogo = true;
          this.showContentLogoErr = true;
          this.showLoading = false;

          if (error && !error.success) {
            this.logoContentErr = error;
          }
        }
      );
  }

  loadPrints(page, total) {
    this.prints
      .brochure_list(this.token, {
        page: page,
        total: total
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            this.pagePrint = parseInt(data.data.currentPage, 0);
            this.totalPagePrint = parseInt(data.data.totalPages, 0);
            for (let i = 0; i < _.size(data.data.prints); i++) {
              data.data.prints[i].selected = false;
              if (_.isEmpty(data.data.prints[i].brochure_caption)) {
                data.data.prints[i].brochure_caption = "No caption";
              }
              this.printArr.push(data.data.prints[i]);
            }
          } else if (data && data.success && data.data[0] == false) {
            this.showContentPrintErr = true;
            this.printContentErr = {
              message: "No results found"
            };
          }

          this.showContentPrint = true;
          this.showLoading = false;

          setTimeout(() => {
            $(".progressive-image").each(function() {
              const image = new Image();
              const previewImage = $(this).find(".loadingImage");
              const newImage = $(this).find(".overlay");
              image.src = previewImage.data("image");
              image.onload = function() {
                newImage.css("background-image", "url(" + image.src + ")");
                newImage.css("opacity", "1");
              };
              image.onerror = function() {
                newImage.css(
                  "background-image",
                  "url(../assets/img/thumb.jpg)"
                );
                newImage.css("opacity", "1");
              };
            });
          }, 1000);
        },
        (error: any) => {
          this.showContentPrint = true;
          this.showContentPrintErr = true;

          if (error && !error.success) {
            this.printContentErr = error;
          }
        }
      );
  }

  loadVideos(page, total) {
    this.videos
      .video_list(this.token, {
        page: page,
        total: total
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            this.pageVideo = parseInt(data.data.currentPage, 0);
            this.totalPageVideo = parseInt(data.data.totalPages, 0);
            for (let i = 0; i < _.size(data.data.videos); i++) {
              data.data.videos[i].selected = false;
              if (_.isEmpty(data.data.videos[i].caption)) {
                data.data.videos[i].caption = "No caption";
              }
              this.videosArr.push(data.data.videos[i]);
            }
          } else if (data && data.success && data.data[0] === false) {
            this.showContentVideosErr = true;
            this.videosContentErr = {
              message: "No results found"
            };
          }
          this.showContentVideos = true;
          this.showLoading = false;

          setTimeout(() => {
            $(".progressive-image").each(function() {
              const image = new Image();
              const previewImage = $(this).find(".loadingImage");
              const newImage = $(this).find(".overlay");
              image.src = previewImage.data("image");
              image.onload = function() {
                newImage.css("background-image", "url(" + image.src + ")");
                newImage.css("opacity", "1");
              };
              image.onerror = function() {
                newImage.css(
                  "background-image",
                  "url(../assets/img/thumb.jpg)"
                );
                newImage.css("opacity", "1");
              };
            });
          }, 1000);
        },
        error => {
          this.showContentVideos = true;
          this.showContentVideosErr = true;
          this.showLoading = false;
          if (error && !error.success) {
            this.videosContentErr = error;
          }
        }
      );
  }

  loadAlbums(page, total, refresher?: any, infinite?: any) {
    this.albums
      .album_list(this.token, {
        page: page,
        total: total
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            if (refresher) {
              this.albumsArr = [];
            }
            this.pageAlbum = parseInt(data.data.currentPage, 0);
            this.totalPageAlbum = parseInt(data.data.totalPages, 0);
            for (let i = 0; i < _.size(data.data.albums); i++) {
              data.data.albums[i].selected = false;
              if (_.isEmpty(data.data.albums[i].title)) {
                data.data.albums[i].title = "No caption";
              }
              this.albumsArr.push(data.data.albums[i]);
            }
          } else if (data && data.success && data.data[0] === false) {
            this.showContentAlbumsErr = true;
            this.albumsContentErr = {
              message: "No results found"
            };
          }
          this.showContentAlbums = true;
          if (refresher) {
            refresher.complete();
          }
          if (infinite) {
            infinite.complete();
          }
        },
        error => {
          this.showContentAlbums = true;
          this.showContentAlbumsErr = true;
          if (refresher) {
            refresher.complete();
          }
          if (infinite) {
            infinite.complete();
          }
          if (error && !error.success) {
            this.albumsContentErr = error;
          }
        }
      );
  }

  viewMore() {
    if (this.action === "image") {
      this.pageImage = this.pageImage + 1;
      if (this.pageImage <= this.totalPageImage) {
        this.showLoading = true;
        this.loadImage(this.pageImage, this.perPage);
      }
    } else if (this.action === "logo") {
      this.pageLogo = this.pageLogo + 1;
      if (this.pageLogo <= this.totalPageLogo) {
        this.showLoading = true;
        this.loadLogos(this.pageLogo, this.perPage);
      }
    } else if (this.action === "print") {
      this.pagePrint = this.pagePrint + 1;
      if (this.pagePrint <= this.totalPagePrint) {
        this.showLoading = true;
        this.loadPrints(this.pagePrint, this.perPage);
      }
    } else if (this.action === "video") {
      this.pageVideo = this.pageVideo + 1;
      if (this.pageVideo <= this.totalPageVideo) {
        this.showLoading = true;
        this.loadVideos(this.pageVideo, this.perPage);
      }
    } else if (this.action === "albums") {
      this.pageAlbum = this.pageAlbum + 1;
      if (this.pageAlbum <= this.totalPageAlbum) {
        this.showLoading = true;
        this.loadLogos(this.pageAlbum, this.perPage);
      }
    }
  }

  selectMedia(item: any) {
    if (_.find(this.selectedMedia, { id: item.id })) {
      item.selected = false;
      _.remove(this.selectedMedia, (row: any) => {
        return row.id === item.id;
      });
    } else {
      item.selected = true;
      this.selectedMedia.push(item);
    }
  }

  private saveCollections() {
    this.isUploading = true;
    async.eachSeries(
      this.selectedMedia,
      (item: any, callback) => {
        this.collections
          .collection_add_media(
            {
              id: this.params.id,
              media: this.action,
              mediaId: item.id
            },
            this.token
          )
          .then(
            (data: any) => {
              if (data && data.success) {
                console.log("data: ", data);
                this.toastr.success(data.data, "SUCCESS");
              }
              callback();
            },
            error => {
              console.log("error: ", error);
              if (error && !error.success) {
                this.toastr.error(error.message, "ERROR");
              }
              callback();
            }
          );
      },
      () => {
        this.isUploading = false;
        this.modalService.setDismissReason("save");
        setTimeout(() => {
          this.bsModalRef.hide();
        }, 1000);
      }
    );
  }

  private saveChannel() {
    this.isUploading = true;
    async.eachSeries(
      this.selectedMedia,
      (item: any, callback) => {
        this.channels
          .channel_add_video(this.token, {
            id: this.params.id,
            videoId: item.id
          })
          .then(
            (data: any) => {
              if (data && data.success) {
                console.log("data: ", data);
                this.toastr.success(data.data, "SUCCESS");
              }
              callback();
            },
            error => {
              console.log("error: ", error);
              if (error && !error.success) {
                this.toastr.error(error.message, "ERROR");
              }
              callback();
            }
          );
      },
      () => {
        this.isUploading = false;
        this.modalService.setDismissReason("save");
        setTimeout(() => {
          this.bsModalRef.hide();
        }, 1000);
      }
    );
  }

  saveChanges() {
    console.log("selectedMedia: ", this.selectedMedia);

    if (_.isEmpty(this.selectedMedia)) {
      this.toastr.warning("Please select image to feature!", "WARNING");
      return;
    }

    if (this.request === "channel") {
      this.saveChannel();
    } else if (this.request === "collection") {
      this.saveCollections();
    }
  }
}
