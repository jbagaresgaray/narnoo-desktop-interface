import {
  Component,
  OnInit,
  NgZone,
  ViewEncapsulation,
  ChangeDetectorRef
} from "@angular/core";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { Location } from "@angular/common";
import { ToastrService } from "ngx-toastr";
import { combineLatest, Subscription } from "rxjs";
import { ElectronService } from "ngx-electron";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import * as $ from "jquery";
import * as _ from "lodash";

import { PrintService } from "../../../../services/print.service";
import { AlbumsService } from "../../../../services/albums.service";
import { ProductsService } from "../../../../services/products.service";

import { UploadComponent } from "../../upload/upload.component";
import { SharerComponent } from "../../sharer/sharer.component";
import { ProductPickerComponent } from "../../../product-picker/product-picker.component";
import { CollectionChannelPickerComponent } from "../../../collection-channel-picker/collection-channel-picker.component";
import { PrintEntryComponent } from "../print-entry/print-entry.component";

@Component({
  selector: "app-print-details",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./print-details.component.html",
  styleUrls: ["./print-details.component.css"]
})
export class PrintDetailsComponent implements OnInit {
  token: string;
  action: string;
  printId: string;

  item: any = {};
  params: any = {};
  business: any = {};
  connect_params: any = {};
  notif_business: any = {};

  showDownload: boolean;

  showError: boolean = false;
  showLoading: boolean = true;

  isGeneral: boolean = false;
  isMarket: boolean = false;

  callback: Promise<any>;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    public location: Location,
    private route: ActivatedRoute,
    public changeDetection: ChangeDetectorRef,
    public zone: NgZone,
    public toastr: ToastrService,
    private modalService: BsModalService,
    public coolDialogs: NgxCoolDialogsService,
    private _electronService: ElectronService,
    public prints: PrintService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }

  ngOnInit() {
    this.isGeneral = true;

    this.printId = this.route.snapshot.paramMap.get("id");
    this.action = this.route.snapshot.queryParamMap.get("action");
    this.connect_params =
      JSON.parse(this.route.snapshot.queryParamMap.get("params")) || {};
    this.notif_business = this.route.snapshot.queryParamMap.get("business");

    console.log("action: ", this.action);
    console.log("printId: ", this.printId);
    console.log("connect_params: ", this.connect_params);
    console.log("notif_business: ", this.notif_business);

    if (this.printId) {
      this.showLoading = true;
      this.initData();
    }
  }

  private initData() {
    const successResponse = (data: any) => {
      if (data && data.success) {
        this.item = data.data;
        this.item.size = parseFloat(this.item.size);
        console.log("this.item: ", this.item);
      }
      this.showLoading = false;
      this.showError = false;

      setTimeout(() => {
        $(".progressive-image").each(function() {
          const image = new Image();
          const previewImage = $(this).find(".loadingImage");
          const newImage = $(this).find(".overlay");

          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(./assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
            console.log("complete");
          };
          image.src = previewImage.data("image");
        });
      }, 1000);
    };

    const errorResponse = error => {
      this.showLoading = false;
      this.showError = true;
    };

    if (this.action === "connect") {
      if (this.connect_params.type === "operator") {
        this.prints
          .brochure_operator_detail(
            this.token,
            this.printId,
            this.connect_params.id
          )
          .then(successResponse, errorResponse)
          .catch(errorResponse);
      } else if (this.connect_params.type === "distributor") {
        this.prints
          .brochure_distributor_detail(
            this.token,
            this.printId,
            this.connect_params.id
          )
          .then(successResponse, errorResponse)
          .catch(errorResponse);
      }
    } else if (this.action === "notif") {
      this.prints
        .brochure_detail(this.token, this.printId)
        .then(successResponse, errorResponse)
        .catch(errorResponse);
    } else {
      this.prints
        .brochure_detail(this.token, this.printId)
        .then(successResponse, errorResponse)
        .catch(errorResponse);
    }
  }

  refresh() {
    this.initData();
  }

  addToProductFeature() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const initialState: any = {
      image: this.item,
      action: "feature",
      type: "print"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      ProductPickerComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  showTab(action) {
    if (this.showLoading) {
      return;
    }

    if (action === "general") {
      this.isGeneral = true;
      this.isMarket = false;
    } else if (action === "market") {
      this.isGeneral = false;
      this.isMarket = true;
    }

    setTimeout(() => {
      $(".progressive-image").each(function() {
        const image = new Image();
        const previewImage = $(this).find(".loadingImage");
        const newImage = $(this).find(".overlay");
        image.src = previewImage.data("image");
        image.onload = function() {
          newImage.css("background-image", "url(" + image.src + ")");
          newImage.css("opacity", "1");
        };
        image.onerror = function() {
          newImage.css("background-image", "url(../assets/img/thumb.jpg)");
          newImage.css("opacity", "1");
        };
      });
    }, 300);
  }

  downloadImage() {
    this.toastr.info("Downloading ...");
    const download = (dataurl, filename) => {
      const a = document.createElement("a");
      a.href = dataurl;
      a.setAttribute("download", filename);
      const b = document.createEvent("MouseEvents");
      b.initEvent("click", false, true);
      a.dispatchEvent(b);
      return false;
    };

    const getFilename = url => {
      url = url
        .split("/")
        .pop()
        .replace(/\#(.*?)$/, "")
        .replace(/\?(.*?)$/, "");
      url = url.split("."); // separates filename and extension
      return { filename: url[0] || "", ext: url[1] || "" };
    };

    this.prints.brochure_download(this.token, this.item.id).then(
      (data: any) => {
        if (data && data.success) {
          let file: any = "";
          if (data.data && _.isArray(data.data) && data.data.length > 0) {
            file = data.data[0];
          } else {
            file = data.data;
          }
          console.log("file: ", file);
          const filename =
            getFilename(file).filename + "." + getFilename(file).ext;

          if (this._electronService.isElectronApp) {
            download(file, filename);
            this.toastr.success("Brochure successfully downloaded", "SUCCESS");
          } else {
            // DOWNLOAD NATIVE VIA ELECTRON
            download(file, filename);
          }
        }
      },
      error => {
        if (error && !error.success) {
          this.toastr.error(
            "Error while downloading file. " + error.message,
            "ERROR"
          );
          return;
        }
      }
    );
  }

  shareBrochure() {
    if (!this.business.paid) {
      this.toastr.warning(
        "This feature is only available to premium users only.",
        "WARNING"
      );
      return;
    }

    const initialState: any = {
      printId: this.item.id,
      action: "print"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(SharerComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  deleteBrochure() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.coolDialogs
      .confirm("Are you sure to delete this brochure?")
      .subscribe(res => {
        if (res) {
          /* this.loadingBar.progress$.subscribe(ev => {
            console.log("image_delete PROGRESS: ", ev);
          }); */
          this.toastr.info("Deleting...", "INFO");
          this.prints.print_delete(this.token, [this.item.id]).then(
            (data: any) => {
              if (data && data.success) {
                console.log("print_delete	: ", data);
                this.toastr.success(
                  "Brochure successfully deleted!",
                  "SUCCESS"
                );
                // this.loadingBar.progress$.unsubscribe();
                setTimeout(() => {
                  this.router.navigate(["/media"]);
                }, 600);
              } else if (data && !data.success) {
                this.toastr.warning(data.message, "WARNING");
              }
            },
            (error: any) => {
              console.log("error: ", error);
            }
          );
        }
      });
  }

  uploadMedia() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.initData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "update_print_policy",
      title: "Update Brochure File",
      printId: this.item.id
    };

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(UploadComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  addToCollection() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const initialState: any = {
      params: this.item,
      type: "print"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      CollectionChannelPickerComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  editEntry() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.refresh();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "update_print_policy",
      title: "Update Brochure File",
      printId: this.item.id
    };

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(PrintEntryComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
