import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  NgZone,
  ViewEncapsulation
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { ContextMenuComponent, ContextMenuService } from "ngx-contextmenu";
import { BroadcasterService } from "ng-broadcaster";
import { combineLatest, Subscription } from "rxjs";
import {
  DropzoneComponent,
  DropzoneDirective,
  DropzoneConfigInterface
} from "ngx-dropzone-wrapper";
import * as _ from "lodash";
import * as async from "async";
import * as $ from "jquery";
import * as xml2js from "xml2js";
import { ElectronService } from "ngx-electron";

import { PrintService } from "../../../services/print.service";
import { ImagesService } from "../../../services/images.service";
import { UtilitiesService } from "../../../services/utilities.service";
import { UploadService } from "../../../services/upload.service";

import { UploadComponent } from "../upload/upload.component";
import { ProductPickerComponent } from "../../product-picker/product-picker.component";
import { CollectionChannelPickerComponent } from "../../collection-channel-picker/collection-channel-picker.component";
import { PrintEntryComponent } from "./print-entry/print-entry.component";

import { environment } from "../../../../environments/environment";

@Component({
  selector: "app-prints",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./prints.component.html",
  styleUrls: ["./prints.component.css"]
})
export class PrintsComponent implements OnInit {
  token: string;
  business: any = {};
  action: string;

  imagesLoadingArr: any[] = [];
  printArr: any[] = [];
  printExpiredArr: any[] = [];
  printCopyArr: any[] = [];
  selectedMedia: any[] = [];
  contextMenu: any[] = [];
  categoriesArr: any[] = [];
  locationArr: any[] = [];
  creatorArr: any[] = [];

  accessKey: string;
  action_uri: string;
  file_name: string;
  policy: string;
  signature: string;
  file_key: string;
  contentType: string;
  successActionStatus: string;
  acl: string;

  printType: string;

  selectedItem: any = {};
  filters: any = {};
  showPreview: boolean = false;
  showUpload: boolean = false;
  showLoading: boolean = false;
  showDragDropUpload = false;

  showContentPrint: boolean = false;
  showContentPrintErr: boolean = false;
  printContentErr: any = {};
  pagePrint: number = 0;
  totalPagePrint: number = 0;
  fileAdded: number = 0;

  printstatus: string = "all";

  perPage = 100;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  @ViewChild(ContextMenuComponent)
  public basicMenu: ContextMenuComponent;

  @ViewChild(DropzoneDirective)
  componentRef?: DropzoneDirective;

  public config: DropzoneConfigInterface = {};

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private changeDetection: ChangeDetectorRef,
    private zone: NgZone,
    public prints: PrintService,
    public images: ImagesService,
    private modalService: BsModalService,
    public toastr: ToastrService,
    private contextMenuService: ContextMenuService,
    private utilities: UtilitiesService,
    private broadcaster: BroadcasterService,
    private uploadService: UploadService,
    private _electronService: ElectronService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
    for (let i = 0; i < 30; ++i) {
      this.imagesLoadingArr.push(i);
    }

    this.broadcaster.on<any>("uploadImageMedia").subscribe(() => {
      this.uploadMedia();
    });

    this.broadcaster.on<any>("refreshImage").subscribe(() => {
      console.log("refreshImage");
      this.refresh();
    });

    this.broadcaster.on<any>("updateImageDetail").subscribe(event => {
      console.log("updateImageDetail: ", event);
    });
  }

  ngOnInit() {
    this.pagePrint = 1;
    this.printArr = [];
    this.showContentPrint = false;
    this.showContentPrintErr = false;
    this.showLoading = true;
    this.showUpload = false;
    this.showDragDropUpload = false;

    if (
      this.business.type === 1 &&
      (this.business.role === 2 || this.business.role === 1)
    ) {
      this.showUpload = true;
    } else {
      this.showUpload = false;
    }

    this.config.dictDefaultMessage =
      "Click Here or Drag & Drop files to instantly upload them.";

    this.loadPrints(this.pagePrint, this.perPage);
    this.generateFilters();
    this.generatePrintPolicy();
  }

  private generateFilters() {
    this.categoriesArr = [];
    this.creatorArr = [];
    this.locationArr = [];

    this.images.search_options_image(this.token).then(
      (data: any) => {
        if (data && data.success) {
          const options = data.data;

          if (_.isArray(options.category)) {
            this.categoriesArr = options.category;
          }

          if (_.isArray(options.creator)) {
            this.creatorArr = options.creator;
          }

          if (_.isArray(options.locations)) {
            this.locationArr = options.locations;
          }
        }
      },
      error => {
        console.log("error search_options_image: ", error);
      }
    );
  }

  private loadPrints(page, total) {
    this.prints
      .brochure_list(this.token, {
        page: page,
        total: total
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            this.pagePrint = parseInt(data.data.currentPage, 0);
            this.totalPagePrint = parseInt(data.data.totalPages, 0);
            for (let i = 0; i < _.size(data.data.prints); i++) {
              data.data.prints[i].selected = false;
              if (_.isEmpty(data.data.prints[i].brochure_caption)) {
                data.data.prints[i].brochure_caption = "No caption";
              }
              this.printCopyArr.push(data.data.prints[i]);
            }
            this.printArr = _.cloneDeep(this.printCopyArr);
            console.log("this.printArr: ", this.printArr);
            this.printSegmentChanged(this.printstatus);
          } else if (data && data.success && data.data[0] === false) {
            this.showContentPrintErr = true;
            this.printContentErr = {
              message: "No results found"
            };
          }

          this.showContentPrint = true;
          this.showLoading = false;

          setTimeout(() => {
            $(".progressive-image").each(function() {
              const image = new Image();
              const previewImage = $(this).find(".loadingImage");
              const newImage = $(this).find(".overlay");
              image.src = previewImage.data("image");
              image.onload = function() {
                newImage.css("background-image", "url(" + image.src + ")");
                newImage.css("opacity", "1");
              };
              image.onerror = function() {
                newImage.css(
                  "background-image",
                  "url(../assets/img/thumb.jpg)"
                );
                newImage.css("opacity", "1");
              };
            });
          }, 1000);
        },
        (error: any) => {
          this.showContentPrint = true;
          this.showContentPrintErr = true;

          if (error && !error.success) {
            this.printContentErr = error;
          }
        }
      );
  }

  private generatePrintPolicy() {
    this.showLoading = true;
    this.uploadService.upload_print_policy(this.token).then(
      (data: any) => {
        if (data && data.success) {
          console.log("upload_print_policy: ", data.data);
          this.accessKey = data.data.accessKey;
          this.action_uri = data.data.action_uri;
          this.file_name = data.data.name;
          this.file_key = data.data.key;
          this.policy = data.data.policy;
          this.signature = data.data.signature;
          this.contentType = data.data.contentType;
          this.successActionStatus = data.data.successActionStatus;
          this.acl = data.data.acl;
        }
        this.showLoading = false;
        this.showUpload = true;

        const vm = this;
        this.config.url = this.action_uri;
        (this.config.acceptedFiles = this.contentType),
          (this.config.paramName = this.file_name);
        this.config.autoProcessQueue = true;

        this.config.init = function() {
          const dz = this;
          dz.on("sending", function(file, xhr, formData) {
            formData.append("acl", vm.acl);
            formData.append("Policy", vm.policy);
            formData.append("X-Amz-Signature", vm.signature);
            formData.append("AWSAccessKeyId", vm.accessKey);
            formData.append("Signature", vm.signature);
            formData.append("success_action_status", vm.successActionStatus);
            formData.append("Content-Type", file.type);
            formData.append("key", vm.file_key.split("/")[0] + "/" + file.name);
          });

          dz.on("addedfile", function(file) {
            if (file.size < environment.printFileSize) {
              console.log("WARNING");
              vm.zone.run(() => {
                vm.toastr.warning(
                  "Your proposed upload file is smaller than the minimum allowed size of " +
                    vm.formatBytes(environment.imageFileSize, 0),
                  "WARNING"
                );
                setTimeout(() => {
                  vm.componentRef.dropzone().removeFile(file);
                }, 600);
              });
              return;
            }
            vm.fileAdded += 1;
            console.log("addedfile: ", vm.fileAdded);
            vm.changeDetection.detectChanges();
          });

          dz.on("success", function(file) {
            setTimeout(() => {
              vm.fileAdded -= 1;
              console.log("success: ", vm.fileAdded);
              vm.componentRef.dropzone().removeFile(file);

              if (vm.fileAdded === 0) {
                setTimeout(() => {
                  vm.zone.run(() => {
                    vm.showDragDropUpload = false;
                    vm.refresh();
                  });
                }, 600);
              }
            }, 1000);
          });

          dz.on("uploadprogress", function(file, progress, bytesSent) {
            // Display the progress
            console.log("progress: ", progress);
          });
        };
      },
      error => {
        console.error("error: ", error);
        this.showLoading = false;
        this.showUpload = false;
      }
    );
  }

  formatBytes = (bytes, decimals) => {
    if (bytes === 0) {
      return "0 Bytes";
    }

    const k = 1024,
      dm = decimals <= 0 ? 0 : decimals || 2,
      sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
      i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  };

  selectMedia(item: any, event) {
    if (event.ctrlKey || event.metaKey) {
      if (_.find(this.selectedMedia, { id: item.id })) {
        item.selected = false;
        _.remove(this.selectedMedia, (row: any) => {
          return row.id === item.id;
        });
      } else {
        item.selected = true;
        this.selectedMedia.push(item);
      }
    } else {
      item.selected = !item.selected;
      _.each(this.printArr, (row: any) => {
        if (row.id !== item.id) {
          row.selected = false;
        }
      });
      if (item.selected) {
        this.selectedItem = item;
      }
    }
  }

  printSegmentChanged(status) {
    const date1 = new Date();
    date1.setHours(0, 0, 0, 0);
    this.printstatus = status;

    if (this.printstatus === "all") {
      if (this.printType) {
        this.filterPrint(this.printType);
      } else {
        this.printArr = _.cloneDeep(this.printCopyArr);
      }
    } else if (this.printstatus === "expired") {
      this.printExpiredArr = _.filter(this.printCopyArr, (row: any) => {
        const date2 = new Date(row.validityDate);
        date2.setHours(0, 0, 0, 0);
        return date2 <= date1;
      });
    }

    console.log("this.printstatus: ", this.printstatus);

    setTimeout(() => {
      $(".progressive-image").each(function() {
        const image = new Image();
        const previewImage = $(this).find(".loadingImage");
        const newImage = $(this).find(".overlay");
        image.src = previewImage.data("image");
        image.onload = function() {
          newImage.css("background-image", "url(" + image.src + ")");
          newImage.css("opacity", "1");
        };
        image.onerror = function() {
          newImage.css("background-image", "url(../assets/img/thumb.jpg)");
          newImage.css("opacity", "1");
        };
      });
    }, 1000);
  }

  filterPrint(type) {
    const params: any = {};
    params.media = "print";
    params.type = type;
    this.printType = type;

    this.showContentPrint = false;
    if (type) {
      this.printArr = _.filter(this.printCopyArr, { type: type });
    } else {
      this.printArr = _.cloneDeep(this.printCopyArr);
    }
    console.log("this.printArr: ", this.printArr);
    setTimeout(() => {
      this.showContentPrint = true;
      this.showContentPrintErr = false;

      setTimeout(() => {
        $(".progressive-image").each(function() {
          const image = new Image();
          const previewImage = $(this).find(".loadingImage");
          const newImage = $(this).find(".overlay");
          image.src = previewImage.data("image");
          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(../assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
          };
        });
      }, 600);
    }, 2000);
  }

  refresh() {
    this.pagePrint = 1;
    this.printArr = [];

    this.showContentPrint = false;
    this.showContentPrintErr = false;

    this.loadPrints(this.pagePrint, this.perPage);
  }

  viewDetail(item: any) {
    console.log("viewDetail: ", item);
    this.router.navigate(["/media/prints/", item.id]);
  }

  viewMore() {
    if (this.action === "connect") {
    } else {
      this.pagePrint = this.pagePrint + 1;
      if (this.pagePrint <= this.totalPagePrint) {
        this.showLoading = true;
        this.loadPrints(this.pagePrint, this.perPage);
      }
    }
  }

  cancelUploadMedia() {
    this.showDragDropUpload = false;
    this.componentRef.dropzone().removeAllFiles(true);

    setTimeout(() => {
      $(".progressive-image").each(function() {
        const image = new Image();
        const previewImage = $(this).find(".loadingImage");
        const newImage = $(this).find(".overlay");
        image.src = previewImage.data("image");
        image.onload = function() {
          newImage.css("background-image", "url(" + image.src + ")");
          newImage.css("opacity", "1");
        };
        image.onerror = function() {
          newImage.css("background-image", "url(../assets/img/thumb.jpg)");
          newImage.css("opacity", "1");
        };
      });
    }, 1000);
  }

  uploadMedia() {
    console.log("uploadMedia");
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    if (this.showDragDropUpload) {
      this.toastr.warning(
        "Unable to proceed, file upload is on-going!",
        "WARNING"
      );
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.refresh();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "print",
      title: "Upload Image"
    };

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(UploadComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  allowDrop(ev) {
    ev.preventDefault();

    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      this.showDragDropUpload = false;
      return;
    }

    if (!this.showContentPrint) {
      return;
    }

    this.showDragDropUpload = true;
  }

  onUploadError(args: any): void {
    console.log("onUploadError:", args);
    if (args && _.isArray(args) && args[0].status === "error") {
      this.parseXML(args[1]).then((data: any) => {
        console.log("parseXML: ", data);
        if (data && data.Error) {
          this.toastr.error(data.Error.Message[0], "ERROR");
        } else {
          this.toastr.error(args[1], "ERROR");
        }
      });
      this.componentRef.dropzone().removeFile(args[0]);
      return;
    } else if (args && _.isArray(args) && args[0].status === "canceled") {
      this.toastr.warning(args[1], "WARNING");
      return;
    }
  }

  onUploadSuccess(args: any): void {
    console.log("onUploadSuccess:", args);
    if (args && _.isArray(args) && args[0].status === "success") {
      this.toastr.success(args[1] || "File upload successfully!", "SUCCESS");
    }
  }

  private parseXML(data) {
    return new Promise(resolve => {
      const parser = new xml2js.Parser({
        trim: true
      });

      parser.parseString(data, function(err, result) {
        console.log("result: ", result);
        resolve(result);
      });
    });
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }

  drop(ev) {
    ev.preventDefault();
    const data = ev.dataTransfer.getData("text");
    console.log("drop data: ", data);
  }

  private addToProductFeature(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const initialState: any = {
      image: item,
      action: "feature",
      type: "image"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      ProductPickerComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private addPrintToCollection(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const initialState: any = {
      params: item,
      type: "print"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      CollectionChannelPickerComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private deletePrint(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.prints.print_delete(this.token, [item.id]).then(
      (data: any) => {
        if (data && data.success) {
          console.log("print_delete: ", data);
          this.toastr.success("Print sucessfully deleted", "WARNING");
          setTimeout(() => {
            this.refresh();
          }, 600);
        }
      },
      (error: any) => {
        console.log("error: ", error);
      }
    );
  }

  private updatePrintFile(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.refresh();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "update_print_policy",
      title: "Update Brochure File",
      printId: item.id
    };

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(UploadComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private updatePrintInfo(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const initialState: any = {
      action: "update_print_policy",
      title: "Update Brochure File",
      printId: item.id
    };

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(PrintEntryComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private downloadImage(item: any) {
    const ctrl = this;
    if (!this.business.paid) {
      this.toastr.warning(
        "This feature is only available to premium users only.",
        "WARNING"
      );
      return;
    }

    this.toastr.info("Downloading ...");
    function download(dataurl, filename) {
      const a = document.createElement("a");
      a.href = dataurl;
      a.setAttribute("download", filename);
      const b = document.createEvent("MouseEvents");
      b.initEvent("click", false, true);
      a.dispatchEvent(b);
      return false;
    }

    function getFilename(url) {
      url = url
        .split("/")
        .pop()
        .replace(/\#(.*?)$/, "")
        .replace(/\?(.*?)$/, "");
      url = url.split("."); // separates filename and extension
      return { filename: url[0] || "", ext: url[1] || "" };
    }

    this.prints.brochure_download(this.token, item.id).then(
      (data: any) => {
        if (data && data.success) {
          const filename =
            getFilename(data.data).filename + "." + getFilename(data.data).ext;
          if (!window["electron"]) {
            download(data.data, filename);
            this.toastr.success("Brochure successfully downloaded", "SUCCESS");
          } else {
            // DOWNLOAD NATIVE VIA ELECTRON
            download(data.data, filename);
          }
        }
      },
      error => {
        if (error && !error.success) {
          this.toastr.error(
            "Error while downloading file. " + error.message,
            "ERROR"
          );
          return;
        }
      }
    );
  }
}
