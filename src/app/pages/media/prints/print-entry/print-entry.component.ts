import { Component, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as async from "async";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";

import { PrintService } from "src/app/services/print.service";

declare let $: any;

@Component({
  selector: "app-print-entry",
  templateUrl: "./print-entry.component.html",
  styleUrls: ["./print-entry.component.css"]
})
export class PrintEntryComponent implements OnInit {
  image: any = {};
  item: any = {};
  business: any = {};
  token: string;
  printId: string;

  showLoading: boolean;
  isSaving: boolean;
  submitted: boolean;

  entryForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    public toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public prints: PrintService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }

  ngOnInit() {
    this.showLoading = false;
    this.isSaving = false;
    this.submitted = false;

    this.entryForm = this.formBuilder.group({
      id: ["", Validators.required],
      brochure_caption: ["", Validators.required],
      caption: ["", Validators.required],
      type: ["", Validators.required],
      privacy: ["", Validators.required],
      validityDate: ["", Validators.required]
    });

    const options = {
      autoHide: true,
      autoPick: true,
      format: "yyyy-mm-dd",
      zIndex: "10001"
    };
    $(document).ready(function() {
      $("[data-toggle='datepicker']").datepicker(options);
    });

    this.initData();
  }

  get f(): any {
    return this.entryForm.controls;
  }

  private toggleFormState() {
    const state = this.isSaving ? "disable" : "enable";
    Object.keys(this.entryForm.controls).forEach(controlName => {
      this.entryForm.controls[controlName][state](); // disables/enables each form control based on 'this.formDisabled'
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  private initData() {
    const successResponse = (data: any) => {
      if (data && data.success) {
        this.item = data.data;
        this.item.caption = this.item.brochure_caption;
        this.item.size = parseFloat(this.item.size);
        console.log("this.item: ", this.item);
      }
      if (this.item) {
        this.item.privacy = this.item.privilege;
        /* this.item.validityDate = $("[data-toggle='datepicker']").datepicker(
          "getDate",
          true
        ); */
        $("[data-toggle='datepicker']").datepicker(
          "setDate",
          this.item.validityDate
        );
        this.entryForm.patchValue(this.item);
      }
      this.showLoading = false;

      setTimeout(() => {
        $(".progressive-image").each(function() {
          const image = new Image();
          const previewImage = $(this).find(".loadingImage");
          const newImage = $(this).find(".overlay");

          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(./assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
            console.log("complete");
          };
          image.src = previewImage.data("image");
        });
      }, 1000);
    };

    const errorResponse = error => {
      this.showLoading = false;
      console.log("Error: ", error);
    };

    this.prints
      .brochure_detail(this.token, this.printId)
      .then(successResponse, errorResponse);
  }

  saveChanges() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.entryForm.patchValue({
      validityDate: $("[data-toggle='datepicker']").datepicker("getDate", true),
      caption:
        this.item.brochure_caption || this.entryForm.value.brochure_caption
    });

    this.submitted = true;
    this.toggleFormState();

    console.log("this.entryForm.invalid: ", this.entryForm.invalid);
    console.log("this.entryForm.value: ", this.entryForm.value);
    console.log("this.entryForm: ", this.entryForm);

    if (this.entryForm.invalid) {
      this.isSaving = false;
      this.validateAllFormFields(this.entryForm);
      this.toggleFormState();
      this.toastr.warning("Fillup the required fields!");
      return;
    }

    const saveEntry = () => {
      this.isSaving = true;
      this.toastr.info("SAVING...", "INFO");
      this.prints.edit_print(this.token, this.entryForm.value).then(
        (data: any) => {
          if (data && data.success) {
            this.isSaving = false;
            this.submitted = false;
            this.toastr.success(data.data, "SUCCESS");
            this.modalService.setDismissReason("save");
            this.bsModalRef.hide();
          } else {
            this.isSaving = false;
            this.submitted = false;
            this.toastr.warning(data.messsage, "WARNING!");
          }
          this.toggleFormState();
        },
        error => {
          console.log("error: ", error);
          this.isSaving = false;
          this.submitted = false;
          this.toggleFormState();
        }
      );
    };

    this.coolDialogs.confirm("Do you want to save entry?").subscribe(res => {
      if (res) {
        saveEntry();
      }
    });
  }
}
