import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router, NavigationExtras } from "@angular/router";
import * as _ from "lodash";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { combineLatest, Subscription } from "rxjs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { LocationsService } from "../../services/locations.service";
import { LocationEntryComponent } from "./location-entry/location-entry.component";

@Component({
  selector: "app-locations",
  templateUrl: "./locations.component.html",
  styleUrls: ["./locations.component.css"]
})
export class LocationsComponent implements OnInit {
  token: string;
  business: any = {};
  showContent: boolean = false;
  showContentError: boolean = false;
  contentErr: any = {};

  locationsArr: any[] = [];
  fakeArr: any[] = [];

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private modalService: BsModalService,
    private changeDetection: ChangeDetectorRef,
    public locations: LocationsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    for (let i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }
  }

  ngOnInit() {
    this.initializeData();
  }

  private initializeData() {
    this.showContent = false;
    this.locations.get_locations(this.token).then(
      (data: any) => {
        if (data && data.success) {
          if (_.isArray(data.data) && data.data[0] !== false) {
            this.locationsArr = data.data;
          }
        }
        this.showContent = true;
        this.showContentError = false;
      },
      error => {
        this.showContent = true;
        this.showContentError = true;
        if (error && !error.success) {
          this.contentErr = error;
        }
      }
    );
  }

  refresh() {
    this.initializeData();
  }

  deleteLocation(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }
    const deleteLocation = () => {
      this.toastr.info("DELETING...", "INFO");
      this.locations.delete_locations(this.token, item.id).then(
        (data: any) => {
          if (data && data.success) {
            console.log("data: ", data);
            this.initializeData();
          } else {
            this.toastr.warning(data.message, "WARNING");
          }
        },
        error => {
          console.log("error: ", error);
        }
      );
    };

    this.coolDialogs
      .confirm("Are you sure to delete this location?")
      .subscribe(res => {
        if (res) {
          deleteLocation();
        }
      });
  }

  updateLocation(item: any) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.initializeData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      id: item.id,
      item: item,
      action: "update"
    };
    console.log("initialState: ", initialState);

    const modalConfig: any = {
      animated: true,
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      LocationEntryComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  addLocation() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.initializeData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      item: {},
      action: "add"
    };
    console.log("initialState: ", initialState);

    const modalConfig: any = {
      animated: true,
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      LocationEntryComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
