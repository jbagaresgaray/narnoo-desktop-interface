import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  NgZone,
  ViewEncapsulation
} from "@angular/core";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { LocationsService } from "src/app/services/locations.service";

@Component({
  selector: "app-location-entry",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./location-entry.component.html",
  styleUrls: ["./location-entry.component.css"]
})
export class LocationEntryComponent implements OnInit {
  token: string;
  action: string;
  item: any = {};
  business: any = {};
  locationId: any;

  isSaving = false;
  submitted = false;

  constructor(
    public bsModalRef: BsModalRef,
    private changeDetection: ChangeDetectorRef,
    public zone: NgZone,
    public toastr: ToastrService,
    public coolDialog: NgxCoolDialogsService,
    private modalService: BsModalService,
    private locations: LocationsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
    this.locationId = this.item.id;
  }

  ngOnInit() {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
    this.locationId = this.item.id;
  }

  saveChanges() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const saveLocation = () => {
      this.toastr.info("Saving...", "INFO");
      this.submitted = true;
      this.isSaving = true;

      if (this.locationId) {
        this.locations
          .edit_location(this.token, {
            id: this.locationId,
            name: this.item.name
          })
          .then(
            (data: any) => {
              this.submitted = false;
              this.isSaving = false;

              if (data && data.success) {
                console.log("data: ", data);
                this.toastr.success(
                  "Location was updated successfully",
                  "SUCCESS"
                );
                this.modalService.setDismissReason("save");
                setTimeout(() => {
                  this.bsModalRef.hide();
                }, 600);
              } else {
                this.toastr.success(data.message, "WARNING");
              }
            },
            error => {
              console.log("error: ", error);
              this.submitted = false;
              this.isSaving = false;
            }
          );
      } else {
        this.locations
          .add_location(this.token, {
            name: this.item.name
          })
          .then(
            (data: any) => {
              if (data && data.success) {
                console.log("data: ", data);
                this.toastr.success(
                  "Location was added successfully",
                  "SUCCESS"
                );
                this.modalService.setDismissReason("save");
                setTimeout(() => {
                  this.bsModalRef.hide();
                }, 600);
              } else {
                this.toastr.success(data.message, "WARNING");
              }
            },
            error => {
              console.log("error: ", error);
              this.submitted = false;
              this.isSaving = false;
            }
          );
      }
    };

    this.coolDialog.confirm("Do you want to save the entry?").subscribe(res => {
      if (res) {
        saveLocation();
      }
    });
  }
}
