import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PipesModule } from "../../pipes/pipes.module";
import { BsDropdownModule } from "ngx-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { ComponentsModule } from "../../components/components.module";

import { LocationsComponent } from "./locations.component";
import { LocationEntryComponent } from "./location-entry/location-entry.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule,
    BsDropdownModule,
    ComponentsModule
  ],
  declarations: [LocationsComponent, LocationEntryComponent],
  entryComponents: [LocationEntryComponent]
})
export class LocationsModule {}
