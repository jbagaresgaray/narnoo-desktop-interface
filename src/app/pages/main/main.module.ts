import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PipesModule } from '../../pipes/pipes.module';

import { MainComponent } from './main.component';

@NgModule({
	imports: [
		CommonModule,
		RouterModule,
		PipesModule
	],
	declarations: [
		MainComponent
	]
})
export class MainModule { }
