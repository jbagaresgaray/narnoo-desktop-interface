import { Component, OnInit } from "@angular/core";
import { Router, NavigationExtras } from "@angular/router";
import * as _ from "lodash";
import * as $ from "jquery";
import { ToastrService } from "ngx-toastr";

import { NewsfeedService } from "../../services/newsfeed.service";
import { ImagesService } from "../../services/images.service";
import { PrintService } from "../../services/print.service";
import { LogosService } from "../../services/logos.service";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.css"]
})
export class MainComponent implements OnInit {
  token: string;
  business: any = {};
  actions: string = "products";

  feedsArr: any[] = [];
  fakeArr: any[] = [];

  showContent: boolean = false;
  isMobile: boolean;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    public newsfeed: NewsfeedService,
    public images: ImagesService,
    public print: PrintService,
    public logos: LogosService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    for (let i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }
  }

  ngOnInit() {
    this.initData();
  }

  initData(event?: any) {
    this.showContent = false;
    this.newsfeed.feed_activity(this.token).then(
      (data: any) => {
        if (data && data.success) {
          this.feedsArr = [];
          _.each(data.data, (row: any) => {
            if (_.isObject(row)) {
              if (row.itemType == "images") {
                row.moreCount = _.size(row.media) - 4;
              }
              this.feedsArr.push(row);
            }
          });

          setTimeout(() => {
            $(".progressive-image").each(function() {
              const image = new Image();
              const previewImage = $(this).find(".loadingImage");
              const newImage = $(this).find(".overlay");

              image.onload = function() {
                newImage.css("background-image", "url(" + image.src + ")");
                newImage.css("opacity", "1");
              };
              image.src = previewImage.data("image");
            });
          }, 300);
        }
        this.showContent = true;
      },
      error => {
        this.showContent = true;
      }
    );
  }

  viewProfile() {
    this.router.navigate(["/profile"]);
  }

  viewImageDetail(media: any) {
    this.router.navigate(["/images"]);
  }

  downloadImage(item: any) {
    if (item.businessType === 1) {
      item.businessType = "Operator";
    } else if (item.businessType === 2) {
      item.businessType = "Distributor";
    }

    if (
      item.itemType === "image" ||
      item.itemType === "following" ||
      item.itemType === "joined"
    ) {
      const navigationExtras: NavigationExtras = {
        queryParams: {
          type: "image",
          item: item,
          business: {
            id: item.businessId,
            businessType: item.businessType
          }
        }
      };
      this.router.navigate(["/activity"], navigationExtras);
    } else if (item.itemType === "brochure") {
      const navigationExtras: NavigationExtras = {
        queryParams: {
          type: "brochure",
          item: item,
          business: {
            id: item.businessId,
            businessType: item.businessType
          }
        }
      };
      this.router.navigate(["/activity"], navigationExtras);
    } else if (item.itemType === "logo") {
      const navigationExtras: NavigationExtras = {
        queryParams: {
          type: "logo",
          item: item,
          business: {
            id: item.businessId,
            businessType: item.businessType
          }
        }
      };
      this.router.navigate(["/activity"], navigationExtras);
    } else if (item.itemType === "video") {
      const navigationExtras: NavigationExtras = {
        queryParams: {
          type: "video",
          item: item,
          business: {
            id: item.businessId,
            businessType: item.businessType
          }
        }
      };
      this.router.navigate(["/activity"], navigationExtras);
    } else if (item.itemType === "album") {
      const navigationExtras: NavigationExtras = {
        queryParams: {
          type: "album",
          item: item,
          business: {
            id: item.businessId,
            businessType: item.businessType
          }
        }
      };
      this.router.navigate(["/activity"], navigationExtras);
    }
  }
}
