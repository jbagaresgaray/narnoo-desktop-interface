import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  NgZone,
  ViewEncapsulation
} from "@angular/core";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import * as $ from "jquery";
import * as _ from "lodash";
import * as async from "async";

import { ProductsService } from "../../services/products.service";

@Component({
  selector: "app-product-picker",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./product-picker.component.html",
  styleUrls: ["./product-picker.component.css"]
})
export class ProductPickerComponent implements OnInit {
  action: string;
  type: string;
  title: string;
  token: string;

  image: any = {};
  business: any = {};
  selected: any = {};

  txtSearch: string = "";

  perPage = 16;
  showLoading: boolean = false;
  isSaving: boolean = false;

  showContentProducts = false;
  showContentProductsErr = false;
  productsContentErr: any = {};
  pageProduct: number = 1;
  totalPageProduct: number = 0;

  productArr: any[] = [];
  productArrCopy: any[] = [];

  imagesLoadingArr: any[] = [];

  constructor(
    public bsModalRef: BsModalRef,
    private changeDetection: ChangeDetectorRef,
    public zone: NgZone,
    public toastr: ToastrService,
    public coolDialog: NgxCoolDialogsService,
    private modalService: BsModalService,
    private products: ProductsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
    for (let i = 0; i < 16; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  ngOnInit() {
    console.log("this.action: ", this.action);
    console.log("this.type: ", this.type);
    console.log("this.image: ", this.image);

    this.pageProduct = 1;
    this.showContentProducts = false;
    this.showContentProductsErr = false;
    this.loadProducts(this.pageProduct, this.perPage);
  }

  private loadProducts(page, total) {
    const successResponse = (data: any) => {
      if (data && data.success) {
        this.productArr = [];
        this.productArrCopy = [];

        const products = data.data.products;
        this.pageProduct = parseInt(data.data.currentPage, 0);
        this.totalPageProduct = parseInt(data.data.totalPages, 0);

        for (let i = 0; i < _.size(products); i++) {
          products[i].selected = false;
          this.productArr.push(products[i]);
        }
        this.productArrCopy = _.cloneDeep(this.productArr);
        console.log("productArr: ", this.productArr);
      }
      this.showLoading = false;
      this.showContentProducts = true;
      this.showContentProductsErr = false;
    };

    const errorResponse = (error: any) => {
      this.showContentProducts = true;
      this.showContentProductsErr = true;
      this.showLoading = false;
      if (error && !error.success) {
        this.productsContentErr = error;
      }
    };

    this.showContentProducts = false;
    this.products
      .product_list(this.token, {
        page: page,
        total: total
      })
      .then(successResponse, errorResponse);
  }

  private async addImage(productId) {
    this.toastr.info("Featuring...", "INFO");
    this.isSaving = true;
    await this.products
      .product_add_image(this.token, {
        productId: productId,
        image: [this.image.id]
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            this.toastr.success(data.data, "SUCCESS");
          } else if (data && !data.success) {
            this.toastr.warning(data.message, "WARNING");
          }
          this.isSaving = false;
        },
        error => {
          console.log("error: ", error);
          this.isSaving = false;
          if (error && !error.success) {
            this.toastr.error(error.message, "ERROR");
          }
        }
      );
  }

  private async featureImage(productId) {
    this.toastr.info("Featuring...", "INFO");
    this.isSaving = true;
    await this.products
      .product_set_feature_item(this.token, {
        productId: productId,
        mediaId: this.image.id,
        type: this.type
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            console.log("success: ", data);
            this.toastr.success(data.data, "SUCCESS");
          } else if (data && !data.success) {
            this.toastr.warning(data.message, "WARNING");
          }
          this.isSaving = false;
        },
        (error: any) => {
          console.log("error: ", error);
          this.isSaving = false;
          if (error && !error.success) {
            this.toastr.error(error.message, "ERROR");
          }
        }
      );
  }

  selectMedia(item: any) {
    this.selected = item;
    _.each(this.productArr, (row: any) => {
      if (row.id !== item.id) {
        row.selected = false;
      } else {
        row.selected = true;
      }
    });
  }

  viewMore(event: any) {
    console.log("Page changed to: " + event.page);
    console.log("Number items per page: " + event.itemsPerPage);

    this.pageProduct = event.page;
    this.showLoading = true;
    this.showContentProducts = false;
    this.showContentProductsErr = false;

    this.loadProducts(this.pageProduct, this.perPage);
  }

  saveChanges() {
    const selectedProduct = _.filter(this.productArr, (row: any) => {
      return row.selected;
    });

    if (this.action === "add_image") {
      this.coolDialog
        .confirm(
          "Are you sure to add the selected images as to product gallery?"
        )
        .subscribe(res => {
          if (res) {
            async.eachSeries(
              selectedProduct,
              (item, callback) => {
                this.addImage(item.productId).then(() => {
                  callback();
                });
              },
              () => {
                this.modalService.setDismissReason("save");
                setTimeout(() => {
                  this.bsModalRef.hide();
                }, 300);
              }
            );
          }
        });
    } else if (this.action === "feature") {
      this.coolDialog
        .confirm("Are you sure to add the selected " + this.type + "?")
        .subscribe(res => {
          if (res) {
            async.eachSeries(
              selectedProduct,
              (item, callback) => {
                this.featureImage(item.productId).then(() => {
                  callback();
                });
              },
              () => {
                this.modalService.setDismissReason("save");
                setTimeout(() => {
                  this.bsModalRef.hide();
                }, 300);
              }
            );
          }
        });
    }
  }
}
