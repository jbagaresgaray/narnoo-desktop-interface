import { ProductPickerModule } from './product-picker.module';

describe('ProductPickerModule', () => {
  let productPickerModule: ProductPickerModule;

  beforeEach(() => {
    productPickerModule = new ProductPickerModule();
  });

  it('should create an instance', () => {
    expect(productPickerModule).toBeTruthy();
  });
});
