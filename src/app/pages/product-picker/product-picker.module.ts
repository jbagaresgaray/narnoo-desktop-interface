import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";
import { BsDropdownModule, PaginationModule } from "ngx-bootstrap";
import { FormsModule } from "@angular/forms";

import { ProductPickerComponent } from "./product-picker.component";

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    PipesModule,
    BsDropdownModule,
    PaginationModule,
    FormsModule
  ],
  declarations: [ProductPickerComponent],
  entryComponents: [ProductPickerComponent]
})
export class ProductPickerModule {}
