import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchBusinessComponent } from './search-business.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SearchBusinessComponent]
})
export class SearchBusinessModule { }
