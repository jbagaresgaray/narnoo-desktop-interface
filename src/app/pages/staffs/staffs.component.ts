import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router, NavigationExtras } from "@angular/router";
import * as md5 from "crypto-js/md5";
import * as _ from "lodash";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { combineLatest, Subscription } from "rxjs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";

import { StaffService } from "../../services/staff.service";
import { StaffEntryComponent } from "./staff-entry/staff-entry.component";

@Component({
  selector: "app-staffs",
  templateUrl: "./staffs.component.html",
  styleUrls: ["./staffs.component.css"]
})
export class StaffsComponent implements OnInit {
  contactsArr: any[] = [];
  contactsArrCopy: any[] = [];
  fakeArr: any[] = [];

  business: any = {};
  contentErr: any = {};

  token: string;
  showContent: boolean = false;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private modalService: BsModalService,
    private changeDetection: ChangeDetectorRef,
    public staffs: StaffService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    for (let i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }
  }

  ngOnInit() {
    this.initializeData();
  }

  private initializeData() {
    this.showContent = false;
    this.staffs.staff_list(this.token).then(
      (data: any) => {
        if (data && data.success) {
          _.each(data.data, (row: any) => {
            row.gravatar =
              "https://www.gravatar.com/avatar/" +
              md5(row.email.toLowerCase(), "hex");
          });
          this.contactsArr = data.data;
          this.contactsArrCopy = data.data;

          this.showContent = true;
        }
      },
      error => {
        this.showContent = true;
        if (error) {
          this.contentErr = error;
        }
      }
    );
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }

  createStaff() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.initializeData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      type: "staff",
      action: "add"
    };
    console.log("initialState: ", initialState);

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(StaffEntryComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  deleteStaff(item) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.coolDialogs
      .confirm("Are you sure to delete this Staff?")
      .subscribe(res => {
        if (res) {
          this.staffs.staff_remove(this.token, item).then(
            (data: any) => {
              if (data && data.success) {
                this.initializeData();
              }
            },
            error => {
              if (error && !error.success) {
                this.toastr.error(error.message, "ERROR");
                return;
              }
            }
          );
        } else {
          console.log("You clicked Cancel. You smart.");
        }
      });
  }

  updateStaff(item) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.initializeData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      item: _.cloneDeep(item),
      type: "staff",
      action: "update"
    };
    console.log("initialState: ", initialState);

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(StaffEntryComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  refresh() {
    this.initializeData();
  }

  onCancel(ev) {
    this.contactsArr = this.contactsArrCopy;
  }

  getItems(ev: any) {
    this.showContent = false;
    const val = ev.target.value;
    if (val && val.trim() !== "") {
      this.contactsArr = this.contactsArrCopy.filter((item: any) => {
        return item.name.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
      this.showContent = true;
    } else {
      this.contactsArr = this.contactsArrCopy;
    }
  }
}
