import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PipesModule } from "../../pipes/pipes.module";
import { BsDropdownModule } from "ngx-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { ComponentsModule } from "../../components/components.module";

import { StaffsComponent } from "./staffs.component";
import { StaffEntryComponent } from "./staff-entry/staff-entry.component";

@NgModule({
  imports: [
    CommonModule,
    PipesModule,
    BsDropdownModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [StaffsComponent, StaffEntryComponent],
  entryComponents: [StaffEntryComponent]
})
export class StaffsModule {}
