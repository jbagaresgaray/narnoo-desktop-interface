import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  NgZone,
  ViewEncapsulation
} from "@angular/core";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { StaffService } from "src/app/services/staff.service";

@Component({
  selector: "app-staff-entry",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./staff-entry.component.html",
  styleUrls: ["./staff-entry.component.css"]
})
export class StaffEntryComponent implements OnInit {
  token: string;
  action: string;
  item: any = {};
  business: any = {};
  type: any;

  isSaving = false;
  submitted = false;

  entryForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public bsModalRef: BsModalRef,
    private changeDetection: ChangeDetectorRef,
    public zone: NgZone,
    public toastr: ToastrService,
    public coolDialog: NgxCoolDialogsService,
    private modalService: BsModalService,
    private staffs: StaffService
  ) {}

  ngOnInit() {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    this.isSaving = false;
    this.submitted = false;

    this.entryForm = this.formBuilder.group({
      name: ["", Validators.required],
      email: ["", Validators.required],
      title: ["", Validators.required],
      phone: ""
    });
  }

  get f(): any {
    return this.entryForm.controls;
  }

  private toggleFormState() {
    const state = this.isSaving ? "disable" : "enable";
    Object.keys(this.entryForm.controls).forEach(controlName => {
      this.entryForm.controls[controlName][state](); // disables/enables each form control based on 'this.formDisabled'
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  saveChanges() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.submitted = true;
    this.toggleFormState();

    if (this.entryForm.invalid) {
      this.isSaving = false;
      this.validateAllFormFields(this.entryForm);
      this.toggleFormState();
      return;
    }

    const saveStaffEntry = () => {
      this.isSaving = true;

      this.toastr.info("Saving...", "INFO");
      if (this.action === "add") {
        this.staffs.staff_add(this.token, this.item).then(
          (data: any) => {
            if (data && data.success) {
              this.submitted = false;
              this.isSaving = false;
              this.toastr.success("Staff successfully saved!", "SUCCESS");
              this.modalService.setDismissReason("save");
              setTimeout(() => {
                this.bsModalRef.hide();
              }, 600);
            } else if (data && !data.success) {
              this.submitted = false;
              this.isSaving = false;
              this.toastr.warning(data.message, "WARNING");
            }
          },
          error => {
            console.log("eRror: ", error);
            this.submitted = false;
            this.isSaving = false;

            if (error && !error.success) {
              this.toastr.error(error.message, "WARNING");
            }
          }
        );
      } else {
        this.staffs.staff_edit(this.token, this.item).then(
          (data: any) => {
            if (data && data.success) {
              this.submitted = false;
              this.isSaving = false;
              this.toastr.success("Staff successfully saved!", "SUCCESS");
              this.modalService.setDismissReason("save");
              setTimeout(() => {
                this.bsModalRef.hide();
              }, 600);
            } else if (data && !data.success) {
              this.submitted = false;
              this.isSaving = false;
              this.toastr.warning(data.message, "WARNING");
            }
          },
          error => {
            console.log("eRror: ", error);
            this.submitted = false;
            this.isSaving = false;

            if (error && !error.success) {
              this.toastr.error(error.message, "WARNING");
            }
          }
        );
      }
    };

    this.coolDialog.confirm("Do you want to save the entry?").subscribe(res => {
      if (res) {
        saveStaffEntry();
      }
    });
  }
}
