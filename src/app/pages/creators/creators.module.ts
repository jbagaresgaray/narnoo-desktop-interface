import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PipesModule } from "../../pipes/pipes.module";
import { BsDropdownModule } from "ngx-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { ComponentsModule } from "../../components/components.module";

import { CreatorsComponent } from "./creators.component";
import { CreatorEntryComponent } from "./creator-entry/creator-entry.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule,
    BsDropdownModule,
    ComponentsModule
  ],
  declarations: [CreatorsComponent, CreatorEntryComponent],
  entryComponents: [CreatorEntryComponent]
})
export class CreatorsModule {}
