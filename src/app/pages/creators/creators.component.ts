import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router, NavigationExtras } from "@angular/router";
import * as md5 from "crypto-js/md5";
import * as _ from "lodash";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { combineLatest, Subscription } from "rxjs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";

import { CreatorsService } from "../../services/creators.service";
import { CreatorEntryComponent } from "./creator-entry/creator-entry.component";

@Component({
  selector: "app-creators",
  templateUrl: "./creators.component.html",
  styleUrls: ["./creators.component.css"]
})
export class CreatorsComponent implements OnInit {
  mode: string = "all";
  creatorsArr: any[] = [];

  creatorsArrCopy: any[] = [];

  fakeArr: any[] = [];

  token: string;
  business: any = {};
  showContent: boolean = false;
  showAllContentError: boolean = false;

  allContentError: any = {};

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private modalService: BsModalService,
    private changeDetection: ChangeDetectorRef,
    public creators: CreatorsService
  ) {
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    for (let i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }
  }

  ngOnInit() {
    console.log("initializeData");
    this.initializeData();
  }

  private initializeData() {
    this.showContent = false;
    this.creators.creator_business(this.token).then(
      (data: any) => {
        console.log("data: ", data);
        if (data && data.success) {
          if (_.isArray(data.data.data) && data.data.data[0] !== false) {
            this.creatorsArr = data.data.data;
            this.creatorsArrCopy = data.data.data;
          }
        }
        this.showContent = true;
      },
      error => {
        console.log("error: ", error);
        this.showAllContentError = true;
        if (error && !error.success) {
          this.allContentError = error;
        }
      }
    );
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }

  createCreators() {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.initializeData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "create"
    };
    console.log("initialState: ", initialState);

    const modalConfig: any = {
      animated: true,
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      CreatorEntryComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  deleteCreator(item) {
    if (this.business.role !== 1) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.coolDialogs
      .confirm("Do you want to unlink this creator?")
      .subscribe(res => {
        if (res) {
          this.creators.creator_unlink_business(this.token, item).then(
            (data: any) => {
              if (data && data.success) {
                this.toastr.success(data.message, "SUCCESS");
                setTimeout(() => {
                  this.initializeData();
                }, 600);
              } else {
                this.toastr.warning(data.result.msg, "WARNING");
              }
            },
            error => {
              console.log("error: ", error);
              if (error && !error.success) {
                this.toastr.error(error.message, "ERROR");
                return;
              }
            }
          );
        } else {
          console.log("You clicked Cancel. You smart.");
        }
      });
  }

  refresh() {
    this.initializeData();
  }
}
