import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  NgZone,
  ViewEncapsulation
} from "@angular/core";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as _ from "lodash";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";

import { CreatorsService } from "../../../services/creators.service";

@Component({
  selector: "app-creator-entry",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./creator-entry.component.html",
  styleUrls: ["./creator-entry.component.css"]
})
export class CreatorEntryComponent implements OnInit {
  creator: any = {};
  action: string;
  token: string;

  isSaving = false;
  submitted = false;

  constructor(
    public bsModalRef: BsModalRef,
    private changeDetection: ChangeDetectorRef,
    public zone: NgZone,
    public toastr: ToastrService,
    public coolDialog: NgxCoolDialogsService,
    private modalService: BsModalService,
    private creatorServices: CreatorsService
  ) {
    this.token = localStorage.getItem("bus.token");
  }

  ngOnInit() {}

  private createCreator() {
    this.submitted = true;
    this.isSaving = true;
    this.toastr.info("Saving...", "INFO");
    this.creatorServices.creator_create_business(this.token, this.creator).then(
      (data: any) => {
        if (data && data.success) {
          this.toastr.success(data.result, "SUCCESS");
          this.modalService.setDismissReason("save");
          setTimeout(() => {
            this.bsModalRef.hide();
          }, 600);
        }
        this.submitted = false;
        this.isSaving = false;
      },
      (error: any) => {
        console.log("error: ", error);
        this.submitted = false;
        this.isSaving = false;
        if (error && !error.success) {
          this.toastr.error(error.error, "ERROR");
          return;
        }
      }
    );
  }

  saveChanges() {
    if (_.isEmpty(this.creator.type)) {
      this.toastr.error("Creator type is required!", "WARNING");
      return;
    }

    if (_.isEmpty(this.creator.firstName)) {
      this.toastr.error("First Name is required!", "WARNING");
      return;
    }

    if (_.isEmpty(this.creator.lastName)) {
      this.toastr.error("Last Name is required!", "WARNING");
      return;
    }

    if (_.isEmpty(this.creator.email)) {
      this.toastr.error("Email Address is required!", "WARNING");
      return;
    }

    if (this.action === "create") {
      this.createCreator();
    } else if (this.action === "update") {
    }
  }
}
