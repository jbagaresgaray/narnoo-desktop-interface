import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatorEntryComponent } from './creator-entry.component';

describe('CreatorEntryComponent', () => {
  let component: CreatorEntryComponent;
  let fixture: ComponentFixture<CreatorEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatorEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatorEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
