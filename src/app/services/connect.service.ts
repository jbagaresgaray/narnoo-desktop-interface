import { Injectable } from "@angular/core";
import { Restangular } from "ngx-restangular";
import { ToastrService } from "ngx-toastr";

@Injectable({
  providedIn: "root"
})
export class ConnectService {
  constructor(public restangular: Restangular, public toastr: ToastrService) {}

  search_businesses(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        "Content-Type": "application/json; charset=utf-8",
        Authorization: "Bearer " + localStorage.getItem("app.usertoken")
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("search/business")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  connect_list(token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        "Content-Type": "application/json; charset=utf-8",
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("connect/list")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  connect_find(token, data?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("connect/find")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  connect_search(token, data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("connect/search")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  connect_add(token, data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("connect/add")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  connect_edit(token, data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("connect/edit")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  connect_revoked(token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        "Content-Type": "application/json; charset=utf-8",
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("connect/revoked")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  connect_followers(token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        "Content-Type": "application/json; charset=utf-8",
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("connect/followers")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  connect_following(token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        "Content-Type": "application/json; charset=utf-8",
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("connect/following")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  connect_remove(token, data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("connect/remove")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
