import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { Restangular } from "ngx-restangular";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class CreatorsService {
  base_url: string = environment.api_url + environment.api_version;

  constructor(public http: HttpClient, public toastr: ToastrService, public restangular: Restangular) { }

  creator_business(token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("creator/business")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  creator_list(token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("creator/list")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  creator_create_business(token, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("creator/create")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  creator_unlink_business(token, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("creator/unlink")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
