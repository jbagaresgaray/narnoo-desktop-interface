import { TestBed } from "@angular/core/testing";

import { ByobService } from "./byob.service";

describe("ByobService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: ByobService = TestBed.get(ByobService);
    expect(service).toBeTruthy();
  });
});
