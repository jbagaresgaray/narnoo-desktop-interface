import { Injectable } from "@angular/core";
import { Restangular } from "ngx-restangular";
import { ToastrService } from "ngx-toastr";

@Injectable({
  providedIn: "root"
})
export class ByobService {
  constructor(public restangular: Restangular, public toastr: ToastrService) {}

  assigned_platform(Id, token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("byob/assigned_platform/" + Id)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  platform_options(Id, token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("byob/platform_options/" + Id)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  platform_edit(data: any, token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("byob/platform_edit")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
