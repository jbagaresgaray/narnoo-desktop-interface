import { Injectable } from "@angular/core";
import { Restangular } from "ngx-restangular";
import { ToastrService } from "ngx-toastr";

@Injectable({
  providedIn: "root"
})
export class UploadService {
  constructor(public restangular: Restangular, public toastr: ToastrService) {}

  upload_image_policy(token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("upload/image_policy")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  upload_print_policy(token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("upload/print_policy")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  upload_video_policy(token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("upload/video_policy")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  upload_logo_policy(token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("upload/logo_policy")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  update_print_policy(token, Id) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("upload/update_print_policy/" + Id)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  update_video_policy(token, Id) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("upload/update_video_policy/" + Id)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  update_video_thumbnail_policy(token, Id) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("upload/video_thumbnail_policy/" + Id)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
