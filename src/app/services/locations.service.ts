import { Injectable } from "@angular/core";
import { Restangular } from "ngx-restangular";
import { ToastrService } from "ngx-toastr";

@Injectable({
  providedIn: "root"
})
export class LocationsService {
  constructor(public restangular: Restangular, public toastr: ToastrService) {}

  get_locations(token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("location/list")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  delete_locations(token, Id) {
    const formData = new FormData();
    formData.append("location", Id);

    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("location/delete")
        .customPOST(formData)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  add_location(token, data) {
    const formData = new FormData();
    formData.append("location", data.name);

    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("location/add")
        .customPOST(formData)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  edit_location(token, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("location/edit")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
