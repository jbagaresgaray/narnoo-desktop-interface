import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LinkHttpPipe } from '../pipes/link-http/link-http.pipe';
import { FileSizePipe } from '../pipes/file-size/file-size.pipe';
import { RelativeTimePipe } from '../pipes/relative-time/relative-time.pipe';
import { TimesagoPipe } from '../pipes/timesago/timesago.pipe';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [
		TimesagoPipe,
		FileSizePipe,
		LinkHttpPipe,
		RelativeTimePipe
	],
	exports: [
		TimesagoPipe,
		FileSizePipe,
		LinkHttpPipe,
		RelativeTimePipe
	]
})
export class PipesModule { }
