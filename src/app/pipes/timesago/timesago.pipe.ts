import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
	name: 'timesago'
})
export class TimesagoPipe implements PipeTransform {
	now: any;

	transform(value: string, args) {
		this.now = moment(value).fromNow();
		return this.now;
	}
}
