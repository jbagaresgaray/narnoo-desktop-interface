import { Routes, CanActivate } from "@angular/router";
import _ from "lodash";

import { AuthGuard } from "./auth/auth.guard";
import { AuthHomeGuard } from "./auth/auth-home.guard";
import { AuthAppGuard } from "./auth/auth-app.guard";

import { LoginComponent } from "./pages/login/login.component";
import { HomeComponent } from "./pages/home/home.component";
import { MainComponent } from "./pages/main/main.component";
import { RegisterComponent } from "./pages/register/register.component";
import { ForgotPasswordComponent } from "./pages/forgot-password/forgot-password.component";
import { ResetPasswordComponent } from "./pages/reset-password/reset-password.component";
import { AboutComponent } from "./pages/about/about.component";

import { ImagesComponent } from "./pages/media/images/images.component";
import { ImageDetailsComponent } from "./pages/media/images/image-details/image-details.component";

import { LogosComponent } from "./pages/media/logos/logos.component";
import { LogoDetailsComponent } from "./pages/media/logos/logo-details/logo-details.component";

import { PrintsComponent } from "./pages/media/prints/prints.component";
import { PrintDetailsComponent } from "./pages/media/prints/print-details/print-details.component";

import { VideosComponent } from "./pages/media/videos/videos.component";
import { VideoDetailsComponent } from "./pages/media/videos/video-details/video-details.component";

import { AlbumsComponent } from "./pages/media/albums/albums.component";
import { AlbumDetailsComponent } from "./pages/media/albums/album-details/album-details.component";

import { BusinessProfileComponent } from "./pages/business-profile/business-profile.component";
import { BusinessUsersComponent } from "./pages/business-users/business-users.component";

import { ChannelsComponent } from "./pages/media/channels/channels.component";
import { ChannelDetailsComponent } from "./pages/media/channels/channel-details/channel-details.component";

import { CollectionsComponent } from "./pages/media/collections/collections.component";
import { CollectionDetailsComponent } from "./pages/media/collections/collection-details/collection-details.component";

import { CompanyBioComponent } from "./pages/company-bio/company-bio.component";

import { CreateBusinessComponent } from "./pages/create-business/create-business.component";

import { CreatorsComponent } from "./pages/creators/creators.component";

import { LocationsComponent } from "./pages/locations/locations.component";

import { ProductsComponent } from "./pages/products/products.component";
import { ProductDetailsComponent } from "./pages/products/product-details/product-details.component";

import { SearchBusinessComponent } from "./pages/search-business/search-business.component";
import { SocialLinksComponent } from "./pages/social-links/social-links.component";

import { StaffsComponent } from "./pages/staffs/staffs.component";

import { ConnectedBusinessComponent } from "./pages/connected-business/connected-business.component";
import { ConnectedMediaComponent } from "./pages/connected-business/connected-media/connected-media.component";

import { BasiclayoutComponent } from "./pages/layout/basiclayout/basiclayout.component";


export const ROUTES: Routes = [
  // Main redirect
  { path: "", redirectTo: "login", pathMatch: "full" },
  {
    path: "login",
    component: LoginComponent,
    data: {
      class: "o-page--center"
    }
  },
  { path: "register", component: RegisterComponent },
  { path: "forgot", component: ForgotPasswordComponent },
  { path: "reset", component: ResetPasswordComponent },
  {
    path: "home",
    canActivateChild: [AuthHomeGuard],
    component: HomeComponent,
    data: {
      class: "o-page"
    }
  },
  {
    path: "",
    component: BasiclayoutComponent,
    canActivate: [AuthHomeGuard],
    children: [
      {
        path: "about",
        canActivateChild: [AuthHomeGuard],
        component: AboutComponent,
        data: {
          isGrid: "grid",
          showSidebar: !_.isEmpty(
            JSON.parse(localStorage.getItem("business")) || {}
          )
        }
      },
      // {
      // 	path: 'main',
      // 	canActivateChild: [AuthGuard],
      // 	component: MainComponent,
      // 	data: {
      // 		isGrid: false,
      // 		showSidebar: true
      // 	}
      // },
      {
        path: "media/images",
        canActivateChild: [AuthHomeGuard, AuthGuard],
        component: ImagesComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "media/images/:id",
        canActivateChild: [AuthGuard],
        component: ImageDetailsComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "media/logos",
        canActivateChild: [AuthGuard],
        component: LogosComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "media/logos/:id",
        canActivateChild: [AuthGuard],
        component: LogoDetailsComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "media/prints",
        canActivateChild: [AuthGuard],
        component: PrintsComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "media/prints/:id",
        canActivateChild: [AuthGuard],
        component: PrintDetailsComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "media/videos",
        canActivateChild: [AuthGuard],
        component: VideosComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "media/videos/:id",
        canActivateChild: [AuthGuard],
        component: VideoDetailsComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "media/albums",
        canActivateChild: [AuthGuard],
        component: AlbumsComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "media/albums/:id",
        canActivateChild: [AuthGuard],
        component: AlbumDetailsComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "media/collections",
        canActivateChild: [AuthGuard],
        component: CollectionsComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "media/collections/:id",
        canActivateChild: [AuthGuard],
        component: CollectionDetailsComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "media/channels",
        canActivateChild: [AuthGuard],
        component: ChannelsComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "media/channels/:id",
        canActivateChild: [AuthGuard],
        component: ChannelDetailsComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "user-profile",
        canActivateChild: [AuthGuard],
        component: BusinessProfileComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "users",
        canActivateChild: [AuthGuard],
        component: BusinessUsersComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "company-bio",
        canActivateChild: [AuthGuard],
        component: CompanyBioComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "creators",
        canActivateChild: [AuthGuard],
        component: CreatorsComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "locations",
        canActivateChild: [AuthGuard],
        component: LocationsComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "products",
        canActivateChild: [AuthGuard],
        component: ProductsComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "products/:id",
        canActivateChild: [AuthGuard],
        component: ProductDetailsComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "social-links",
        canActivateChild: [AuthGuard],
        component: SocialLinksComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "staffs",
        canActivateChild: [AuthGuard],
        component: StaffsComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "connected-business",
        canActivateChild: [AuthGuard],
        component: ConnectedBusinessComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      },
      {
        path: "connected-business/:id/business",
        canActivateChild: [AuthGuard],
        component: ConnectedMediaComponent,
        data: {
          isGrid: "grid",
          showSidebar: true
        }
      }
    ]
  },
  // Handle all other routes
  { path: "**", redirectTo: "fbxsample" }
];
