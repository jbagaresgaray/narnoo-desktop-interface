import { ComponentsModule } from './components.module';

describe('SkeletonItemModule', () => {
  let skeletonItemModule: ComponentsModule;

  beforeEach(() => {
    skeletonItemModule = new ComponentsModule();
  });

  it('should create an instance', () => {
    expect(skeletonItemModule).toBeTruthy();
  });
});
