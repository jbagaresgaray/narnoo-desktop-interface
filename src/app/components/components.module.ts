import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SkeletonItemComponent } from './skeleton-item/skeleton-item.component';

@NgModule({
  imports: [CommonModule],
  declarations: [SkeletonItemComponent],
  exports: [SkeletonItemComponent],
  entryComponents: [SkeletonItemComponent]
})
export class ComponentsModule {}
