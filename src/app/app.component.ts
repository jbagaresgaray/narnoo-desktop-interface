import { Component, OnInit } from "@angular/core";
import $ from "jquery";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = "narnoo-desktop-interface";

  constructor() {}

  ngOnInit(): void {
    if (navigator.appVersion.indexOf("Win") !== -1) {
      $("body").addClass("app-root-windows");
      $("body").addClass("platform-windows");
    }
    if (navigator.appVersion.indexOf("Mac") !== -1) {
      $("body").addClass("app-root-mac");
      $("body").addClass("platform-mac");
    }
    if (navigator.appVersion.indexOf("X11") !== -1) {
      $("body").addClass("app-root-unix");
      $("body").addClass("platform-unix");
    }
    if (navigator.appVersion.indexOf("Linux") !== -1) {
      $("body").addClass("app-root-linux");
      $("body").addClass("platform-linux");
    }
  }
}
