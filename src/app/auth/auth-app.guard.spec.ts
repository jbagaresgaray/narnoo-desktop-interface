import { TestBed, async, inject } from '@angular/core/testing';

import { AuthAppGuard } from './auth-app.guard';

describe('AuthAppGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthAppGuard]
    });
  });

  it('should ...', inject([AuthAppGuard], (guard: AuthAppGuard) => {
    expect(guard).toBeTruthy();
  }));
});
