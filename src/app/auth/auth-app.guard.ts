import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import _ from 'lodash';


@Injectable({
	providedIn: 'root'
})
export class AuthAppGuard implements CanActivate {
	constructor(private router: Router) { }

	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): boolean {
		return this.checkLogin();
	}

	checkLogin(): boolean {
		let token = localStorage.getItem('app.usertoken');
		if (!_.isEmpty(token)) {
			this.router.navigate(['/home']);
			return true;
		}

		// Navigate to the login page with extras
		return true;
	}
}
