import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, Router } from '@angular/router';
import { Observable } from 'rxjs';
import _ from 'lodash';

import { LoginService } from '../services/login.service';

@Injectable({
	providedIn: 'root'
})
export class AuthHomeGuard implements CanActivate {

	constructor(private authService: LoginService, private router: Router) { }

	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): boolean {
		return this.checkLogin();
	}

	checkLogin(): boolean {
		let token = localStorage.getItem('app.usertoken');
		if (!_.isEmpty(token)) { return true; }

		if (this.authService.isLoggedIn) { return true; }

		// Navigate to the login page with extras
		this.router.navigate(['/login']);
		return false;
	}
}
