import { BrowserModule } from "@angular/platform-browser";
import { NgModule, ModuleWithProviders } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastrModule } from "ngx-toastr";
import { NgxSpinnerModule } from "ngx-spinner";
import { LoadingBarHttpClientModule } from "@ngx-loading-bar/http-client";
import { LoadingBarHttpModule } from "@ngx-loading-bar/http";
import { LoadingBarModule, LoadingBarService } from "@ngx-loading-bar/core";
import { NgxCoolDialogsModule } from "ngx-cool-dialogs";
import { ContextMenuModule } from "ngx-contextmenu";
import { BroadcasterService } from "ng-broadcaster";
import { NgxElectronModule } from "ngx-electron";
import { ClipboardModule } from "ngx-clipboard";
import { TagInputModule } from "ngx-chips";

import {
  DropzoneModule,
  DropzoneConfigInterface,
  DROPZONE_CONFIG
} from "ngx-dropzone-wrapper";

import {
  BsDropdownModule,
  TabsModule,
  ButtonsModule,
  ModalModule,
  AccordionModule,
  PopoverModule,
  PaginationModule
} from "ngx-bootstrap";
import { RestangularModule } from "ngx-restangular";
import { QuillModule } from "ngx-quill";

import { AppComponent } from "./app.component";
import { BusinessProfileModule } from "./pages/business-profile/business-profile.module";
import { BusinessUsersModule } from "./pages/business-users/business-users.module";
import { CompanyBioModule } from "./pages/company-bio/company-bio.module";
import { CreatorsModule } from "./pages/creators/creators.module";
import { HomeModule } from "./pages/home/home.module";
import { LocationsModule } from "./pages/locations/locations.module";
import { LoginModule } from "./pages/login/login.module";
import { MainModule } from "./pages/main/main.module";
import { MediaModule } from "./pages/media/media.module";
import { ProductsModule } from "./pages/products/products.module";
import { SocialLinksModule } from "./pages/social-links/social-links.module";
import { StaffsModule } from "./pages/staffs/staffs.module";
import { ForgotPasswordModule } from "./pages/forgot-password/forgot-password.module";
import { ResetPasswordModule } from "./pages/reset-password/reset-password.module";
import { RegisterModule } from "./pages/register/register.module";
import { AboutModule } from "./pages/about/about.module";
import { ConnectedBusinessModule } from "./pages/connected-business/connected-business.module";
import { DatapickerModule } from "./pages/datapicker/datapicker.module";
import { CollectionChannelPickerModule } from "./pages/collection-channel-picker/collection-channel-picker.module";
import { ProductPickerModule } from "./pages/product-picker/product-picker.module";

import { PipesModule } from "./pipes/pipes.module";
import { ComponentsModule } from "./components/components.module";

import { environment } from "../environments/environment";
import { ROUTES } from "./app.routes";
import { AuthGuard } from "./auth/auth.guard";

import { BasiclayoutComponent } from "./pages/layout/basiclayout/basiclayout.component";

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  url: environment.api_url + environment.api_version,
  createImageThumbnails: true,
  autoProcessQueue: false,
  maxFiles: 10,
  parallelUploads: 10,
  addRemoveLinks: true,
  method: "POST",
  timeout: 3600000
};

export function RestangularConfigFactory(
  RestangularProvider,
  loader: LoadingBarService
) {
  RestangularProvider.setBaseUrl(environment.api_url + environment.api_version);
  RestangularProvider.addFullRequestInterceptor(() => loader.start());
  RestangularProvider.addErrorInterceptor(() => loader.complete());
  RestangularProvider.addResponseInterceptor(data => {
    loader.complete();
    return data || {};
  });
}

const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(ROUTES);
@NgModule({
  declarations: [AppComponent, BasiclayoutComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxSpinnerModule,
    QuillModule,
    NgxElectronModule,
    ClipboardModule,
    TagInputModule,
    DropzoneModule,
    LoadingBarModule.forRoot(),
    ContextMenuModule.forRoot({
      useBootstrap4: true
    }),
    NgxCoolDialogsModule.forRoot(),
    ToastrModule.forRoot(),
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ButtonsModule.forRoot(),
    ModalModule.forRoot(),
    PopoverModule.forRoot(),
    AccordionModule.forRoot(),
    PaginationModule.forRoot(),
    RestangularModule.forRoot([LoadingBarService], RestangularConfigFactory),
    PipesModule,
    ComponentsModule,
    BusinessProfileModule,
    BusinessUsersModule,
    CompanyBioModule,
    CreatorsModule,
    HomeModule,
    LocationsModule,
    LoginModule,
    MainModule,
    MediaModule,
    ProductsModule,
    SocialLinksModule,
    StaffsModule,
    ForgotPasswordModule,
    ResetPasswordModule,
    RegisterModule,
    AboutModule,
    ConnectedBusinessModule,
    DatapickerModule,
    CollectionChannelPickerModule,
    ProductPickerModule
  ],
  providers: [
    BroadcasterService,
    {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
