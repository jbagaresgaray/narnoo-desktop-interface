"use strict";

const { ipcMain, ipcRenderer, BrowserWindow } = require("electron");
const ProgressBar = require("electron-progressbar");
// const download = require("download");
const { download } = require("electron-dl");
const path = require("path");
const os = require("os");

let progressBar = null;

module.exports = function(app, win) {
	ipcMain.on("synchronous-message", (event, arg) => {
		console.log("synchronous-message: ", arg); // prints "ping"
		event.returnValue = "pong";
	});
	ipcMain.on("show::progressBar", (event, arg) => {
		if (progressBar) {
			console.log("existing progressBar");
			progressBar.maxValue = progressBar.getOptions().maxValue + 100;
		} else {
			console.log("create new progressBar");
			progressBar = new ProgressBar({
				indeterminate: false,
				abortOnError: true
			});
		}
	});
	ipcMain.on("file::upload", (event, arg) => {
		if (arg) {
			if (arg.progress) {
				progressBar.detail = "Please Wait...";
				progressBar.value = arg.progress;
				progressBar.indeterminate = false;
				progressBar.closeOnComplete = false;

				progressBar
					.on("completed", function() {
						console.info(`completed...`);
						progressBar.detail = "Upload Complete...";
					})
					.on("aborted", function() {
						console.info(`aborted...`);
						progressBar = null;
					})
					.on("progress", function(value) {
						let progress = value / 100;
						let progress2 = Math.round(value);
						progressBar.text = "Uploading " + arg.filename;
						progressBar.detail = `Uploading ${progress2} %  of ${
							progressBar.getOptions().maxValue
						}% ...`;
						win.setProgressBar(progress);
					});
				if (arg.progress >= 100) {
					if (arg.fileAdded === 0) {
						progressBar.setCompleted();
					}
				}
			}
		}
	});
	ipcMain.on("media::deleting", (event, arg) => {
		console.log("media::deleting: ", arg);
		if (arg) {
			progressBar.text = "Deleting...";
			progressBar.detail = "Please Wait...";
			progressBar.indeterminate = false;
			progressBar.value = arg;
			progressBar.closeOnComplete = true;
			progressBar
				.on("completed", function() {
					console.info(`completed...`);
					progressBar.detail = "Deleting Complete...";
					progressBar = null;
				})
				.on("aborted", function() {
					console.info(`aborted...`);
					progressBar.setCompleted();
					progressBar = null;
				})
				.on("progress", function(value) {
					let progress = value / 100;
					console.log("progressBar value: ", progress);
					win.setProgressBar(progress);
				});
			if (arg >= 100) {
				progressBar.setCompleted();
			}
		}
	});
	ipcMain.on("open:link", (event, arg) => {
		require("electron").shell.openExternal(arg);
	});
	ipcMain.on("file:download", (event, arg) => {
		console.log("file:download: ", arg);

		/*const savePath = path.join(os.tmpdir(), arg.filename);
		const dragicon = path.join(__dirname, "../assets/icons/dragicon.png");
		download(arg.file, os.tmpdir()).then(
			() => {
				event.sender.startDrag({
					icon: dragicon,
					file: savePath
				});
			},
			error => {
				console.log("Download Error: ", error);
			}
		);*/

		download(BrowserWindow.getFocusedWindow(), arg.file, {
			saveAs: true,
			filename: arg.filename
		})
			.then(dl => console.log("dl: ", dl.getSavePath()))
			.catch(error => {
				console.error("download ERror: ", error);
			});
	});
};
