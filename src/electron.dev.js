"use strict";

const {
  app,
  BrowserWindow,
  globalShortcut,
  Menu
} = require("electron");
const url = require('url');
const path = require('path');
const menuBuilder = require('./scripts/menu');


let win;

function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 1260,
    height: 800,
    backgroundColor: "#ffffff",
    icon: path.join(__dirname, 'src/assets/icons/png/64x64.png'),
    webPreferences: {
      nodeIntegration: true,
    },
    title: "Narnoo Client Desktop"
  });
  // win.setMenu(null);
  require('./scripts/events')(app, win);

  menuBuilder.buildMenu();
  globalShortcut.register('CmdOrCtrl+Shift+D', () => {
    win.webContents.toggleDevTools();
  });

  win.loadURL(url.format({
    pathname: 'localhost:4200',
    protocol: 'http:',
    slashes: true
  }));

  //// uncomment below to open the DevTools.
  win.webContents.openDevTools();

  // Event when the window is closed.
  win.on("closed", function () {
    win = null;
  });
}

// Create window on electron intialization
app.on("ready", () => {
  createWindow();
});

// Quit when all windows are closed.
app.on("window-all-closed", function () {
  globalShortcut.unregisterAll();
  // On macOS specific close process
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", function () {
  // macOS specific close process
  if (win === null) {
    createWindow();
  }
});
